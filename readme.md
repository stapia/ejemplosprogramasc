# This is a repository of classroom examples #

Please, be aware of the following conditions:

* The Examples may **include deliberate bugs** in order to explain common pitfalls.
* The examples are not suitable for any propose except academic use. Readers might find some professional use, but it is unlikely.
* The examples are not the best solution for a given problema. Actually, I use to play around worse solutions to teach some concepts. 
* The examples could be written in Spanish.

# Copyright #

Copyright by Santiago Tapia-Fernández

Examples are under **LGLP, except for academic or teaching use** (courses, tutorials, books and so forth). If you are interested in publish any example for that propose, please contact me. 

# Content, use, install #

All examples are 1 file 1 program examples in ANSI C, using standard input and output. In order to compile:


```
#!bash

gcc -ansi -Wall -pedantic -o program.run program.c
./program.run

```


Sometimes, you may need to link the math library, add **-lm** to the compile options.

# Repositorio de Ejemplos de Clase #

Los alumnos de mis clases pueden encontrar aquí los ejemplos que hacemos en clase en directo. Mi recomendación es tener mucho cuidado, como ya he dicho en inglés:

* Los programas pueden **tener algún error**.
* No necesariamente son la mejor solución para un problema.

El objetivo de tenerlo aquí es que "veáis" un sistema de control de versiones en acción y también una forma de difundir el código fuente que hacemos en clase sin necesidad de que estéis apuntando todo el rato. 

Esto no significa que podáis coger los ejemplos como material independiente del soporte de la clase. Normalmente el código fuente no está explicado mediante los necesarios comentarios simplemente porque los comentarios son lo que se explica en clase. Pero si os puede servir como recordatorio o para empezar a mirar lo que hemos hecho un día que no hayáis podido acudir a clase. 

El contenido está organizado solo por carpetas con el grupo y el año.