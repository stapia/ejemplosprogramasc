
/* Media móvil */

/* Usar con ./media_movil.exe < datos.txt */

#include <stdio.h>

void media_movil() {
	double x, y, z;
	double media;
	int i;
	
	scanf("%lf %lf %lf", &x, &y, &z);
	media = (x + y + z) / 3.0;
	printf("%.10f\n", media);
	
	for ( i = 0; i < 7; ++i ) {
		/* Desplazamos valores */
		x = y;
		y = z;
		/* Pedimos el nuevo */
		scanf("%lf", &z );
		/* Calculamos la nueva media */
		media = (x + y + z) / 3.0;
		printf("%.10f\n", media);
	}
}

int main() {
	media_movil();
	return 0;
}

