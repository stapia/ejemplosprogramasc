
/* Calcular la media de 5 números */

#include <stdio.h>

int main() {
	int i;
	double dato, suma, media;
	
	suma = 0;
	for ( i = 0; i < 5; i = i + 1) {
		scanf("%lf", &dato);
		suma = suma + dato;
	}
	
	media = suma / 5;
	printf("Media: %f\n", media);
	
	return 0;
}
