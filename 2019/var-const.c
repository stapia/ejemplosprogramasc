
#include <stdio.h>

int main() {
	
	/* Cuidado: Trunca la parte fraccionaria! */
	int num = -3.6; /* Se llama conversión implícita */
	
	double x = 3.6e-3;
	double y; /* Ojo con la inicializacion */
	double longitud, area = 0, radio = 5;
	
	/* Los espacios dan lo mismo */
	const double            Pi= 3.1416;
	
	/* Error: No se puede cambiar una constante 
	Pi = 3; */
	
	float mi2numero = 0.0;
	/* Identificadores "Raros"
	float 2minumero = 0.0; MAL
	float _hola_mundo, _   ,   holaMundo,HolaMundo; BIEN
	*/
	printf("Numero: %f\n", mi2numero);

	longitud = 2*Pi*radio;
	area = Pi*radio*radio;
	
	/* El %d es para enteros, no para double!!! */
	printf("La longitud es %f y el area %d\n", 
			longitud, area);
	
	printf("Números: %d %f" 
		   "\n y más cosas", num, y);
	
	/* Los tabuladores alinean a "columnas" */
	printf("Números: %d\t\t%f \n ", num, x);
	
	return 0;
}
