

#include <stdio.h>

int main() {
	int a = 5;
	int bo;
	
	bo = a > 10;
	printf("a > 10 es %d\n", bo);
	
	if ( a > 10 ) {
		printf("Mayor que 10\n");
	}
	else if ( a >= 0 )  {
		printf("Mayor o igual que 0\n");
	}
	else {
		printf("Negativo\n");
	}
	return 0;
}
