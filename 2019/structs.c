
#include <stdio.h>

struct masa_puntual {
	double masa;
	double velocidad[3];
	double posicion[3];
};

void mostrar( struct masa_puntual st ) {
	printf("masa: %f\n", st.masa);
	printf("posicion: [%f, %f, %f]\n", 
			st.posicion[0], st.posicion[1], st.posicion[2]);
	printf("velocidad: [%f, %f, %f]\n", 
			st.velocidad[0], st.velocidad[1], st.velocidad[2]);	
}

/* Modifica la struct */
void calcular ( struct masa_puntual *st, double delta_t ) {
	int i;
	for ( i = 0; i < 3; ++i ) {
		st->posicion[i] += st->velocidad[i]*delta_t;
	}
}

struct masa_puntual acelerar( struct masa_puntual st, 
			double fuerza[3], double delta_t ) 
{
	int i;
	struct masa_puntual res;
	
	for ( i = 0; i < 3; ++i ) {
		res.masa = st.masa; 
		res.posicion[i] = st.posicion[i] 
		   + st.velocidad[i] * delta_t 
		   + 0.5*(fuerza[i]/st.masa)*delta_t*delta_t;
		res.velocidad[i] = st.velocidad[i] 
			+ (fuerza[i]/st.masa)*delta_t;
	}
	
	return res;
}

int main() {
	struct masa_puntual p, q, acel[10];
	int i; double fuerza[3] = { 1.0, 0.25, -2.2 };
	
	{ /* Esto sirve para colapsar todas estas 
		sentencias */
		p.masa = 1; 
		p.velocidad[0] = -0.5;
		p.velocidad[1] = 0;
		p.velocidad[2] = 1.1;

		p.posicion[0] = 2.0;
		p.posicion[1] = 0;
		p.posicion[2] = -6.0;
		
		printf("Original\n");
		mostrar( p );
		
		/* Guardo los valores anteriores */
		q = p;
		printf("La copia es:\n");
		mostrar( q );
		
		calcular ( &p, 1.0 );
		
		printf("Modificada\n");
		mostrar( p );
	}
	/* Cada componente del vector es el resultado
	 * de aplicar la fuerza a la componente anterior
	 * durante 0.1 segundos */
	acel[0] = p;
	
	for ( i = 1; i < 10; ++i ) {
		acel[i] = acelerar( acel[i-1], fuerza, 0.1 );
		/* Al final del movimiento le sumo el 
		 * 10% de masa extra */
		acel[i].masa += 0.1*acel[i].masa; 
	}
	
	for ( i = 0; i < 10; ++i ) {
		mostrar(acel[i]);
	}
	
	return 0;
}
