
/* Calcular el máximo de 7 números */

#include <stdio.h>

int main() {
	int i;
	double dato, maximo;

	scanf("%lf", &maximo);
	
	for ( i = 1; i <= 6; i = i + 1) {
		scanf("%lf", &dato);
		
		/* Si es mayor */
		if ( dato > maximo ) {
			/* Lo tengo que cambiar */
			maximo = dato;
		}
	}
	
	printf("maximo: %f\n", maximo);
	
	return 0;
}
