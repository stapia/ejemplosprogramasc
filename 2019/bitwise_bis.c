
#include <stdio.h>

int main()
{
	unsigned char num = 138, result;
	
	result = 0x0f ^ num;
	
	printf("result = %d\n", result);
	
	/* Cuando se muestran los número en Hexadecimal,
	 * se observa que cambia la a por 5 
	 * la a es diez es decir: 1010
	 * y el cinco es: 0101 mientras que el ocho
	 * que son los primeros 4 bits no cambian */
	printf("num = %x, result = %x\n", num, result);
	
	return 0;
}

