
#include <stdio.h>
#include <string.h>

#define DIM 256

int main() {
	int i; char *aux;
	char cadena[20] = "HOLA";
	char texto[DIM] = "";
	
	printf("cadena es '%s'\n", cadena);
	printf("texto es $%s$\n", texto);
	
	scanf("%255s", texto);
	printf("texto es $%s$\n", texto);

	/* MAL : cadena = "mundo" */
	strcpy(cadena, "mundo");
	printf("cadena es '%s'\n", cadena);
	
	strcat(texto," ");
	strcat(texto, cadena);
	printf("texto es $%s$\n", texto);
	
	printf("longitud de texto es %ld\n", 
	        strlen(texto) );
	   
	/* MAL cadena == "mundo" */     
	if ( strcmp("mundo", cadena) == 0 ) {
		printf("cadena es igual a mundo\n"); 
	}
	else {
		printf("cadena es distinta de mundo\n"); 
	}

	strcpy(cadena, "mundo 234");
	printf("cadena es '%s'\n", cadena);
	
	/* Guardar en texto, el valor de 4 
	 * palabras leidas de teclado */
	strcpy(texto, ""); 
	for ( i = 0; i < 4; ++i ) {
		scanf("%19s", cadena);
		strcat(texto, cadena);
		strcat(texto, " ");
	} 
	printf("texto es $%s$\n", texto);
	
	/* Prohibido strcat */
	strcpy(texto, ""); 
	aux = texto;
	for ( i = 0; i < 4; ++i ) {
		scanf("%19s", aux);
		aux = aux + strlen(aux);
		*aux = ' ';
		++aux;
	}
	--aux;
	*aux = '\0';
	printf("texto es $%s$\n", texto);
	
	return 0;
}

