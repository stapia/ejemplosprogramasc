
#include <stdio.h>

int main() {
	/* Datos */
	int hora1 = 2, hora2 = 3, min1 = 20, min2 = 25;
	char meridiam1, meridiam2;
	
	/* Resultado intermedio */
	int min_desde_0_1, min_desde_0_2;

	/* Resultado */
	int hora = -99, min = -99;
	
	printf("Introduce dos horas en formato hh:mm [a|p]m\n");
	scanf("%d:%d %cm", &hora1, &min1, &meridiam1);
	scanf("%d:%d %cm", &hora2, &min2, &meridiam2);
	
	min_desde_0_1 = hora1 * 60 + min1 
	                + (meridiam1 == 'p') * 12 * 60;	
	min_desde_0_2 = hora2 * 60 + min2 
	                + (meridiam2 == 'p') * 12 * 60;	
	                
	hora = (min_desde_0_2 - min_desde_0_1) / 60;
	min = (min_desde_0_2 - min_desde_0_1) % 60;
	
	/* Cuidado hay que tener en cuenta que puede
	 * salir negativo */
	
	printf("Resultado: %d:%02d\n", hora, min);
	return 0;
}
