
#include <stdio.h>
#include <stdlib.h>

void leer(double* pol, int GR_MAX) {
	int i;
	for ( i = 0; i <= GR_MAX; ++i ) {
		scanf("%lf", &(pol[i]));
	}
}

void escribir(const double pol[], int GR_MAX) {
	int i;
	printf("%.2f ", pol[0]);

	for ( i = 1; i <= GR_MAX; ++i ) {
		if ( pol[i] < 0 ) {
			printf("%.2f x^%d ", pol[i],i);
		} else if ( pol[i] > 0 ) {
			printf("+%.2f x^%d ", pol[i],i);
		} else {
			/* Caso de pol[i] == 0 (sobra) */
		}
	}
	printf("\n");	
}

/* Escribir una funcion que sume dos 
 * polinomios de un grado máximo dado
 * y devuelva el resultado en una nueva
 * variable dinámica */
double* suma(int gr_max, const double p[],
                         const double q[]) {
	double *res; int i;
	res = (double*)malloc((gr_max+1)*sizeof(double)); 
	for ( i = 0; i <= gr_max; ++i ) {
		res[i] = p[i] + q[i];
	}
	return res;
}
 
int main() {
	int gr_max; void *p;
	double *pol, *sum;
	scanf("%d", &gr_max);
	p = malloc(sizeof(double)*(gr_max+1));
	pol = (double*) p; /* Operador cast */
	leer( pol, gr_max );
	escribir( pol, gr_max );
	
	sum = suma(gr_max, pol, pol);
	escribir( sum, gr_max );
	
	free( p );
	free( sum );
	
	return 0;
}
