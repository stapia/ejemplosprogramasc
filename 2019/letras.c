
#include <stdio.h>

int main() {
	
	/* Esto es un error: 
	char letra = "B"; */
	
	char letra = 'B';
	char otra = '\t'; /* Esto es una única letra tabulador */
	unsigned char num = 120;
	char tres = '3';
	char cero = '0';
	
	printf("la letra es: $%c$ \n", letra);
	printf("la otra es: $%c$ \n", otra);

	printf("la letra es: $%d$ \n", letra);
	printf("será C? %c \n", letra+1);
	printf("Y este 67 %d \n", letra+1);

	printf("Num (con d): %d \n", num);
	printf("Num (con c): %c \n", num);
	
	printf("Resta : %d\n", tres - cero);
	printf("Resta : $%c$\n", tres - cero); /* Hasta el 32 son caracteres "no imprimibles" */

	printf("Pitido : $%c$\n", 7);
	
	/* En linux se puede ver en la terminal
	 *  con echo $? */
	return 9;
}
