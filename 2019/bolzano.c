
#include <stdio.h>
#include <math.h>

int main() {
	double a, b, m;
	const double precision = 1e-14;
	
	scanf("%lf %lf", &a, &b);
	
	/* Aplicamos bolzano, pero reducimos
	 * el tamaño del intervalo dividiendo
	 * por 2. */
	 
	while ( fabs(b - a) >= precision ) {
		/* Cuidado con calcular la distancia y no el punto medio */
		m = (b + a) / 2; 
		if ( sin(a) * sin(m) < 0 )  {
			b = m;
		}
		else {
			a = m;
		}
	}
	 
	printf("x con f(x) = 0 es %.14f\n", m);
	
	return 0;
}

