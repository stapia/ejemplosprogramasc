
#include <stdio.h>

int main() {
	
	double x;
	int booleano;
	int presi, v1, v2, v3;
	
	scanf("%lf", &x);
	/* Cuidado, hay que repetir la x en la expresión */
	booleano = (0.0 <= x) && (x < 5.0);
	printf("Está en [0, 5) es %d\n", booleano);
	
	printf("Introduce el voto del presi y los vocales:\n");
	scanf("%d %d %d %d", &presi, &v1, &v2, &v3);
	booleano = ( presi && ( v1 || v2 || v3) ) || 
			   ( v1 && v2 && v3 );
	printf("Votación aprobada es %d\n", booleano);
	
	return 0;
}
