
/* Multiplicación de una matriz cuadrada 3x3 (A) por 
 * un vector columna llamado x, es decir: A x = b */

#include <stdio.h>

int main() {
	int i, j;
	double suma;
	
	double A[3][3] = {
			{ 1.5, 2.6, -8.9 },
			{ 3.4, 5.6, 8.9 },
			{ -1.4, -0.5, 0.9 }
		};
		
	double b[3];
	double x[] = { 3.4, 5.6, 8.9 };
	
	for ( i = 0; i < 3; ++i ) {
		suma = 0;
		for ( j = 0; j < 3; ++j ) {
			suma = suma + A[i][j] * x[j];
		}
		b[i] = suma;
	}

	for ( i = 0; i < 3; ++i ) {
		printf("%f\n", b[i]);
	}
	printf("\n");
	
	
	return 0;
}
