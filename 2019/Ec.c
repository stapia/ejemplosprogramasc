
#include <stdio.h>
#include <math.h>

int main() {
	/* Esto son los datos */
	double masa, velocidad;
	/* Esto lo vamos a calcular */
	double Ec;
	
	printf("Introduce la masa en kg\n"
	       "y la velocidad en m/s\n"); 
	scanf("%lf %lf", &masa, &velocidad);
	
	/* Cuidado con las divisiones enteras!!*/
	Ec = (1/2.0)*masa*velocidad*velocidad;
	Ec = masa*pow(velocidad,2)/2;
	
	printf("Ec = %f J\n", Ec);
	return 0;
}
