
#include <stdio.h>

/* Ordenar: 
 * Ordenar un vector double mediante
 * el algoritmo de la burbuja 
 * */
 
void swap( double *x, double *y ) {
	double aux;
	aux = *x; 
	*x = *y;
	*y = aux;
}

void bubble_sort(double *v, int tam) {
	int i, n; 
	for ( n = tam-1; n >= 1; --n ) {		
		/* Esto es una "pasada" hasta n */
		for ( i = 0; i < n; ++i ) {
			if ( v[i] > v[i+1] ) {
				swap( &(v[i]), &(v[i+1]) );
			}
		}
	}
}


int main() {
	/* double vector[5] = { 5, 4, 3, 2, 1 }; */
	double vector[5] = { 5, 4, 3, 5, 1 }; 
	int i;
	bubble_sort(vector, 5);
	for ( i = 0; i < 5; ++i ) {
		printf("%f\n", vector[i]);
	}
	return 0;
}
