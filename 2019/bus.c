
/* Julio de 2014 número 10 */

#include<stdio.h>
#include<stdlib.h> 
#include<string.h> 

struct butaca  { int fila; char col; char * estado; }; 

int main()  { 
	struct butaca **bus; 
	int i, j;
	
	bus = (struct butaca**) malloc( 20 * sizeof(struct butaca *) );
	
	for ( i = 0; i < 20; ++i ) {
		bus[i] = (struct butaca*) malloc( 4 * sizeof(struct butaca) );
		for ( j = 0; j < 4; ++j ) {
			bus[i][j].fila = i + 1;
			bus[i][j].col = j + 'A';
			bus[i][j].estado = NULL;
		}
	}
	
	bus[0][0].estado = (char*) malloc( 10 * sizeof(char) );
	strcpy(bus[0][0].estado, "4545454545");
	
	return 0;
}
