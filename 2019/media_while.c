
#include <stdio.h>

/* Hacer un programa que calcule la media
 * de los datos introducidos por el usuario
 * suponiendo que todos los datos válidos
 * son mayores o iguales que cero */

int main() {
	
	int numero_datos; 
	double media, suma, dato;
	
	numero_datos = 0;
	suma = 0;
	scanf("%lf", &dato);
	while ( dato >= 0 ) {
		suma = dato + suma;
		/* dato = lo que diga el usuario  */
		++numero_datos; /* numero_datos = numero_datos + 1 */
		scanf("%lf", &dato);
	}
	
	if ( numero_datos != 0 ) {
		media = suma / numero_datos;
		printf("La media es %f\n", media);
	}
	else {
		printf("No has introducido datos válidos\n");
	}
	
	return 0;
}

