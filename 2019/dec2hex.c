
#include <stdio.h>

int main()
{
	int numero; 
	char cifra_hx;
	
	scanf("%d", &numero);
	
	if ( 0 <= numero && numero <= 9 ) {
		cifra_hx = '0' + numero;
	}
	else if ( 10 <= numero && numero <= 15 ) {
		cifra_hx = 'a' + numero - 10;
	}
	else {
		printf("No es Hx\n");
	}
	
	printf("%c\n", cifra_hx);
	
	return 0;
}

