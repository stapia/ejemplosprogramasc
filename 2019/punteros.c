
#include <stdio.h>

int main() {
	double a = 0.0, b = 9.9;
	double *p;
	
	p = & a;
	*p = 7.7;
	
	printf("p: %p a: %f  b: %f\n",(void*)p, a, b);
	
	p = & b;
	*p = *p + 1;

	printf("p: %p a: %f  b: %f\n",(void*)p, a, b);
	
	if ( a > b ) {
		/* La mayor es a */
		p = & a;
	}
	else {
		/* La mayor es b */
		p = & b;
	}
	
	*p = 2.0 * *p;
	printf("El doble de la mayor es %f\n", *p);
	
	return 0;
}
