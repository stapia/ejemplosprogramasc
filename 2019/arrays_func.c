
#include <stdio.h>

#define DIM 10

/* Calcular media aritmetica */
double media( double v[], int dim /* Un parametro array (referencia) */ ) {	
	double suma = 0; int i;
	for ( i = 0; i < dim; ++i ) {
		suma = suma + v[i];
	}
	return suma / dim;
}

void mostrar(double v[DIM] /* Un array */ ) {
	int i;
	for ( i = 0; i < DIM; ++i ) {
		printf("%.3f ", v[i] );
		/* Cada 4 pasos del bucle escribe
		 * un salto de línea */
		if ( (i+1) % 4 == 0 ) {
			printf("\n");
		}
	}
	printf("\n");
}

/* Leer valores */
void leer( double *p, int dim /* Un parametro array (referencia) */ ) {
	int i; 
	for ( i = 0; i < dim; ++i ) {
		scanf("%lf", &(p[i]) );
	}
}

int main() {
	double v[DIM];
	int i;
	
	leer( v, DIM );
	mostrar( v );
	
	
	/* Mostrar valores */
	printf("----- Vector original ----- \n");
	
	printf("La media es: %f\n", media(v, DIM) );

	/* Diferencias finitas de primer orden */
	for ( i = 0; i < DIM-1; ++i ) {
		v[i] = v[i+1] - v[i];
	}
	/* Pongo el último a cero para no inducir
	 * al error de pensar que hay "diferencia" */
	v[DIM-1] = 0;
	
	/* Mostrar valores */
	printf("----- Diferencias finitas de primer orden ----- \n");
	for ( i = 0; i < DIM; ++i ) {
		printf("%15.3f ", v[i] );
		if ( (i+1) % 4 == 0 ) {
			printf("\n");
		}
	}
	printf("\n");
	
	/* Multiplicar por 2 cada elemento y lo muestra */
	printf("----- Multiplicadas por 2 ----- \n");
	for ( i = 0; i < DIM; ++i ) {
		v[i] = 2 * v[i];
		printf("%15.3f ", v[i] );
		if ( (i+1) % 4 == 0 ) {
			printf("\n");
		}
	}
	printf("\n");
	
	return 0;
}
