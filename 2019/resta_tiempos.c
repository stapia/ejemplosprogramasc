
#include <stdio.h>

int main() {
	/* Datos */
	int hora1 = 2, hora2 = 3, min1 = 20, min2 = 25;
	char meridiam1, meridiam2;

	/* Resultado */
	int hora = -99, min = -99;
	
	printf("Introduce dos horas en formato hh:mm [a|p]m\n");
	scanf("%d:%d %cm", &hora1, &min1, &meridiam1);
	scanf("%d:%d %cm", &hora2, &min2, &meridiam2);
	
	/* if ( meridiam1 == 'a' && meridiam2 == 'p') */
	/*
	if ( meridiam1 == meridiam2 ) {
		if ( (hora2 < hora1) || 
		     ((hora2 == hora1) && (min2 < min1)) ) {
			hora2 = hora2 + 24;
		}
	}
	else {
		hora2 = 12 + hora2;
	}
	*/
	
	if ( meridiam1 != meridiam2 ) 
	{
		hora2 = 12 + hora2;
	}
	else if ((hora2 < hora1) || ((hora2 == hora1) && (min2 < min1)) ) 
	{
		hora2 = hora2 + 24;
	}
	 
	if ( min2 < min1 ) {
		min = min2 + 60 - min1;
		hora = hora2 - hora1 - 1;
	}
	else {
		min = min2 - min1;
		hora = hora2 - hora1;
	}
	
	printf("Resultado: %d:%02d\n", hora, min);
	return 0;
}
