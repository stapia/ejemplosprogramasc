
#include <stdio.h>
#include <stdlib.h>

#define DIM 5

int main() {
	/* Un puntero que apunta a un puntero 
	 * que apunta a double*/
	double **mat;
	int filas = 3, cols = 4;
	int i,j;
	double suma;
	
	/* Crea un vector de punteros, hay filas punteros */
	mat = (double**) malloc( filas * sizeof(double*) );
	
	for ( i = 0; i < filas; ++i ) {
		/* mat[i] es double*      */
		mat[i] = (double*) malloc ( cols*sizeof(double) ); 
		/* Rellena la mat con valores aleatorios */
		for ( j = 0; j < cols; ++j ) {
			mat[i][j] = rand() * 1.0 / RAND_MAX;
		}
	}
	 
	/* Escribe por pantalla la suma 
	 * las dos primeras filas de mat */
	for ( j = 0; j < cols; ++j ) {
		suma = mat[0][j] + mat[1][j];
		printf("%f ", suma);
	}
	printf("\n");
	
	/* Liberar memoria */
	for ( i = 0; i < filas; ++i ) {
		free( mat[i] );
	}
	
	free( mat );

	return 0;
}
