
/* Calcular el máximo de 7 números */

#include <stdio.h>

int main() {
	int i, resultado;
	
	resultado = 0;

	for ( i = 1; i <= 6; i = i + 1) {
		resultado = resultado << 2 | 1;
		/* resultado = resultado | (1 << (i - 1) * 2; */
	}
	
	printf("resultado: %d\n", resultado);
	
	return 0;
}
