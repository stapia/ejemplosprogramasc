
#include <stdio.h>

#define GR_MAX 5
#define DIM (GR_MAX+1)

void leer(double* pol) {
	int i;
	for ( i = 0; i <= GR_MAX; ++i ) {
		scanf("%lf", &(pol[i]));
	}
}

void escribir(const double pol[]) {
	int i;
	printf("%.2f ", pol[0]);

	for ( i = 1; i <= GR_MAX; ++i ) {
		if ( pol[i] < 0 ) {
			printf("%.2f x^%d ", pol[i],i);
		} else if ( pol[i] > 0 ) {
			printf("+%.2f x^%d ", pol[i],i);
		} else {
			/* Caso de pol[i] == 0 (sobra) */
		}
	}
	printf("\n");	
}

/* Evalua un polinomio de grado GR_MAX 
 * en el punto x, es decir, calcula p(x)
 * el polinomio, pol, se guarda en un array
 * de tamaño GR_MAX+1 (representacion densa) */
double eval(double x, const double *pol) {
	double res = pol[GR_MAX]; int i;
	for ( i = GR_MAX-1; i >= 0; --i ) {
		res = pol[i] + x * res;
	}
	return res;
}

/* Suma dos polinomios */
void suma (double sum[], const double p[], const double q[]) {
	int i;
	for ( i = 0; i <= GR_MAX ; ++i ) {
		sum[i] = p[i] + q[i];
	}
}

int main() {
	
	double pol[DIM];
	
	leer( pol );
	escribir( pol );
	
	printf("p(%.2f) = %.2f\n", 1.1, eval(1.1, pol) );
	
	suma(pol, pol, pol);
	escribir( pol );
	
	return 0;
}
