
#include <stdio.h>

int listado( const char* filename ) {
	FILE *notas;
	
	notas = fopen( filename, "w");
	
	if ( notas == NULL ) {
		return 1;
	}
	else {
		int i; int mat; double n1, n2;
		/* for ( i = 0; i < 30; ++i ) { */
		for ( i = 0; i < 2; ++i ) {
			/* fscanf(stdin, "%d %lf %lf", &mat, &n1, &n2); */
			scanf("%d %lf %lf", &mat, &n1, &n2);
			fprintf(notas, "%d %.2f %.2f\n", mat, n1, n2);
		}
		fclose( notas );
		return 0;
	}
}

int main() {
	
	char name[128];
	scanf("%127s", name);
	
	if ( listado( name ) == 1 ) {
		fprintf(stderr, "Error\n");
	}
	
	return 0;
}
