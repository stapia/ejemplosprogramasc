
/* Calcula y devuelve el maximo valor 
 * de un vector */
int maximo( int *v, int dim ) {
	int max, i;
	max = v[0];
	for ( i = 0; i < dim; ++i ) {
		if ( v[i] > max ) {
			max = v[i];
		}
	}
	return max;
}

/* Devuelve 1 si todos son negativos y 
 * 0 en caso contrario */
int todos_negativos( int *w, int tam ) {
	int i = 0, result = 1;
	/* for ( i = 0; result && i < tam; ++i ) */
	while ( result && i < tam ) {
		result = w[i] < 0;
		++i;
	}
	return result;
}

int main() {
	int w[4] = { -2 , -4, -7, -10 };
	printf("El maximo es %d\n", maximo(w,4));
	return 0;
}
