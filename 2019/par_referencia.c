
#include <stdio.h>

void doblar_mal( int x ) {
	x = 2 * x;
}

void doblar( int *p ) {
	*p = 2 * *p;
}

void swap( int *a, int *b ) {
	int aux;
	aux = *a;
	*a = *b;
	*b = aux;
}

void order( int *a, int *b ) {
	if ( *a < *b ) {
		swap( a, &(*b) );
	}
}

void order3( int *a, int *b, int *c ) {
	order( a, b );
	order( b, c );
	order( a, b );
}

int main() {
	int a = 3, b = 7, otra = 20;

	doblar_mal( a );
	doblar_mal( b );
	printf("a: %d, b: %d\n", a, b);
	
	doblar( &a );
	doblar( &b );
	printf("a: %d, b: %d\n", a, b);

	swap( &a, &b );
	printf("a: %d, b: %d\n", a, b);

	order3( &a, &b, &otra );
	printf("a: %d, b: %d, otra: %d\n", a, b, otra);
	
	return 0;
}
