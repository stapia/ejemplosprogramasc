
#include <stdio.h>

/* Variable global */
double PI = 3.1416;

/* Aqui si puedo definir funciones */
double maximo2(double x, double y) {
	if ( x > y ) {
		return x;
	}
	else {
		return y;
	}
}

double maximo3(double x, double y, double z) {
	return maximo2( x, maximo2( y, z ));
}

int main()
{
	/* Prohibido porque es local!! 
	 * int maximo(int a, int b) ....
	 * */ 
	 
	/* Variables locales */
	double x = 3.3, y = 5.4; /* o cualquier otro identificador */
	double resultado; 
	
	resultado = maximo2( y, x + 4 );
	
	printf("El máximo es %f\n", resultado);
	
	resultado = maximo3( -y, 20.2e2, -x + 4 );

	printf("El máximo es %f\n", resultado);
	
	return 0;
}

