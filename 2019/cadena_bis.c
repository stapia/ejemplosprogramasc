
/* Varios problemas "sencillos"
 * con cadenas, con/sin string.h */
 
/* Dada una cadena escribir por
 * pantalla una letra por línea */

#include <stdio.h>
#include <string.h>
 
void mostrar(char cadena[]) {
	int i;
	int length = strlen(cadena);
	for ( i = 0; i < length; ++i ) {
		printf("%c\n", cadena[i] );
	}
}

void mostrar_string(const char* p) {
	while ( *p /* != '\0' */ ) {
		printf("%c\n", *p );
		++p;
	}
}
 
/* Dada una cadena, modificarla
 * de manera que quede al revés */
void reverse( char cadena[] ) {
	int i, j, len = strlen(cadena); 
	char aux;
	for ( i = 0, j = len - 1; i < j; ++i, --j ) {
		aux = cadena[i];
		cadena[i] = cadena[j];
		cadena[j] = aux;
	}
}
 
/* Dada una cadena, sustituir 
 * todas las ocurrencias de una letra
 * dada por '#' */

void sustituir( char *cadena, char letra ) {
	int i, len = strlen(cadena);
	for ( i = 0; i < len; ++i ) {
		if ( letra == cadena[i] ) {
			cadena[i] = '#';
		}
	}
}

/* Dada una cadena, una letra y un 
 * índice modificarla insertando
 * la letra en la posición, suponemos que
 * pos es menor que strlen */
void insertar(char *p, char ins, int pos) {
	int i = pos; 
	char aux = p[i], aux2;
	p[i] = ins;
	++i;
	while ( p[i] != '\0' ) {
		aux2 = p[i];
		p[i] = aux;
		aux = aux2;
		++i;
	}
	
	p[i] = aux;
	p[i+1] = '\0';
} 
 
int main() {
	char probando[256] = "Hola mundo";
	
	mostrar(probando);
	mostrar_string(probando);
	
	insertar(probando, '$', 0);
	printf("probando ->%s<-\n", probando);

	insertar(probando, '=', 5);
	printf("probando ->%s<-\n", probando);
	
	reverse(probando);
	printf("probando ->%s<-\n", probando);
	
	sustituir(probando, 'o');
	printf("probando ->%s<-\n", probando);
	
	
	return 0;
}
