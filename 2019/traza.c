
#include <stdio.h>

/* Junio 2015. Pregunta 9 */

#define DIM 5

int traza(const char* filename) {
	FILE *g;
	int num, suma = 0;
	int i, j;
	
	g = fopen( filename, "r" );
	
	if ( g != NULL ) {
		for ( i = 0; i < DIM; ++i ) {
			for ( j = 0; j <= i ; ++j ) {
				fscanf(g, "%d", &num);
				if ( i == j ) {
					suma += num;
				}
			}
		}
	}
	fclose( g );
	return suma; 
}

int main() {
	return 0;
}
	
