
#include <stdio.h>
#include <stdlib.h>

#define DIM 5

int main() {
	/* Vector que contiene DIM punteros a double */
	double *mat[DIM];
	
	/* Crea una matriz de DIM x N siendo N un dato
	 * arbitrario */
	int N, i, j;
	double suma;
	
	scanf("%i", &N);
	
	for ( i = 0; i < DIM; ++i ) {
		/* mat[i] es double*      */
		mat[i] = (double*) malloc ( N*sizeof(double) ); 
		/* Rellena la mat con valores aleatorios */
		for ( j = 0; j < N; ++j ) {
			mat[i][j] = rand() * 1.0 / RAND_MAX;
		}
	}
	 
	/* Escribe por pantalla la suma 
	 * las dos primeras filas de mat */
	for ( j = 0; j < N; ++j ) {
		suma = mat[0][j] + mat[1][j];
		printf("%f ", suma);
	}
	printf("\n");
	
	/* Liberar memoria */
	for ( i = 0; i < DIM; ++i ) {
		free( mat[i] );
	}

	return 0;
}
