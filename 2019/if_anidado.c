
#include <stdio.h>

int main() {
	
	if ( 1 > 2 ) 
		if ( 4 < 4 ) 
			printf("hola desde if\n");
		else 
			printf("hola desde else\n");
		
	printf("He acabado parte 1\n");

	if ( 1 > 2 ) {		
		if ( 4 < 4 ) {
			printf("hola desde if\n");
		}
		else {
			printf("hola desde else\n");
		}
	}
		
	printf("He acabado parte 2\n");
	
	return 0;
}
