
#include <stdio.h>

#define DIM 3

void transpose( double mat[][DIM] /*, int filas */ ) {
	int i, j;
	double aux;
	
	for ( i = 0; i < DIM; ++i ) {
		for (  j = 0; j < i; ++j ) {
			aux = mat[i][j];
			mat[i][j] = mat[j][i];
			mat[j][i] = aux;
		}
	}
}

int main() {
	return 0;
}
