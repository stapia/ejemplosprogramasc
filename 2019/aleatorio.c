
#include <stdlib.h>
#include <stdio.h>

int main() {
	int aleatorio, i; 
	double aleatorio_d;
	
	/* Inicializa la semilla de números aleatorios 
	 * es decir, la sucesión de números que se usa
	 * para producir valores en la llamada a rand() */
	srand( 4 );
	
	for ( i = 0; i < 10; ++i ) {		
		/* Cada llamada a rand() proporciona un valor distinto (aleatorio) */
		aleatorio = rand() % 100 + 1; /* El resto toma valores entre 0 y el divisor -1 */
		printf("Número: %d\n", aleatorio);
	}

	for ( i = 0; i < 10; ++i ) {	
		/* Entre 2.0 y 5.0 */	
		aleatorio_d = 2.0 + 3.0 * rand() / RAND_MAX;
		printf("Número: %.14f\n", aleatorio_d);
	}
	
	return 0;
}
