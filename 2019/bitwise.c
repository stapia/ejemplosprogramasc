
#include <stdio.h>

int main() {
	
	int i = 23, j;
	
	j = ~i + 1;
	printf("j = %d\n", j);
	
	j = i & 8;
	printf("j = %d\n", j);
	
	j = i & 4;
	printf("j = %d\n", j);
	
	return 0;
}
