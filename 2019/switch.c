
#include <stdio.h>

int main() {
	double a, b; 
	char op; 
	
	/* Cuidado con el espacio delante del %c */
	scanf("%lf%lf %c", &a, &b, &op);
	switch ( op ) {
		case '+': 
			printf("%f\n",a+b);
		break; 
		case 42: /* Este debe ser el '*' */ 
			printf("%f\n", a*b);
		break; 
		case '/': 
			printf("%f\n", a/b);
		/* break; Esto esta mal hay que poner el break */
		default:
			printf("Unknown operation\n");
	} 	
	return 0;
}
