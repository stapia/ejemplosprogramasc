
#include <stdio.h>

#define DIM 10

#define Real double
#define SCANF_REAL "%lf"

int main() {
	
	/* Mal, no se conoce el tamaño 
	int a; 
	double v[a];
	*/ 
	
	Real v[DIM];
	Real suma;
	int i;
	
	/* Leer valores */
	for ( i = 0; i < DIM; ++i ) {
		scanf(SCANF_REAL, &(v[i]) );
	}
	
	/* Mostrar valores */
	printf("----- Vector original ----- \n");
	for ( i = 0; i < DIM; ++i ) {
		printf("%15.3f ", v[i] );
		/* Cada 4 pasos del bucle escribe
		 * un salto de línea */
		if ( (i+1) % 4 == 0 ) {
			printf("\n");
		}
	}
	printf("\n");
	
	/* Calcular media aritmetica */
	suma = 0;
	for ( i = 0; i < DIM; ++i ) {
		suma = suma + v[i];
	}
	printf("La media es: %f\n", suma / DIM);

	/* Diferencias finitas de primer orden */
	for ( i = 0; i < DIM-1; ++i ) {
		v[i] = v[i+1] - v[i];
	}
	/* Pongo el último a cero para no inducir
	 * al error de pensar que hay "diferencia" */
	v[DIM-1] = 0;
	
	/* Mostrar valores */
	printf("----- Diferencias finitas de primer orden ----- \n");
	for ( i = 0; i < DIM; ++i ) {
		printf("%15.3f ", v[i] );
		if ( (i+1) % 4 == 0 ) {
			printf("\n");
		}
	}
	printf("\n");
	
	/* Multiplicar por 2 cada elemento y lo muestra */
	printf("----- Multiplicadas por 2 ----- \n");
	for ( i = 0; i < DIM; ++i ) {
		v[i] = 2 * v[i];
		printf("%15.3f ", v[i] );
		if ( (i+1) % 4 == 0 ) {
			printf("\n");
		}
	}
	printf("\n");
	
	return 0;
}
