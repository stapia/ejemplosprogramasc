
#include <math.h>
#include <stdio.h>

int main()
{
	double x, y;
	double angulo;
	
	printf("Introduce x e y\n");
	scanf("%lf %lf", &x, &y);
	angulo = atan2( y, x );
	
	printf("El angulo es %.14f rad\n", angulo);
	
	return 0;
}

