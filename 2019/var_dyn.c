
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void hacer_double() {
	double *p =(double*) malloc(sizeof(double));
	*p = 45.6;
	printf("%f en %p\n", *p, (void*)p );
	free( p );
}

int main() {
	/* Hacer un programa que concatene 
	 * dos cadenas indicadas independendiente-
	 * mente de su longitud */
	 
	char cad1[] = "Hola mundo";
	char cad2[] = ", Soy un programa";
	char result[28];
	char *res_dyn = (char*) malloc(
	  sizeof(char)*(strlen(cad1)+strlen(cad2)+1));
	
	strcpy(result, cad1);
	strcat(result, cad2);

	strcpy(res_dyn, cad1);
	strcat(res_dyn, cad2);
	
	printf("En estatico: %s\n", result);
	printf("En dinamico: %s\n", res_dyn);
	
	free( res_dyn );
	
	return 0;
}
