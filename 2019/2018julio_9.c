
/* Un struct que contiene un vector llamado coord */
struct vector3D { double coord[3]; };
 
/* Un struct que contiene 3 struct llamados A, B y C */
struct triangulo { struct vector3D A, B, C; };

/* Una funcion que tiene como parametros 2 struct y devuelve otro */
struct vector3D vectorial(struct vector3D v, struct vector3D w); /*Solo usar */ 

/* Una funcion que tiene como parametro dos vectores de struct */
/* v_triang es un vector de triangulos, v_triang[i] es un triangulo */
/* Cuando ponga A es un vertice y además un struct vector3D */

void calcular(struct triangulo* v_triang, struct vector3D* v_normales, int N) {
	struct vector3D AB, AC;
	int i,j;
	
	for ( i = 0; i < N; ++i ) 
	{
		for ( j = 0; j < 3; ++j ) {
			AB.coord[j] = v_triang[i].B.coord[j] - v_triang[i].A.coord[j];
			AC.coord[j] = v_triang[i].C.coord[j] - v_triang[i].A.coord[j];
		}
		v_normales[i] = vectorial(AB, AC);
	}
}

int main() {
	return 0;
}
