
#include <stdio.h>

int main() {
	
	int i = 7, j = 3; 
	double x, y, z;
	
	printf("Escribe valores para y, z\n");
	scanf("%lf %lf", &y, &z);
	
	j = i / j;
	printf("j = %d\n", j);
	
	j = 3;
	x = i / j;
	printf("x = %f\n", x);
	
	/* Probar a hacer z = 9 */
	x = (2*y)/(z-9) + (z*z)/(7-y);
	printf("x = %f\n", x);
	
	/* Probar a poner 0 */
	scanf("%d", &j);
	j = i / j;
	printf("j = %d\n", j);
	
	return 0;
}
