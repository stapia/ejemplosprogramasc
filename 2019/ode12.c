
#include <stdio.h> 
#include <math.h> 

/* Junio de 2014. Pregunta 3 */

double ode12(double x, char* error); 

double fun(double x) { 
	char letra; double result; 
	result = ode12(x, &letra);
	if ( letra == 'E' ) {
		return -10.0;
	}
	return sin(x*x) * result;
}
