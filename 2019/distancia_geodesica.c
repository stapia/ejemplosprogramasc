
/* https://en.wikipedia.org/wiki/Geographical_distance */
#include <math.h>
#include <stdio.h>

double distancia_km(double lambda1, double lambda2, 
                 double phi1, double phi2) {
	const double R = 6371.009;
	double delta_phi = phi2 - phi1;
	double delta_lambda = lambda2 - lambda1;
	double phi_m = (phi1 + phi2) / 2;
	double dist = R * sqrt( (delta_phi*delta_phi) + 
	                    (cos(phi_m) * delta_lambda) *
	                    (cos(phi_m) * delta_lambda) );
	return dist;
}

int main()
{	
	return 0;
}

