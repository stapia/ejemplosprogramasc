
#include <stdio.h>

int main() {
	
	FILE *g;
	int num = 45;
	
	/* Con "a" no sobreescribe 
	g = fopen("datos.dat", "a"); 
	Este escribe en la carpeta de arriba
	g = fopen("..\\datos.dat", "w");
	*/
	
	g = fopen("datos.txt", "w");
	fprintf(g, "hola\n");
	
	fprintf(g, "%d\n", num);
	
	fprintf(g, "%d%d\n", num, num);
	
	fprintf(g, "%d", num);
	num = fprintf(g, "%d\n", num);

	fprintf(g, "%d\n", num);
	
	fclose(g);
	
	return 0;
}