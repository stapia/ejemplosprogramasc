
#include <stdio.h>

/* Junio 2014. Pregunta 8 */

double carcasa(char *s, char c)
{
	FILE *g;
	char letra; int num; double gr; char cad[30];
	double suma = 0.0;
	g = fopen( s, "r" ); 
	if ( g == NULL ) {
		printf("Error, no se puede abrir '%s'", s);		
		return 0.0;
	} else {
		/* Cuidado con el espeacio antes de %c, hay que ponerlo */
		while ( 4 == fscanf( g, " %c %d %lf %29s", &letra, &num, &gr, cad) ) {
			if ( letra == c ) {
				suma += num * gr;
			}
		}
		fclose( g );
		return suma;
	}
}

int main() {
	return 0;
}
