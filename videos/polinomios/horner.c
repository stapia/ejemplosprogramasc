
/* 
    Santiago Tapia-Fernández © 2020
    UD. Informática Industrial. Departamento AIEEII 
    ETSII - Universidad Politécnica de Madrid (UPM)

    Evaluación de un polinomio mediante la regla de Horner
*/
    
/* 
    Ejemplo:
        p(x)  =  3.5 + 0.3x -7.1x^2 + 1.6x^3 + 2.1x^5 =>
        tupla => ( 3.5, 0.3, -7.1, 1.6, 0.0, 2.1 )
        p(x) = 3.5 + x * ( 0.3 + x * ( (-7.1) + x * ( 1.6 + x * (0.0 + x * 2.1)))) 
        p(2) = 3.5 + 2 * ( 0.3 + 2 * ( (-7.1) + 2 * ( 1.6 + 2 * (0.0 + 2 * 2.1)))) 
             = 55.7 */

#include <stdio.h>

double eval(double x, const double *pol, int grado) {
    double result; int i;
    result = pol[grado];
    for ( i = grado - 1; i >= 0; --i ) {
        result = result * x + pol[i];
    }
    return result;
}

int main() {
    double pol[6] = { 3.5, 0.3, -7.1, 1.6, 0.0, 2.1 };
    double x[5] = { -2, -1, 0, 1, 2 };
    double result; int i;
/*
    Para comprobar
        x    =   -2.0 ;  -1.0 ;  0.0 ;  1.0 ;  2.0    
        p(x) = -105.5 ;  -7.6 ;  3.5 ;  0.4 ; 55.7  */

    for ( i = 0; i < 5; ++i ) {
        result = eval(x[i], pol, 5);
        printf("p(x = %f) = %f\n", x[i], result);
    }

    return 0;
}


