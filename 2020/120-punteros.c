
#include <stdio.h>

int main() {
    int A = 0;
    int *puntero;

    /* Puntero toma la dirección de memoria de A */
    puntero = & A; /* Operador &, address of: calcula la dirección de A */

    /* *puntero es A porque * & A -> A ya que * y & son inversas */
    *puntero = 99; /* Operador *, indirection: calcula la variable de la dirección dada */

    /* El operador (Tipo) expresion, llamado cast u operador de casting
    permite convertir un valor a otro tipo de dato */
    printf("valor de A: %d, dirección de A: %p\n", A, (void*)puntero);
    return 0;
}