
#include <stdio.h>

/* Un programa que pide al
usuario números en coma flotante
hasta que introduzca un valor negativo
y muestra la suma de todos ellos
sin sumar el valor negativo */

int main() {
    double suma, dato;
    suma = 0;
    scanf("%lf", &dato);
    while ( dato >= 0 ) {
        suma = suma + dato;
        scanf("%lf", &dato);
    }
    printf("Suma: %f\n", suma);
    return 0;
}
