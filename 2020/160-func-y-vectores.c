
/* 
    Escribir una función que, dado un vector, calcule la media
    de sus componentes */
double media(double vector[], int tam) {
    double suma = 0; int i;
    for ( i = 0; i < tam; ++i ) {
        suma += vector[i];
    }
    return suma / tam;
}

/* 
    Escribir una función que, dados dos vectores, sume al primer
    vector los valores del segundo vector de índice par */

void suma(double *a, const double *b, int tam) {
    int i;
    for ( i = 0; i < tam; i = i + 2) {
        a[i] = a[i] + b[i];
    }
}

/* 
    Escribir una función que, dado un vector, calcule la suma de
    cada una de sus componentes con la componente que la sigue 
    y guarde el resultado en otro vector. Se supone que el tamaño
    del segundo vector permite guardar el resultado y el tamaño 
    de a es al menos 2. */
void suma_sig(const double *a, double *result, int tam) {
    int i;
    for ( i = 0; i < tam-1; ++i ) {
        result[i] = a[i] + a[i+1];
    }
}

/* 
    Escribir una función que, dado un vector, lo modifique de manera
    que el nuevo valor de sus componentes sea la diferencia entre el
    valor original de cada componente y la componente que la precede 
    (es decir: las diferencias finitas de orden 1). Como las diferencias
    finitas son tam-1, se debe escribir cada una en indice i+1, el v[0]
    lo tengo que fijar a 0 (cero)  */

void diff_1(double v[], int tam) {
    int i;
    for ( i = tam-2; i >= 0; --i ) {
        v[i+1] = v[i+1] - v[i];
    }
    v[0] = 0;
}

#include <stdio.h>

void mostrar(const double v[], int tam) {
    int i;
    for ( i = 0; i < tam; ++i ) {
        printf("%5.1f ", v[i]);
    }
    printf("\n");
}

/* 
    Escribir el programa principal para que llame y compruebe el 
    funcionamiento de las funciones anteriores */

int main() {

    double a[] = { 1.1, 2.2, 3.3, 4.4, 5.5 }; /* el 5 lo calcula el compilar */
    double b[5] = { 10.1, 22.2, 30.3, 42.4, 50.5 };
    /* diff_1 ->  {  0    12.2, ..., .      8.1  } */
    /* Prohibido: double c[]; !!!! */
    double med;

    med = media(a, 5);
    printf("La media de a es %f\n", med);

    med = media(b, 3);
    printf("La media de los primeros 3 valores de b es %f\n", med);

    med = media(b+1, 4);
    printf("La media los valores excepto el primero de b es %f\n", med);

    suma(a, b, 5); /* Modificando a */
    printf("Mostrar a:\n");
    mostrar(a, 5);

    suma_sig(a, b, 5); /* Modificando b */
    printf("Mostrar b:\n");
    mostrar(b, 5);

    diff_1(b, 5);
    printf("Mostrar b:\n");
    mostrar(b, 5);
    return 0;
}
