#include <stdio.h>

/* mcd copiada de un ejemplo anterior */
int mcd(int a, int b) {
    /* int cociente, resto; */
    int resto;

    while ( b != 0 ) {
        /* cociente = a / b; No es necesario */
        resto = a % b;
        a = b; /* ¡Mucho cuidado con el orden de las sentencias! */
        b = resto;
    }

    return a;
}

/*  a) Escribir una función que calcule el cociente
    y el resto de una división entera.

    in: dividendo y divisor
    out: cociente y resto => out: cociente; in/out: resto
*/
int cociente_resto(int dividendo, int divisor, int* resto) {
    *resto = dividendo % divisor;
    return dividendo / divisor;
}

/* b) Escribir una función, simplificar, para que simplifique
    el valor de una fracción dada a través de dos variables:
    una para el numerador y otra para el denominador.

    La fracción va a ser a / b, 
    la nueva fracción será: a' / b'
    dividiendo a y b por el mcd(a, b)

    in: a, b (2 números enteros)
    out: a', b' (2 número enteros)

    in/out: a -> a', b -> b'
*/
void simplificar(int* p_a, int* p_b) {
    int max_divisor;
    max_divisor = mcd(*p_a, *p_b);
    *p_a = *p_a / max_divisor;
    *p_b = *p_b / max_divisor;
}

/*
    int *p; => Puntero a entero (int*) porque es una declaracion 
            Dirección de memoria de un entero

    x = *p + 5 => operador indirección porque es una expresión
*/

int main() {
    /* c) 
        Compruebe si las funciones de a) y b) son correctas 
    */
    int cociente_resultante, resto_resultante;
    int dividendo_entrada = 20;
    int a = 35, b = 21;

    cociente_resultante = cociente_resto(dividendo_entrada, 3+3, &resto_resultante);
    printf("Cociente: %d, resto: %d\n", cociente_resultante, resto_resultante);

    simplificar(&a, &b);
    printf("Numerador: %d, Denomindador: %d\n", a, b);

    a = 21;
    b = 35;
    simplificar(&a, &b);
    printf("Numerador: %d, Denomindador: %d\n", a, b);

    return 0;
}
