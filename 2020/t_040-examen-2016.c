
#include <stdio.h>

/* Problema 2 */
int aprobados(const char *filename) {
    FILE *g; 
    int aprobados = 0;

    g = fopen(filename, "r");
    /* Vamos a leer esto:
    M1501 S 2 6 10  
    M1502 N 4 6 10 7 9
    */

    if ( g != NULL ) {
        int ok, i; 
        char matricula[6]; char si_no;
        int num_trabajos; double nota, media = 0;

        /* Intento leer el comienzo de la primera linea */
        ok = fscanf(g, "%5s %c %d", matricula, &si_no, &num_trabajos);
        while ( ok == 3 ) {
            /* Proceso la linea */
            media = 0;
            for ( i = 0; i < num_trabajos; ++i ) {
                fscanf(g, "%lf", &nota);
                media += nota;
            }
            if ( num_trabajos > 0 ) {
                media = media / num_trabajos;
            }
            aprobados += ( si_no == 'S' ) /* condicion a) */ 
                        && /* Todas las condiciones */
                         ( num_trabajos >= 3 ) && (media > 5); /* condicion b) */
            /* Intento leer el comienzo de la linea siguiente */
            ok = fscanf(g, "%5s %c %d", matricula, &si_no, &num_trabajos);
        }

        fclose(g);
    }
    return aprobados;
}

/* Prototipos para el problema 3 */

void capicua(char *matricula) {
    matricula[0] = matricula[4];
    /* ... hacer el resto */
}

void capicua_N(int *matricula) {
    /* Haciendo divisiones por 100 o 1000 */
}

int main() {
    { /* Problema 2 */
        int numero_aprobados; 
        numero_aprobados = aprobados("datos.txt");
        printf("Número de aprobados = %d\n", numero_aprobados);
    }

    { /* Problema 3 */
        char matricula[6] = "12345";
        int num_matricula = 12345;
        capicua(matricula);
        capicua_N(&num_matricula);
    }

    return 0;
}