
#include <stdio.h>

int main() {

	unsigned short a = 20;
	int b = 10;
	long c = 34;

	float x = 4.5;

	printf("size a: %ld\n", sizeof(a));
	printf("size b: %ld\n", sizeof(b));
	printf("size c: %ld\n", sizeof(c));

	printf("float: %ld\n", sizeof x);
	printf("double: %ld\n", sizeof(double));

	return 0;
}
