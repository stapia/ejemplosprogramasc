
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* 
    Escribir un programa que: */

/* 
    a) Defina dos macros para numero de filas y columnas */

#define ROWS 3
#define COLS 6

int main() {

    /* 
        b) Declare una matriz (rectangular) y dos vectores (ambos de coma flotante).
        Tales que, si la matriz es A y los vectores son x y se b se pueda hacer
        que A x = b en cuanto a las dimensiones de todos ellos. */
    double A[ROWS][COLS];
    double x[COLS];
    double b[ROWS];
    double traza;
    int dim, simetrica;

    int i, j;

    /*
        c) Asigne valores aleatorios ENTEROS entre -3 y 12 para A y x.  */

    for ( i = 0; i < COLS; ++i ) {
        x[i] = rand() % 16 - 3;
    }

    for ( i = 0; i < ROWS; ++i ) {
        for ( j = 0; j < COLS; ++j ) {
            A[i][j] = rand() % 16 - 3;
        }
    }

    /*
        d) Calcule el producto de la matriz por el vector */

    for ( i = 0; i < ROWS; ++i ) {
        for ( j = 0; j < COLS; ++j ) {
            printf("%5.1f ", A[i][j]);
        }

        if ( i < COLS ) {
            printf("\t %5.1f", x[i]);
        }

        printf("\n");
    }

    for ( i = ROWS; i < COLS; ++i ) {
        for ( j = 0; j < COLS; ++j ) {
            printf("%5c ", ' ');
        }
        printf("\t %5.1f\n", x[i]);
    }

    for ( i = 0; i < ROWS; ++i )
    {
        double prod = 0; /* nos podemos ahorrar esta variable */
        for ( j = 0; j < COLS; ++j) {
            prod += A[i][j] * x[j]; /* prod = prod + A[i][j] * x[j]; */
        }
        b[i] = prod;
    }
    
    /*
        e) Muestre el resultado */

    
    
    printf("\n");
    for ( i = 0; i < ROWS; ++i )
    {
        printf("%5.1f\t", b[i]);
    }
    printf("\n");

    /*
        f) Cambie la macros para que la matriz sea cuadrada, después
        calcule la traza y muestre el resultado por pantalla. */

    traza = 0;
    for ( i = 0; i < ROWS && i < COLS; ++i )
    {
        traza += A[i][i];
    }
    printf("traza = %f\n", traza);

    /* 
        g) Compruebe si la matriz es simetrica */

    /* Hacemos que dim sea el minimo de ROWS y COLS */
    dim = ROWS;
    if ( COLS < ROWS ) {
        dim = COLS;
    }
    simetrica = 1;
    for ( i = 0; simetrica && (i < dim - 1); ++i ) {
        for ( j = i+1; (simetrica == 1) && (j < dim); ++j ) {
            if ( fabs(A[i][j] - A[j][i]) > 1e-10 ) { 
                simetrica = 0;
            }
        }
    }
    printf("Simétrica es %d\n", simetrica);

    return 0;
}