
#include <stdio.h>

int comprobarVentas(double ventas[][12], int numVendedores) {
    int vendedor, mes;
    double suma_ventas_mes, ventas_maximas = 0.0;
    int mes_maximo = 0;

    for ( mes = 0; mes < 12; ++mes ) {
        /* Calcular suma de ventas */
        suma_ventas_mes = 0;
        for ( vendedor = 0; vendedor < numVendedores; ++vendedor ) {
            suma_ventas_mes += ventas[vendedor][mes];
        }
        /* Al final de este bucle de arriba obtengo en suma_ventas_mes
        la suma de las ventas de todos los vendedores en mes */

        /* Y esto calcula el maximo de ventas por meses */
        if ( suma_ventas_mes > ventas_maximas ) {
            ventas_maximas = suma_ventas_mes;
            mes_maximo = mes;
        }
    }
    return mes_maximo;
}

int main() {
    return 0;
}