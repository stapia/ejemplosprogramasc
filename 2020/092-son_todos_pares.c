
#include <stdio.h>

/* 
Escribir un programa que pida por teclado números enteros
mayores que cero (acaba de pedir números cuando se 
introduzca un valor negativo o cero) y muestre por pantalla
"todos pares" cuando todos los números introducidos sean
pares y "hay impares" en caso contrario.

Variante: Modificar para que no siga pidiendo números
si se encuentra un número impar
*/

int main() {
    int numero, todo_pares;
    /* Vamos a suponer adiccionalmente que todo pares de 
    un conjunto vacío es verdadero. */
    todo_pares = 1;
    scanf("%d", &numero);
    while ( numero > 0 ) {
        /* todos_pares = todos_pares && ( numero % 2 == 0 ); */
        if ( numero % 2 != 0 ) { /* Me encuentro un impar */
            todo_pares = 0;
        }
        scanf("%d", &numero);        
    }
    printf("todo_pares: %d\n", todo_pares);
    if ( todo_pares ) { /* Es lo mismo: todo_pares == 1 */
        printf("Todo pares\n");
    }
    else {
        printf("Hay algún impar\n");
    }
    return 0;
}

        /* MAL!!!!!
        if ( numero % 2 == 0 ) {
            todo_pares = 1;
        }
        else {
            todo_pares = 0;
        }
        MAL !! */
