
#include <stdio.h>

/* Función f para elevar al cuadrado */
double f(double x) {
    return x*x;
}

/* 
    Calcule la integral de Riemann desde a = 1 hasta b = 2
    en k intervalos (parámetro formal)
*/
double riemann( unsigned k /* param formal */) {
    int i;
    double a = 1, b = 2;
    double d = (b - a) / k;
    double suma = 0;
    for ( i = 0; i <= k-1; ++i ) {
        suma = suma + d * f(a + i * d);
    }
    return suma;
}

int main() {
    double res;

    res = riemann(4);
    printf("Riemann: %.14f\n", res);

    res = riemann(1000000);
    printf("Riemann: %.14f\n", res);

    return 0;
}