
#include <stdio.h>

int main() {
    int N = 7;
    scanf("%d", &N);
    printf("La N va a ser: %d\n", N);

    /* ESto está mal si quitamos las llaves
    if ( N >= 0 ) {
        if ( N % 2 == 0 )
            printf("PAR");
    }
    else
        printf("Negativo");
    */

    if ( N >= 0 && N % 2 == 0 ) {
        printf("PAR\n");
    }

    if ( N < 0) {
        printf("Negativo\n");
    }


    return 0;
}
