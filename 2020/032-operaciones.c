
#include <stdio.h>

int main() {
    int a = 235, b = 4, c = -4;
    double x = 3.4, y = -10.7;
    int res;
    double res_f;
    res = a + b + c + 6;
    res_f = x - y;
    printf("res= %d, res_f= %f\n", res, res_f);

    res = ((a + b) * c + 4) * 2;
    res_f = (x - y) / 3.4;
    printf("res= %d, res_f= %f\n", res, res_f);

    res = a % 13; /* Resto de la division entera */
    res_f = (x - y) / 3.4;
    printf("res= %d, res_f= %f\n", res, res_f);

    return 0;
}
