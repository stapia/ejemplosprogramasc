
/* Preprocesador */
#include <stdio.h>

/* Declaracion */
int main() {

    /* Declaracion */
	const int a = 10; /* const int b;  */
	double x, y = 3;
	/* float a; Error no puedo repetir */

	/* Asignaciones */
	y = 10.4;
	x = 1 + y;

    /* a = 4;  No porque es constante */
    /* b = 3;  No porque es constante */

	/* Sentencias */
	printf("a: %d\n", a);
	printf("x: %f, y: %f\n", x, y);

	/* Sentencia */
	return 0;
}
