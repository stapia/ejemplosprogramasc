
#include <stdio.h>

double extraeMaximo(double *v /*double v[]*/, int n, double x) {
    int i; 
    double max = v[0];
    int i_max = 0;
    for ( i = 1; i < n; ++i ) {
        if ( v[i] > max ) {
            max = v[i];
            i_max = i;
        }
    }
    for ( i = i_max; i < n - 1; ++i ) {
        v[i] = v[i+1];
    }
    v[n-1] = x;

    return max;
}

int main() {
    double v[11] = { 2.3, 14.5, 74.5, 22.1, 9.6, 4.5, 56.7, 9.8, -8.4, 0.7 };
    int i, n = 10;

    double maximo = extraeMaximo(v, n, -1.3);

    for ( i = 0; i < n; ++i ) {
        printf("%5.1f ", v[i]);
    }
    printf("Y el máximo era: %f\n", maximo);

    /* 
        Tiene que resultar en v con valores:
        {2.3, 14.5, 22.1, 9.6, 4.5, 56.7, 9.8, -8.4, 0.7, -1.3 }  */

    return 0;
}