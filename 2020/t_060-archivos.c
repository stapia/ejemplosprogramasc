
#include <stdio.h>

/*

| '1' | 'A' | 'x' | ' ' | #13 | #10 | 'g' | '+' | '$ | 'Ñ' | EOF

EOF: End Of File, no exite como tal

# <=> number 

*/

int main() {
    int bytes = 0;
    char letra;

    scanf("%c", &letra);

    if ( letra == '\n' || letra == 'ñ' ) { /* Esto no compila por estar en UTF-8 */
        bytes = bytes + 2;
    }

    printf("'%c'\n", letra);

    return 0;
}