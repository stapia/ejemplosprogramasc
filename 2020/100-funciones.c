
#include <stdio.h>

int dato = 22; /* NO usar, pero esto es una declaración global */

void mostrarZ(double Re, double Im) 
{
    printf("z = %f + %f * i \n", Re, Im);
    /* return; Valido, pero innecesario */
}

double media3(double x, double y, double z) 
{
    double media;
    media = (x + y + z) / 3.0;
    return media; /* valido pero innecesario (media) */
    /* Mas breve: return (x + y + z) / 3.0; */
}

double sumar(double x, double y) 
{
    return x + y; 
}

/* No compila: double z = sumar(3,4); */

double sumar3(double x, double y, double z) {
    /* return sumar(x,y) + z; */
    return sumar( sumar(x,y), z);
} 

double elevar_al_cubo(double x) {
    return x*x*x;
}

int signo(int numero) {
    if ( numero < 0 ) {
        printf("Es negativo");
        return -1; /* Interrumpe la ejecución de la función */
    }

    if ( numero > 0 ) {
        printf("Es positivo");
        return +1;
    }
    
    printf("Es cero");
    return 0;
}

int main(/* void */)
{
    int a = 3; double res;

    res = elevar_al_cubo(2.4);
    printf("2.4 elevado al cubo es: %f\n", res);
    
    mostrarZ(12, a + 2.3);
    mostrarZ(3, a + 3.3);
    mostrarZ(a + 1.0, a + 2.3);

    res = 10 * media3( a, 3.56, 10);
    printf("Media es: %f\n", res);

    /* printf("Media es: %f\n", media); Mal porque media no está en 
    este ámbito */

    return a + 1;
}