
#include <stdio.h>

struct robot {
    double masa;
    double x[2];
    double v[2];
    double a[2];
};

/*
    struct robot s1;
    a.masa

    struct robot *s2;
    a->masa
*/

void actualizar(struct robot * r, double f[2], double inc_t) {
    /* 
    F = m * a -> a = F / m
    inc x = v0 * inc_t + (1/2) * a * inc_t^2 
    inc v = a * inc_t */
    int i;
    for ( i = 0; i < 2; ++i ) {
        r->a[i] = f[i] / r->masa;
        /* r->x[i] += r->v[i] * inc_t + r->a[i] * inc_t * inc_t / 2.0; */
        r->x[i] += r->v[i] * inc_t + 0.5 * r->a[i] * inc_t * inc_t;
        r->v[i] += r->a[i] * inc_t;
    }
}

int main() {
    struct robot c3po; /*  = { 2, { 1, 2.2}, ... }; */
    double fuerza[2] = {  1.2, -2.2 };

    c3po.masa = 2; 
    c3po.x[0] = 1;
    c3po.x[1] = 2.2;
    c3po.v[0] = 0;
    c3po.v[1] = 0.2;
    c3po.a[0] = 0.5;
    c3po.a[1] = -2.2;

    actualizar(&c3po, fuerza, 0.001);

    return 0;
}