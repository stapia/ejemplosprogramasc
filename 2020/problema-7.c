/************************************************/
/* 
	FUNDAMENTOS DE PROGRAMACION - Curso 2019-20
	SIMULACRO EXAMEN 28 de mayo

	Alumno: XXXX XXXXXXXXXX XXXXXXXXXX 
	Número de matricula: YYYYY
	DNI: ZZZZZZZZZZZ 
*/
/************************************************/

/*
	El alumno debe escribir la respuesta en los lugares indicados 
	mediante comentarios:
	
	"INICIO código fuente respuesta del ALUMNO"

	"FIN código fuente respuesta del ALUMNO"

	Busque "ALUMNO" para encontrarlos facilmente. Está prohibido modificar o manipular el resto del archivo.
*/

/* Problema 7

Escribir un programa que utilice memoria dinámica para guardar un número arbitrario de 
palabras que el usuario debe introducir por teclado siguiendo las instrucciones más abajo.
*/

#include <stdio.h>
#include <stdlib.h> 
/* INICIO código fuente respuesta del ALUMNO */

/* FIN código fuente respuesta del ALUMNO */

int main() {
/*
Apartado 1:

	a) En primer lugar el usuario introduce el número de palabras que va a escribir,
	b) el programa debe usar memoria dinámica para almacenar las cadenas introducidas por
	el usuario, 
	c) se puede suponer una longitud de palabra inferior o igual a 15 caracteres.
*/

/* INICIO código fuente respuesta del ALUMNO */
	char **v_cadenas; /* Vector de cadenas <=> Matriz dinámica (vector de punteros a filas) */
	int numero_palabras, i;
	scanf("%d", &numero_palabras);
	v_cadenas = (char**)malloc(numero_palabras*sizeof(char*));
	for ( i = 0; i < numero_palabras; ++i ) {
		v_cadenas[i] = (char*) malloc(16*sizeof(char));
		scanf("%15s", v_cadenas[i]);
	}
/* FIN código fuente respuesta del ALUMNO */

/*
Apartado 2:
	d) El programa debe volver a mostrar las cadenas de una en una, en bucle
	y separandolas, al escribir, por un salto de línea 
	e) y liberar la memoria dinámica.
*/

	/* INICIO código fuente respuesta del ALUMNO */
	for ( i = 0; i < numero_palabras; ++i ) {
		printf("out: %s\n", v_cadenas[i]);
	}

	for ( i = 0; i < numero_palabras; ++i ) {
		free(v_cadenas[i]);
	}
	free(v_cadenas);
	/* FIN código fuente respuesta del ALUMNO */

	return 0;
}
