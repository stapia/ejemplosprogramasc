
#include <stdio.h>

int main() {
    const double So = 3; /* m */
    const double Vo = 2; /* m/s */
    const double a = -2.0; /* m/s² */
    double t, s;
    printf("Introduce tiempo en segundos\n");
    scanf("%lf", &t);
    /* MAL !!!!
    s = So + Vo * t + (1/2)*a*t^2;
    */
    s = So + Vo * t + a*t*t / 2.0;
    printf("S = %.3f m\n", s);
    return 0;
}
