
#include <stdio.h>

int main() {
    double anguloEnGrados;
    printf("Introduce el angulo en [0,180] en º\n");
    scanf("%lf", &anguloEnGrados);

    if ( anguloEnGrados < 90 ) {
        printf("Agudo\n");
    }
    /* Ojo mejor fabs(anguloEnGrados - 90) < 1e-14 */
    else if ( anguloEnGrados == 90 )
    {
        printf("Recto\n");
    }
    /* Puedo añadir los que me hayan falta
    else if ( ) {
        printf("\n");
    }
    */
    else {
        printf("Obtuso\n");
    }

    return 0;
}
