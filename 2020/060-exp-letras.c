
#include <stdio.h>

int main() {
    char letra;
    char result, res1, res2;
    int num;

    scanf("%d", &num);
    /* scanf("%c", &letra); (Mal)*/
    scanf(" %c", &letra);
    printf("letra = ##%c##\n", letra);

    /* Si la letra es minúscula, obtiene la misma
    letra en mayúscula */
    result = letra - 'a' + 'A';
    printf("result = ##%c##\n", result);

    /* Si num está entre 0 y 9 inclusives obtiene
    la letra correspondiente entre '0' y '9' */
    res1 = num + '0';
    printf("res1 = ##%c##\n", res1);

    /* Si num está entre 10 y 15 inclusives obtiene
    la letra correspondiente entre 'A' y 'F' */
    res2 = num -10 + 'A';
    printf("res2 = ##%c##\n", res2);

    /* Y ahora todo junto */
    result = res1 * (num <= 9 ) + res2 * (num >= 10);
    printf("result = ##%c##\n", result);

    result = res1 * (num >= 0 && num <= 9 ) +
             res2 * (num >= 10 && num <= 15 );
    printf("result = ##%c##\n", result);

    return 0;
}
