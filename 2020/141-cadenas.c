
#include <stdio.h>
/* Atención incluimos string.h para la funciones con str */
#include <string.h>

/* 
    Escribir un programa que: */

int main() {

    /* 
        a) Declare 3 cadenas: dos de ellas con tamaño 32 y 36. La otra sin tamaño
        explícito, pero con valor inicial ". Sayonara, baby!" */
    char una[32], dos[36];
    char tres[] = ". Sayonara, baby!";
    int i, longitud;

    /* 
        b) Guarde dos valores que introduzca el usuario por teclado en la primera 
        y segunda cadenas. */
    scanf("%31s %35s", una, dos); /* una <=> &(una[0]) */ 

    /*
        c) Muestre el valor de las tres cadenas, en un único printf. Cada cadena
        en una línea de texto y precedidas por 1. 2. 3. */
    printf("1: '%s'\n2: $%s$\n3: #%s#\n", una, dos, tres);
    
    /*
        d) Muestre las letras de la segunda cadena, una letra por línea de texto. */
    for ( i = 0; dos[i] != '\0'; ++i ) {
        printf("%c\n", dos[i]); /* dos es cadena dos[i] es char */
    }

    /*
        e) Muestre por pantalla la longitud de la primera cadena. */

    longitud = strlen(una); /* len = strlen(una); */
    printf("Longitud: %d\n", longitud);

    /*
        f) Compruebe si la primera y segunda cadenas son iguales y lo muestre
        por pantalla */ 

    if ( strcmp(una, dos) == 0 ) { /* MALLL una == dos */
        printf("Iguales\n");
    }
    else {
        printf("Distintas\n");
    }

    /*
        g) Compruebe si se puede guardar la concatenación de la primera y tercera
        cadenas en la segunda. Si se puede, el programa lo hace, si no se puede 
        debe poner en la segunda cadena "Si problemo". dos = una + tres */ 

    if ( strlen(una) + strlen(tres) + 1 <= 36 ) {
        strcpy( dos, una); /* No vale strcat(dos, una) */
        strcat( dos, tres);
    } else {
        strcpy(dos, "Si problemo");
    }
    printf("dos: %s\n", dos);

    return 0;
}