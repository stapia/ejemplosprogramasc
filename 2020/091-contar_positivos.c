
#include <stdio.h>

/* 
Escribir un programa que cuente y muestre por pantalla
el número de enteros positivos entre 5 valores (enteros)
que se piden al usuario por teclado.
*/

int main() {
    int x, contador, i;

    contador = 0;
    for( i = 1; i <= 5; ++i ) {
        scanf("%d", &x);
        contador = contador + ( x > 0 );
        /* Quizas esta es más sencilla 
        if ( x > 0 ) {
            contador = contador + 1;
        }
        */ 
    }

    printf("Número de positivos es %d\n", contador);
    return 0;
}
