
#include <stdio.h>

/* 
    Marzo 2019 
    5. Completar el programa para que calcule y muestre por pantalla el valor aproximado
    del coseno de un ángulo dado mediante su serie de Taylor. La serie se debe calcular 
    hasta el término n = 10 inclusive. El ángulo a usar en la serie debe ser 
    obligatoriamente la mitad del ángulo dado, por tanto, hay que utilizar la  fórmula  
    del  coseno  doble  para  calcular  el  resultado  cos(2⋅𝛼)= 2⋅cos^2(𝛼)−1. 
    Nota:  es  muy  conveniente calcular cada término de la serie en función del 
    término anterior. Se recomienda hacer algún ejemplo a mano para comprobar 
    cómo se hace ese cálculo. 
    cos(𝑥) = 1 +∑(−1)^𝑛 ⋅ 𝑥^2𝑛 / (2⋅𝑛)!, el sumatorio desde 𝑛=1 a ∞

    Una vez realizado el programa, convierta el código desarrollado en 
    una función. Compruebe el resultado comparando con los valores de
    la función cos para x entre 0 y 2 con incrementos de 0.1.
*/

/* Vamos a escribir algunos terminos de la serie:
1 + (n=1) -1 * x^2 / 2! + (n=2) x^4 / 4!  + (n=3) (-1) x^6 / 6! + ...

T3 = - x^6 / 6! 
   = (-1) * x^4 * x^2 / (6 * 5 * 4!)
   = T2 * (-1) * x^2 / (2*(n=3) * (2*(n=3)-1))
*/

#include<stdio.h> 
#include<math.h> 

double cos_por_serie(double alpha) {
    double x, Tn = 1, Serie = 1; 
    int n;
    x = alpha / 2;
    for ( n = 1; n <= 10; n++ ) {
        Tn = Tn * (-1) * x * x / ( (2*n) * (2*n-1) );
        Serie = Serie + Tn;
    }
    /* cos(x) es Serie y cos(𝛼)= 2⋅cos^2(𝛼/2) − 1, es 2⋅cos^2(x) − 1 */
    return 2 * Serie * Serie - 1;
}

int main()  {
    int i;
    double alpha = 1.0471975511965977; /* 60º en rad */
    double cos1 = 0, cos2 = 0;
    cos1 = cos(alpha);
    printf("cos1(60) = %.14f, cos2(60)= %.14f\n", cos1, cos2);
    printf("  Con cos                  Con Serie\n");
    for ( i = 0; i <= 20; ++i ) {
        alpha = 0.1 * i;
        printf("%18.14f \t %18.14f\n", cos(alpha), cos_por_serie(alpha));    
    }

    return 0;
}
