
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* 
    Escribir un programa que: 
*/

/* 
    a) Defina una macro que servirá posteriormente para indicar
    el tamaño de un vector.
*/
#define DIM 5

int main() {

    /* 
        b) Declare dos vectores de números en coma flotante utilizando la macro anterior
        como tamaño.
    */
   double a[DIM] = {1.1, 3.1, 5.1}, b[DIM];
   int i, r; 
   double x, y;
   double prod_escalar;

   int t = time(NULL);
   printf("El time es %d\n", t);
   srand(t);

    /* 
        c) Guarde en la primera componente de a un valor que introduzca 
        el usuario por teclado  */
    scanf("%lf", &(a[0]));

    /*
        d) Asigne valores aleatorios al resto de componentes, de manera que si el valor de la primera
        componente es X, los valores del resto deben de estar entre -X y +X. 
        Y muestre por pantalla el resultado. */

    x = a[0];
    for ( i = 1; i < DIM; ++i ) {
        r = rand();
        y = 1.0 * r / RAND_MAX; /* r está entre 0 y RAND_MAX */ 
        /* y está 0 y 1, al multiplicar pasa a estar entre 0 y 2 y al mulplicar
        pasa a estar entre 0 y 2X, al restar queda en [-X, +X] */
        a[i] = y * 2 * x - x; 
    }

    for ( i = 0; i < DIM; ++i ) {
        printf("a[%d] = %f\n", i+1, a[i]); /* Cuidado se muestran los ¡indices +1! */
    }

    /*
        e) Asigne los valores del primer vector al segundo. */

    /*  b = a; MALLL!!!!! NO se pueden asignar */ 
    for ( i = 0; i < DIM; ++i ) {
        b[i] = a[i];
    }

    /*
        f) Reasigne los valores al segundo vector en orden inverso. 
        Y muestre por pantalla el resultado. */

    printf("--------- Vector b antes --------\n");
    for ( i = 0; i < DIM; ++i ) {
        printf("b[%d] = %f\n", i+1, b[i]);
    }

    for ( i = 0; i < DIM / 2; ++i ) {
        double aux = b[DIM - 1 - i]; 
        b[DIM - 1 - i] = b[i]; /* Da valor a b[DIM-1] para i = 0 */
        b[i] = aux; /* Da valor a b[0] para i = 0 */
    }

    printf("--------- Vector b despues --------\n");
    for ( i = 0; i < DIM; ++i ) {
        printf("b[%d] = %f\n", i+1, b[i]);
    }

    for ( i = 0; i < DIM; ++i ) {
        /* i = 0 => j = DIM - 1 -0 = DIM - 1 
           i = DIM - 1 => j = DIM - 1 - (DIM - 1) = 0 */
        int j = DIM - 1 - i; 
        b[j] = a[i];
    }

    printf("--------- Vector b despues --------\n");
    for ( i = 0; i < DIM; ++i ) {
        printf("b[%d] = %f\n", i+1, b[i]);
    }

    /*
        g) Calcule y muestre por pantalla el producto escalar de ambos vectores. */

    prod_escalar = 0;
    for ( i = 0; i < DIM; ++i ) {
        prod_escalar = prod_escalar + a[i] * b[i];
    }
    printf("Producto escalar = %f\n", prod_escalar);

    return 0;
}