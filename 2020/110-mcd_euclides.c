
#include <stdio.h>

/* 
Algoritmo de Euclides para el MCD.

Escribir una función para que devuelva (o retorne) el MCD
de dos números enteros. Escribir en el programa varias llamadas
a la función para que comprobar que la función es correcta. 
*/

/* 
Ejemplo1: 
    a    b    /     %
   78   24    3     6
   24    6    4     0
    6    0
   MCD = 6
Ejemplo2:
   92   56    1    36
   56   36    1    20
   36   20    1    16
   20   16    1     4
   16    4    4     0
   4     0
   MCD = 4 
*/

int mcd(int a, int b) {
    /* int cociente, resto; */
    int resto;

    while ( b != 0 ) {
        /* cociente = a / b; No es necesario */
        resto = a % b;
        a = b; /* ¡Mucho cuidado con el orden de las sentencias! */
        b = resto;
    }

    return a;
}

int main() {
    int divisor_max;
    int a; 

    a = 78;
    divisor_max = mcd(a, 24);
    printf("mcd(78,24) = %d\n", divisor_max);

    a = 90;
    divisor_max = mcd(a+2, 56);
    printf("mcd(92,56) = %d\n", divisor_max);

    return 0;
}
