
#include <stdio.h>

int main() {

    int i;
    int v[4] = { 1, 2, 3, 4 }; /* Qué es v? es un array */

    for ( i = 0; i < 4; ++i ) {
        scanf("%d", &(v[i])); 
    }

    for ( i = 0; i < 4; ++i ) {
        printf("%d", v[i]);
    }
    printf("\n");

    return 0;
}