
/* Escribir un programa que muestre por pantalla si 
una letra introducida por teclado es una vocal minúscula
o mayúscula, si no es ninguna de las dos cosas
mostrar un mensaje de error */

#include <stdio.h>

int main() {
    char letra;

    scanf(" %c", &letra); /* Mejor con un espacio delante! */

    switch ( letra ) /* Obligatoriamente una expresión "entera" */
    {
    case 'a': case 'e': case 'i': case 'o': case 'u':
        printf("La letra es una vocal minúscula\n");
        break;

    case 'A': case 'E': case 'I': case 'O': case 'U':
        printf("La letra es una vocal mayúscula\n");
        break;
    
    default:
        printf("Error: no es una vocal\n");
        break;
    }

    if ( letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' 
         || letra == 'u' ) {
        printf("La letra es una vocal minúscula\n");        
    }

    if ( 'a' <= letra && letra <= 'z' ) {
        printf("La letra es una minúscula\n");
    }

    return 0;
}