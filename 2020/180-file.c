
#include <stdio.h>
#include <stdlib.h>

int main() {

    /* 
        Comprendiendo scanf: Declara e inicializa una variable entera
        léela desde teclado, pero introduce una letra, luego lee la letra
        y muestra el resultado del número y la letra. Modifica para que 
        se compruebe el resultado */
    if ( 0 ) 
    {
        int num = 9999999, comprobacion;
        char letra = 'a';
        comprobacion = scanf("%d", &num);
        /* Un espacio en la cadena de formato quita los blancos */
        scanf(" %c", &letra); 
        printf("num: %d, letra: '%c' y comprobacion: %d\n", num, letra, comprobacion);
    }

    /*
        Comprendiendo scanf: intenta leer dos palabras, hay que 
        hacerlo en dos cadenas distintas */
    if ( 0 ) 
    {
        char cad1[80], cad2[80];
        scanf("%s%s", cad1, cad2);
        printf("cad1: '%s', cad2: '%s'\n", cad1, cad2);
    }

    /*
        Comprendiendo scanf: intenta leer dos números enteros separados
        por un punto */
    if ( 0 ) 
    {
        int num1, num2;
        scanf("%d.%d", &num1, &num2);
        printf("num1: %d, num2: %d\n", num1, num2);
    }

    /* 
        Escribir las sentencias que permitan: generar N letras
        aleatorias entre 'a' y 'z' y escriban en un archivo de nombre
        dado su número en la tabla ASCII y luego la propia letra. Un número
        y letra por línea del archivo. */

    if ( 0 ) {
        char filename[80]; FILE *g;
        /* El usuario introduce el nombre del archivo */
        scanf("%79s", filename); /* 79 = 80 - 1, es decir, tamaño cadena - 1 */
        /* Se abre el archivo */
        g = fopen(filename, "w"); 
        /* ó g = fopen("datos.txt", "w"); si es siempre el mismo */

        if ( g != NULL ) { /* Comprueba si se ha abierto correctamente */
            int i, N, aleatorio;
            char letra;
            scanf("%d", &N);
            for ( i = 0; i < N; ++i ) {
                /* Se genera un número aleatorio entre 0 y ('z' - 'a') */
                aleatorio = rand() % ('z' - 'a' + 1); 
                letra = 'a' + aleatorio; /* Y se obtiene la letra */
                fprintf(g, "%d %c\n", aleatorio, letra);
            }
            fclose(g);
        }
    }

    /* 
        Escribir las sentencias que permitan: leer y mostrar por pantalla
        el contenido del archivo del apartado anterior */

    if ( 1 ) {
        char filename[80]; FILE *g;
        /* El usuario introduce el nombre del archivo */
        scanf("%79s", filename); /* 79 = 80 - 1, es decir, tamaño cadena - 1 */
        /* Se abre el archivo */
        g = fopen(filename, "r"); 
        /* ó g = fopen("datos.txt", "w"); si es siempre el mismo */

        if ( g != NULL ) { /* Comprueba si se ha abierto correctamente */
            int num, ok; char letra;

            ok = fscanf(g, "%d %c", &num, &letra);
            while ( ok == 2 ) {
                /* Procesar una linea, es decir un número y una letra */
                printf("el número: %d y la letra: '%c'\n", num, letra);

                /* Leer la siguiente linea */
                ok = fscanf(g, "%d %c", &num, &letra);
            }
            fclose(g);
        }

    }

    return 0;
}