
#include <stdio.h>

int main() {
    unsigned char num = 255;

    printf("num: %d\n", num);

    num = num + 1; /* Hay que hacer la
    suma de 255 + 1 en binario y "cortar" */

    printf("num: %d\n", num);

	return 0;
}
