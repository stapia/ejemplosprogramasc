

#include <stdio.h>

int main() {
    int a = 3, b = 4, c = 2;
    int res;

    res = 0;
    res += a; /* res = res + a */
    printf("res= %d\n", res);

    res = res + a;
    printf("res= %d\n", res);

    ++res; /* res = res + 1 */
    printf("res= %d\n", res);

    res--; /* res = res - 1 */
    printf("res= %d\n", res);

    return 0;
}
