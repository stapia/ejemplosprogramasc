
#include <stdio.h>
#include <string.h>

/* 
    "Hola uno" -> Hola --- 
    "Hola dos" -> Hola --- 
    "Adios dos" -> Adios --- 
    "Este uno es otro ejemplo uno" -> Este uno es otro ejemplo ---
    "Hola Mundo" -> Hola Mundo
*/

void cambiar(char cadena[]) {
    int i; 
    printf("Cambiar\n");
    for ( i = 0 ; cadena[i] != '\0' && i < 100; ++i ) {
        if ( strcmp(cadena+i, "uno") == 0 || strcmp(cadena+i, "dos") == 0 ) {
            printf("Coincide e i = %d cad[i] = '%c'\n", i, cadena[i]);
            strcpy(cadena+i, "---");
        }
    }
}

int main() {
    char cadena[100] = "Este uno es otro ejemplo uno";
    char *p = cadena + 5; /* Apunta al primer uno */

    cambiar(cadena);
    printf("Cadena: '%s'\n", cadena);

    strcpy(p, "---");
    printf("Cadena: '%s'\n", cadena);
    
    return 0;
}