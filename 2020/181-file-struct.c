
#include <stdio.h>

/* 
    Define un tipo de dato que sirva para representar 
    un monomio, ai*x^i (un término de un polinomio) */

struct Monomio {
    double coef;
    int exponente;
};

/*
    Escribir una función que escriba un monomio en un archivo dado
    ya abierto. */
void escribir(FILE* g, struct Monomio mono) {
    fprintf(g, "%+.2f*X^%d ", mono.coef, mono.exponente);
}

/*
    Escribir una función que lea un monomio desde un archivo:
    en primer lugar el valor del coeficiente y luego el de 
    la potencia */

void leer(FILE* g, struct Monomio *mono) {
    /* mono->coef es lo mismo que (*mono).coef */
    if ( fscanf(g, "%lf %d", &(mono->coef), &(mono->exponente)) != 2 ) {
        /* No lo he podido leer */
        mono->coef = 0.0;
        mono->exponente = -1;
    }
}

/*
    Escribir una función que sume el valor de dos monomios. 
    Discutir que se puede hacer cuando no se pueda... */

struct  Monomio sumar(struct Monomio m1, struct Monomio m2 /*, int* error */ ) {
    struct Monomio resultado;
    if ( m1.exponente == m2.exponente ) {
        resultado.coef = m1.coef + m2.coef;
        resultado.exponente = m1.exponente;
    } else {
        /* ??? */
        resultado.coef = 0;
        resultado.exponente = -1;
    }
    return resultado;
}

/* 
    Definir un tipo de dato que sirva para almacenar un polinomio,
    es decir, una sucesión de monomios */

struct Polinomio {
    struct Monomio v_mono[10];
    int grado;
};

/* 
    Escribir una función que permita leer un polinomio desde 
    un archivo dado */
void leer_polinomio(FILE* g, struct Polinomio *p) {
    int i; 
    p->grado = -1;
    for ( i = 0; i < 10; ++i ) {
        /* p->mono[i] */
        leer(g, &(p->v_mono[i]) );

        if ( p->v_mono[i].exponente >= 0 && p->v_mono[i].exponente > p->grado ) {
            p->grado = p->v_mono[i].exponente;
        }
    }
}

/* 
    Escribir una función que permita escribir un polinomio en 
    un archivo dado */

void escribir_polinomio(FILE* g, const struct Polinomio *p) {
    int i;
    for ( i = 0; i < 10; ++i ) {
        if ( p->v_mono[i].exponente >= 0 ) {
            escribir(g, p->v_mono[i]);
        }
        
    }
    printf("\n");
}

/* 
    Escribir una función que permita sumar dos polinomios */

int main() {
    if ( 0 ) { /* Probando monomios */
        struct Monomio m;
        /* m.coef = 2.1; m.exponente = 3; */

        /* scanf("%d", &num); <=> fscanf(stdin, "%d", &num); */
        /* printf("num: %d\n", num); <=> fprintf(stdout, "num: %d\n", num); */

        leer(stdin, &m);
        escribir(stdout, m);
    }

    if ( 1 ) { /* Probando polinomio */
        struct Polinomio p;
        leer_polinomio(stdin, &p);
        printf("El grado del polinomio es: %d\n", p.grado);
        escribir_polinomio(stdout, &p);
    }
    
    return 0;
}