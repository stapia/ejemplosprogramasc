#include <stdio.h>

/* 
    a) Escribir una función que sume a una varible
    el valor de otra. 
*/

/* MALLLLL!!!!! 
void sumar(int a, int b) {
    a = a + b;
    printf("Dentro de la función: a: %d\n", a);
}
*/
void sumar(int *p, int b) {
    (*p) = (*p) + b;
}

/* 
    c) Escribir una función que intercambie el valor de
   dos variables. 
*/
void swap(int *p, int *q) {
    int aux;
    aux = *p;
    *p = *q;
    *q = aux;
}

/* 
    e) Escribir una función que ordena, de menor a mayor, 
    los valores de 3 variables. ¿Es mejor hacer otra función
    antes?
*/
void ordenar_2(int *p, int *q) {
    if ( (*p) > *q ) { /* La de la izquierda es mayor que la de la derecha */
        /* Intercambiar */
        swap(p, q);
    }
}

void ordenar_3(int *p, int *q, int *r) {
    ordenar_2(p, q);
    ordenar_2(q, r); /* Puedo haber cambiado el 2 */
    ordenar_2(p, q);
}

int main() {
    int x = 4, y = 6, z = -7;
    /*
        b) LLame a la función definida en a)
    */
    sumar(&x, y);
    printf("Después de llamar a la función: x = %d\n", x);
    /*
        d) LLame a la función definida en c)
    */
    swap(&x, &y);
    printf("Después de intercambiar: x = %d, y = %d\n", x, y);
    /*
        f) LLame a la función definida en e)
    */
    ordenar_3(&x, &y, &z);
    printf("Después de ordenar: x = %d, y = %d, z = %d\n", x, y, z);
    return 0;
}
