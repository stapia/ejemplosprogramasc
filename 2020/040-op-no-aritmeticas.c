
#include <stdio.h>

int main() {
    int a = 3, b = 4, c = 2;
    int res;

    res = a < b;
    printf("a < b -> %d\n", res);

    res = a == c;
    printf("a == c -> %d\n", res);

    /*
    Sintacticamente correcto pero
        ¡¡ NO hacerlo !!
    res = a = c;

    Más largo, pero más claro
    a = c;
    res = a;
    */

    res = a != c;
    printf("a != c -> %d\n", res);

    res = a <= c;
    printf("a <= c -> %d\n", res);

    res = (a <= c) && (b >= c);
    printf("(a <= c) && (b >= c) -> %d\n", res);

    res = (a <= c) || b;/* equivalente b != 0 */
    printf("(a <= c) || b -> %d\n", res);

    res = !(a <= c);
    printf("!(a <= c) -> %d\n", res);

    a = 13 + 16 * 1234;
    res = a & 15; /* and bitwise */
    printf("a & 15 -> %d\n", res);

    /* 4   -> 0... 0100 
      13   -> 0... 1101 
    4 | 13 -> 0... 1101 or bitwise  */
    a = 4;
    res = a | 13;
    printf("a(4) | 13 -> %d\n", res);

    res = a | 11;
    printf("a(4) | 11 -> %d\n", res);

    res = a ^ b; /* xor bitwise, uno u otro, pero no ambos */
    printf("a ^ b -> %d\n", res);

    res = ~a + 1; /* not bitwise */ 
    printf("~a + 1 -> %d\n", res);

    /* 13   -> 0... 1101 */
    a = 13;
    /* 13 << 5  -> (5 bit menos) 0... 110100000 en inglés shitf */
    res = a << 5;
    printf("a << 5 -> %d\n", res);

    return 0;
}
