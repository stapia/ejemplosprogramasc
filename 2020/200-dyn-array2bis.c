
#include <stdio.h>
#include <stdlib.h>

/* 
    Completar la función para que:
    a) Dado el nombre de un archivo, lo abra
    b) Lea del archivos los dos números enteros que indican 
    las filas y columnas de la matriz 
    c) Cree las variables dinámicas necesarias para guardar 
    las componentes de la matriz dada en forma de vector de 
    componentes colocado por filas
    d) Lea los valores de las componentes del archivo
    e) Devuelva la dirección de la matriz dinámica 
    f) ¿Qué pasa con el tamaño? */

double* leer(const char* filename, int *rows, int* cols ) {
    FILE* g;
    int filas, columnas;
    double *matriz = NULL;

    g = fopen(filename, "r");
    if ( g != NULL ) {
        double x; int i, j;
        fscanf(g, "%d %d", &filas, &columnas);
        *rows = filas;
        *cols = columnas;
        matriz = (double*)malloc(filas*columnas*sizeof(double)); 
        for ( i = 0; i < filas; ++i ) {
            for ( j = 0; j < columnas; ++j ) {
                fscanf(g, "%lf", &x);
                matriz[i*columnas+j] = x;
            }
        }
        fclose(g);
    }
    else {
        printf("No se ha podido abrir '%s'\n", filename);
    }
    return matriz;
}

/* 
    Muestra los valores de una matriz dinámica dada
    por un vector de componentes colocadas por filas y su tamaño. */
void mostrar(double *matriz, int rows, int cols) {
    int i, j;
    for ( i = 0; i < rows; ++i ) {
        for ( j = 0; j < cols; ++j ) {
            /* Nota: matriz[i] es el puntero que apunta
            al primer elemento de una fila */
            printf("%+.2f ", matriz[i*cols+j]);
        }
        printf("\n");
    }
}

int main() {
    double *mat; int filas, columnas;
    mat = leer("200-dyn-array2.txt", &filas, &columnas);
    mostrar(mat, filas, columnas);
    free(mat);
    return 0;
}