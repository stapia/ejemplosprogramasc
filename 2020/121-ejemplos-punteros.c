
#include <stdio.h>

/* 
    Escribir un programa que:
*/

int main() {
/*
    a) Declare 2 variables (cualquier tipo, por ejemplo, int) 
       y un puntero
    b) Lea el valor de ambas desde teclado
*/
    int a, b; int *p;

    scanf("%d %d", &a, &b);

/*
    c) Compruebe cuál es la mayor entre ellas, guarde su dirección
       en el puntero, la multiple por 2 y muestre el resultado.
*/
    if ( a > b ) {
        p = &a;
    }
    else {
        p = &b;
    }
    *p = (*p) * 2;

    printf("La mayor multiplicada por 2 es %d\n", *p); /* *p es int */

/*
    d) Compruebe cuál es la menor, guarde su dirección en un puntero,
       lea un nuevo valor para ella y muestre el nuevo valor
*/
    if ( a < b ) {
        p = &a;
    }
    else {
        p = &b;
    }
    printf("La menor es %d, introduce un nuevo valor\n", *p); /* *p es int */
    scanf("%d", p);
    printf("El nuevo valor es %d\n", *p); /* *p es int */

/*
    e) Compruebe si la segunda variable es par, en ese caso 
    guarde su dirección en el puntero, guarde NULL en caso contrario
    f) Luego escriba el valor de la variable o "NULL"
*/
    if ( b % 2 == 0 ) {
        p = &b;
    }
    else {
        p = NULL;
    }

    if ( p == NULL ) {
        printf("Null\n");
    }
    else {
        printf("La variable es par con valor %d\n", *p);
    }
    
    return 0;
}