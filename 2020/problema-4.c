/************************************************/
/* 
	FUNDAMENTOS DE PROGRAMACION - Curso 2019-20
	SIMULACRO EXAMEN 28 de mayo

	Alumno: XXXX XXXXXXXXXX XXXXXXXXXX 
	Número de matricula: YYYYY
	DNI: ZZZZZZZZZZZ 
*/
/************************************************/

/*
	El alumno debe escribir la respuesta en los lugares indicados 
	mediante comentarios:
	
	"INICIO código fuente respuesta del ALUMNO"

	"FIN código fuente respuesta del ALUMNO"

	Busque "ALUMNO" para encontrarlos facilmente. Está prohibido modificar o manipular el resto del archivo.
*/

/* Problema 4 

Dada la variable cla de tipo struct clase. Escribir la función intro para que asigne 
valor por teclado a los miembros de dicha variable. El miembro nf sólo puede tomar 
valor 0 ó 1, si nf es 0 el miembro final toma el valor de "suspenso" y si nf es 1 
la cadena toma el valor de "aprobado".
*/

#include <stdio.h>
#include <string.h>

struct clase {
	int matricula;
	float nota;
	int nf;
	char final[40];
};

/* INICIO código fuente respuesta del ALUMNO */

struct clase intro() {
	struct clase result;
	scanf("%d %f", &(result.matricula), &(result.nota));
	result.nf = result.nota >= 5.0;
	if ( result.nf ) {
		strcpy(result.final, "aprobado");
	}
	else {
		strcpy(result.final, "suspenso");
	}
	return result;
}

/* FIN código fuente respuesta del ALUMNO */

#ifndef OTRO_MAIN
int main() {
	struct clase cla;
	cla = intro();
	/* Otras operaciones por ejemplo: */
	printf("%f\n", cla.nota);
	/* ... */
	return 0;
}
#endif /* OTRO_MAIN */