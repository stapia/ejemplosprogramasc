
#include <stdio.h>

int main() {
    int num = 234;
    int res;
    double x = 3;

    /* Escribe una expresion cuyo resultado sea 1 (verdadero)
    cuando num sea par y 0 (falso) en caso contrario */
    res = (num % 2) == 0;
    printf("Es par: %d", res);

    /* Escribe una expresion cuyo resultado sea 1 (verdadero)
    cuando cuando x pertenece al intervalor [3.3, 5] y 0 (falso)
    en caso contrario */
    res = ( 3.3 <= x ) && ( x <= 5.0 );
    printf("Dentro el intervalo: %d", res);

    return 0;
}
