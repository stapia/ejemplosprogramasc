
#include <stdio.h>

struct Datos {
    double masa;
    double coord[3];
};

void mostrar(struct Datos st) {
    int i;
    printf("masa: %f\n", st.masa);
    printf("coordenadas: ");
    for ( i = 0; i < 3; ++i ) {
        printf("%f ", st.coord[i]);
    }
    printf("\n");
}

void incrementar_coord_y(struct Datos *p_st) {
    p_st->coord[1] += 10;
}

int main() {

    struct Datos dat; /* Válido, pero arriesgado = { 3.3214, { 1, 2, 3 } }; */

    dat.masa = 3.0213;
    /* dat.coord = { 1, 2, 3 }; MAL!! */
    dat.coord[0] = 34.342;
    dat.coord[1] = 0.342;
    dat.coord[2] = 14.354;

    mostrar(dat);

    incrementar_coord_y(&dat);

    mostrar(dat);

    return 0;
}