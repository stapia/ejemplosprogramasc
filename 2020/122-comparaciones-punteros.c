
#include <stdio.h>

/*  
    e) Escribir una función que, dados dos punteros, compruebe 
       si apuntan a la misma variable y muestre por pantalla 
       si es así o no.
*/
void comprobar_punteros_iguales(int* px, int* py) {
    if ( px == py ) {
        printf("Apuntan a la misma variable\n");
    }
    else {
        printf("Apuntan a variables distintas\n");
    }
}

/* 
    Escribir un programa que:
*/

int main() {
/*
    a) Declare 2 variables (cualquier tipo, por ejemplo, int) 
       y dos punteros
    b) Guarde la dirección de cada variable respectivamente en 
       cada puntero
    c) Compruebe si los punteros apuntan a la misma variable 
       (evidentemente no)
*/
    int a = 10, b = 20; int *p, *q; 

    p = &a;
    q = &b;

    if ( p == q ) {
        printf("Apuntan a la misma variable\n");
    }
    else {
        printf("Apuntan a variables distintas\n");
    }
    
/*
    d) Asigne el valor de un puntero al otro y haga la misma
       comprobación. ¿Es lo mismo que utilizar & ?
*/
    p = q; /* el valor de q es &b, la dirección de b */

    if ( p == q ) {
        printf("Después: apuntan a la misma variable\n");
    }
    else {
        printf("Después: Apuntan a variables distintas\n");
    }

/*
    f) Llame a la función definida anteriormente con distintas
    combinaciones de los punteros y resultados de & 
*/

    printf("Son p y q iguales?\n");
    comprobar_punteros_iguales(p,q);
    printf("Es p igual a si mismo?\n");
    comprobar_punteros_iguales(p,p);
    printf("Es p igual a la dirección de a?\n");
    comprobar_punteros_iguales(p,&a);

    return 0;
}