
#include <stdio.h>
/*
    372 es un número (Complemento a 2) 

    "372" no es un número, está ASCII y null terminated string */

int main() {

    int x = 83;
    char cad_num[3] = ""; /* 3 -> 2 cifras más nulo */

    /* No es lo mismo '0', '1', '2' que 0, 1, 2 */
    cad_num[0] = '0' + x % 10; 
    /* A la inversa: x = cad_num[0] - '0' luego se multiplica y suma */
    cad_num[1] = '0' + (x / 10) % 10;
    /* cad_num[2] = '\0'; */

    printf("Como string: '%s'\n", cad_num);

    return 0;
}