
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define PI 3.1416

/* 
    Definir una struct que guarde los datos 
    de una orden de movimiento de un robot */
struct orden {
    char codigo; /* 'A' ó 'G' */
    double valor; /* Avances o giro en º */
};

/* 
    Escribir una función que guarde (modifique) en el struct 
    anterior una orden de movimiento leída de un
    archivo dado (ya abierto). Si se encuentra una línea
    de comentario debe devolver 0, si tiene éxito debe
    devolver 1 y EOF en cualquier otro caso */

int leer(FILE* g, struct orden *ord) {
    int ok;
    ok = fscanf(g, " %c", &(ord->codigo) );
    if ( ok != 1 ) {
        return EOF;
    }
    else if ( ord->codigo == '#' ) {
        return 0;
    }
    else if ( ord->codigo == 'G' || ord->codigo == 'A' ) {
        fscanf(g, "%lf", &(ord->valor));
        return 1;
    }
    else {
        printf("Error código desconocido: '%c'\n", ord->codigo);
        return  EOF;
    }
}

/* 
    Escribir una función que lea (para quitarla) 
    una línea de texto de un archivo dado (ya abierto) */

void quitar_linea(FILE* g) {
    char letra; int ok;
    ok = fscanf(g, "%c", &letra);
    while ( ok == 1 && letra != '\n' ) {
        ok = fscanf(g, "%c", &letra);
    }
}

/* 
    Escribir una función que, dado un archivo,
    devuelva una estructura con el puntero que apunta a
    un vector dinámico de ordenes y su tamaño */

struct vector_ordenes {
    struct orden* p_orden;
    int N;
};

struct vector_ordenes leer_ordenes(FILE* g) {
    struct vector_ordenes result; 
    int ok, i = 0; int tam = 16;
    result.p_orden = (struct orden*) malloc(tam*sizeof(struct orden));
    /* result.p_orden[i] es un struct orden y cada struct orden es
       un codigo y un valor */
    ok = leer(g, &(result.p_orden[i]));
    while ( ok != EOF ) {
        if ( ok == 0 ) { /* Es una línea de comentario */
            quitar_linea(g);
        }
        else {
            ++i;
        }
        /* Copiar a un nuevo vector más grande */
        if ( i >= tam ) {
            /* Duplicamos la capacidad del vector */
            struct orden* antiguo = result.p_orden; int j;
            result.p_orden = (struct orden*) malloc(2*tam*sizeof(struct orden));
            /* Copiamos el antiguo */
            for ( j = 0; j < tam; ++j ) {
                result.p_orden[j] = antiguo[j];
            }
            /* Y liberamos */
            free(antiguo);
            tam = 2 * tam;
        }
        ok = leer(g, &(result.p_orden[i]));
    }
    result.N = i;
    return result;
}

void escribir_ordenes(FILE* g, struct vector_ordenes ordenes) { 
    int i = 0;
    for ( ; i < ordenes.N; ++i  ) {
        printf("Orden con código: '%c' y valor: %f\n", ordenes.p_orden[i].codigo, ordenes.p_orden[i].valor);
    }  
}

/* 
    Define un struct para guardar la "posición" de un robot */
struct posicion_robot {
    double x[2];
    double angulo; 
};

/* 
    Escribir una función que transforme un vector de
    ordenes de movimiento en posiciones del robot.
    La posición y orientación iniciales se dan como parámetros.
    Adicionalmente se debe calcular y modificar una variable
    que se pase por referencia con el número de posiciones calculadas */
struct posicion_robot* calcular_posiciones(
    int *p_N, struct vector_ordenes ordenes, 
    double x0[2], double angulo0 ) 
{
    struct posicion_robot * result;
    int i, j;
    *p_N = ordenes.N + 1;
    result = (struct posicion_robot *) malloc((*p_N) * sizeof(struct posicion_robot));

    for ( j = 0; j < 2; ++j ) {
        result[0].x[j] = x0[j];
        result[0].angulo = angulo0;
    }
    
    for ( i = 0; i < *p_N - 1; ++i ) {
        if ( ordenes.p_orden[i].codigo == 'G' )  {
            result[i+1].angulo = result[i].angulo + ordenes.p_orden[i].valor; 
            result[i+1].x[0] = result[i].x[0];
            result[i+1].x[1] = result[i].x[1];
        }
        else if ( ordenes.p_orden[i].codigo == 'A' ) {
            result[i+1].angulo = result[i].angulo; 
            result[i+1].x[0] = result[i].x[0] + ordenes.p_orden[i].valor*20*cos(PI*result[i].angulo/180);
            result[i+1].x[1] = result[i].x[1] + ordenes.p_orden[i].valor*20*sin(PI*result[i].angulo/180);
        }                
    }
    return result;
}

/* 
    Escribir una función para que, dado un archivo (ya abierto), escriba
    en él las posiciones que toma un vector de posiciones del robot dado. */

void escribir_posiciones(FILE* g, struct posicion_robot* pos, int N) {
    int i;
    for ( i = 0; i < N; ++i ) {
        /* 700 es la altura del SVG, hace falta poner 
           el - para invertir el eje Y */
        fprintf(g, "%.1f,%.1f ", pos[i].x[0], 700 - pos[i].x[1]);
    }
    fprintf(g, "\n");
}

int main() {
    struct vector_ordenes ordenes;
    struct posicion_robot *posiciones;
    double x0[2] = { 300, 100 };
    double angulo0 = 90; 
    int N;
    FILE *g;

    g = fopen("211-robot-sencillo.txt", "r");
    if ( g != NULL ) {
        ordenes = leer_ordenes(g);
        escribir_ordenes(stdout, ordenes);
        posiciones = calcular_posiciones(&N, ordenes, x0, angulo0);
        escribir_posiciones(stdout, posiciones, N);
        free(posiciones);
        free(ordenes.p_orden);
        fclose(g);
    }
    return 0;
}