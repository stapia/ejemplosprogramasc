
#include <stdio.h>

/* 
Escribir un programa que calcule y muestre por pantalla
el máximo de 5 números en coma flotante que se piden al
usuario por teclado.
*/

int main() {
    double x, maximo;
    int i;

    scanf( "%lf", &x );
    maximo = x;

    for ( i = 0; i < 4; i = i + 1 /* ++i o i++ */) {
        scanf( "%lf", &x );
        if ( x > maximo ) {
            maximo = x;
        }
    }
    printf("Maximo: %f", maximo);

    return 0;
}
