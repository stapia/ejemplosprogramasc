
#include <stdio.h>

/* Un programa que pide al
usuario 5 números en coma flotante
y muestra la suma de todos ellos */

int main() {
    double suma, dato;
    int i;
    suma = 1000; /* Por supuesto esto es 0 */
    for ( i = 0; i < 5; i = i + 1 ) {
        scanf("%lf", &dato);
        suma = suma + dato;
    }
    printf("Suma: %f\n", suma);
    printf("Valor de i: %d\n", i);
    return 0;
}
