
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define PI 3.1416

/* 
    Definir una struct que guarde los datos 
    de una orden de movimiento de un robot */
struct orden {
    char codigo[3];
    int valor;
};

/* 
    Escribir una función que guarde (modifique) en el struct 
    anterior una orden de movimiento leída de un
    archivo dado (ya abierto). Si se encuentra una línea
    de comentario debe devolver 0, si tiene éxito debe
    devolver 1 y EOF en cualquier otro caso */

int leer(FILE* g, struct orden *ord) {
    int ok;
    ok = fscanf(g, "%2s", ord->codigo );
    if ( ok != 1 ) {
        return EOF; /* #define EOF -1 */
    }
    else if ( ord->codigo[0] == '#' ) {
        return 0;
    }
    else if ( strcmp(ord->codigo, "G") == 0 || strcmp(ord->codigo, "AM") == 0 ) {
        int num;
        fscanf(g, "%d", &num);
        ord->valor = num;
        return 1;
    }
    else if ( strcmp(ord->codigo, "A") == 0 ) {
        return 1;
    }
    else {
        printf("Error código desconocido: '%s'\n", ord->codigo);
        return  EOF;
    }
}

/* 
    Escribir una función que lea (para quitarla) 
    una línea de texto de un archivo dado (ya abierto) */

void quitar_linea(FILE* g) {
    char letra; int ok;
    ok = fscanf(g, "%c", &letra);
    while ( ok == 1 && letra != '\n' ) {
        ok = fscanf(g, "%c", &letra);
    }
}

/* 
    Escribir una función que, dado un archivo,
    devuelva un nuevo vector dinámico con todas las ordenes de 
    movimiento contenidas en el archivo. El final de las ordenes de 
    movimiento se debe indicar con una cadena vacia en el
    código de movimiento guardado en el vector */
struct orden* leer_ordenes(FILE* g) {
    struct orden* result; 
    int ok, i = 0, N = 16;
    result = (struct orden*) malloc(N*sizeof(struct orden));
    /* result[i] es un struct orden y cada struct es un codigo y un valor */
    ok = leer(g, &(result[i]));
    while ( ok != EOF ) {
        if ( ok == 0 ) { /* Es una línea de comentario */
            quitar_linea(g);
        }
        else {
            ++i;
        }
        /* Copiar a un nuevo vector más grande */
        if ( i >= N ) {
            /* Duplicamos la capacidad del vector */
            struct orden* antiguo = result; int j;
            result = (struct orden*) malloc(2*N*sizeof(struct orden));
            /* Copiamos el antiguo */
            for ( j = 0; j < N; ++j ) {
                result[j] = antiguo[j];
            }
            /* Y liberamos */
            free(antiguo);
            N = 2 * N;
        }
        ok = leer(g, &(result[i]));
    }
    strcpy(result[i].codigo, "");
    return result;
}

void escribir_ordenes(FILE* g, struct orden* ordenes) { 
    int i = 0;
    while ( strcmp(ordenes[i].codigo, "") != 0 ) {
        printf("Orden con código: %s y valor: %d\n", ordenes[i].codigo, ordenes[i].valor);
        ++i;
    }  
}

/* 
    Define un struct para guardar la "posición" de un robot */

struct posicion_robot {
    double x[2];
    int angulo; /* 0, 90, 180 y 270 */
    /* bien, bien: double angulo; */ 
};

/* 
    Escribir una función que transforme un vector de
    ordenes de movimiento en posiciones del robot.
    La posición y orientación iniciales se dan como parámetros.
    Adicionalmente se debe calcular y modificar una variable
    que se pase por referencia con el número de posiciones calculadas */
struct posicion_robot* calcular_posiciones(
    int *p_N, struct orden* ordenes, 
    double x0[2], int angulo0 ) 
{
    struct posicion_robot * result;
    int i = 0, j = 0;
    while ( strcmp(ordenes[i].codigo, "") != 0 ) {
        ++i;
    }
    *p_N = i + 1;
    result = (struct posicion_robot *) malloc((*p_N) * sizeof(struct posicion_robot));

    for ( j = 0; j < 2; ++j ) {
        result[0].x[j] = x0[j];
        result[0].angulo = angulo0;
    }
    
    for ( i = 0; i < *p_N - 1; ++i ) {
        if ( strcmp(ordenes[i].codigo, "G") == 0  ) {
            /* Aumento o disminuyo de 90 en 90 */
            result[i+1].angulo = result[i].angulo + ordenes[i].valor * 90; 
            result[i+1].x[0] = result[i].x[0];
            result[i+1].x[1] = result[i].x[1];
        }
        else if ( strcmp(ordenes[i].codigo, "A") == 0 ) {
            result[i+1].angulo = result[i].angulo; 
            result[i+1].x[0] = result[i].x[0] + 20*cos(PI*result[i].angulo/180);
            result[i+1].x[1] = result[i].x[1] + 20*sin(PI*result[i].angulo/180);
        }        
        else if ( strcmp(ordenes[i].codigo, "AM") == 0 ) {
            result[i+1].angulo = result[i].angulo; 
            result[i+1].x[0] = result[i].x[0] + ordenes[i].valor*20*cos(PI*result[i].angulo/180);
            result[i+1].x[1] = result[i].x[1] + ordenes[i].valor*20*sin(PI*result[i].angulo/180);
        }                
    }
    return result;
}

/* 
    Escribir una función para que, dado un archivo (ya abierto), escriba
    en él las posiciones que toma un vector de posiciones del robot dado. */

void escribir_posiciones(FILE* g, struct posicion_robot* pos, int N) {
    int i;
    for ( i = 0; i < N; ++i ) {
        fprintf(g, "%.1f,%.1f ", pos[i].x[0], pos[i].x[1]);
    }
    fprintf(g, "\n");
}

int main() {
    struct orden *ordenes;
    struct posicion_robot *posiciones;
    double x0[2] = { 200, 500 };
    int angulo0 = 270; /* De 90 en 90 */
    int N;
    FILE *g;

    g = fopen("210-robot-mov.txt", "r");
    if ( g != NULL ) {
        ordenes = leer_ordenes(g);
        escribir_ordenes(stdout, ordenes);
        posiciones = calcular_posiciones(&N, ordenes, x0, angulo0);
        escribir_posiciones(stdout, posiciones, N);
        free(posiciones);
        free(ordenes);
        fclose(g);
    }
    return 0;
}