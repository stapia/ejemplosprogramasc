
#include <stdio.h>
#include <math.h>

int main() {
    int a = 3, b = 4, c = 2;
    int res; double res_f;
    res = (a + b) / c;
    res_f = (a + b) / c;
    printf("res= %d res_f= %f\n", res, res_f);

    res = 1 / 2;
    res_f = 1 / 2.0;
    printf("res= %d res_f= %f\n", res, res_f);

    res = 1 / 2.0;
    res_f = 1.0 / 2;
    printf("res= %d res_f= %f\n", res, res_f);

    /* Excepción de coma flotante (`core' generado)
    c = 0;
    res = a / c;
    */
    res_f = a / 0.0;
    printf("res= %d res_f= %f\n", res, res_f);

    res_f = log(-1.0);
    printf("res= %d res_f= %f\n", res, res_f);

    return 0;
}
