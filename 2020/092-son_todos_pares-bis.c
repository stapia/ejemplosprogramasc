
#include <stdio.h>

/* 
Escribir un programa que pida por teclado números enteros
mayores que cero (acaba de pedir números cuando se 
introduzca un valor negativo o cero) y muestre por pantalla
"todos pares" cuando todos los números introducidos sean
pares y "hay impares" en caso contrario.

Variante: Modificar para que no siga pidiendo números
si se encuentra un número impar
*/

int main() {
    int numero, todo_pares;
    todo_pares = 1;
    do {
        scanf("%d", &numero);
        if ( numero > 0 ) {
            todo_pares = todo_pares && ( numero % 2 == 0 ); 
        }
    } while ( numero > 0 );

    if ( todo_pares ) { /* Es lo mismo: todo_pares == 1 */
        printf("Todo pares\n");
    }
    else {
        printf("Hay algún impar\n");
    }
    return 0;
}
