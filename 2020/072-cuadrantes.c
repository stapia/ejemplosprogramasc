

#include <stdio.h>

int main() {
    double x, y; double angulo;
    const double PI = 3.141592654;
    scanf("%lf%lf", &x, &y);

    if ( (x >= 0) && ( y > 0 ) ) {
        printf("Cuadrante 1\n");
        angulo = 180*atan(y/x)/PI;
    }
    else if ( (x < 0) && (y >= 0) ) {
        printf("Cuadrante 2\n");
        angulo = 180 + 180*atan(y/x)/PI;
    }
    else if ( x <= 0 && y < 0) {
        printf("Cuadrante 3\n");
        angulo = 180 + 180*atan(y/x)/PI;
    }
    else {
        printf("Cuadrate 4\n");
        angulo = 270 + 180*atan(y/x)/PI;
    }

    printf("Angulo = %f\n", angulo);
    return 0;
}
