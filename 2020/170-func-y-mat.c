
#include <stdio.h>
#include <string.h>

#define DIM 3

/* 
    Muestra una matriz de nf filas y DIM columnas */
void mostrar(int A[/*Es libre*/][DIM], int nf) {
    int i,j;
    for ( i = 0; i < nf; ++i) {
        for (j = 0; j < DIM;  ++j) {
            printf("%4d ", A[i][j]);
        }
        printf("\n");
    }
}

/* 
    Escribir una función que, dada una matriz cuadrada, calcule su transpuesta */

void trans(int A[DIM][DIM] /* Se supone filas = DIM, también int A[DIM][DIM] */ ) {
    int i, j; int aux;
    for ( i = 1; i < DIM; ++i ) {
        for ( j = 0; j < i; ++j ) {
            aux = A[i][j];
            A[i][j] = A[j][i];
            A[j][i] = aux;
        }
    }

}

/* 
    Escribir una función que, dado un vector de cadenas alfanumáricas de 
    tamaño 80, muestre por pantalla cada una de ellas en una línea nueva. 
    Modifique la función para que cambie la primera letra a mayúsculas.
    Luego modifique la función para que muestre la longitud de cada cadena entre
    paréntesis después de mostrar cada cadena.*/

void mostrar_cadenas(char v_cad[][80], int tam) {
    int i;
    for ( i = 0; i < tam; ++i ) {
        if ( 'a' <= v_cad[i][0] && v_cad[i][0] <= 'z' ) {
            v_cad[i][0] = v_cad[i][0] - 'a' + 'A';
        }
        printf("%s (%ld)\n", v_cad[i], strlen(v_cad[i]));
    }
}

/* 
    Escribir el programa principal para que llame y compruebe el 
    funcionamiento de las funciones anteriores */

int main() {
    int matA[DIM][DIM] = { { 11, 12, 13 }, { 21, 22, 23 }, { 31, 32, 33 } };
    /* Implícitamente es char v_cad[3][80] */
    char v_cad[][80] = { "buenos", "días", "mundo", "este año" };

    mostrar(matA, 2);
    printf("\n");

    trans(matA);

    mostrar(matA, DIM);

    mostrar_cadenas(v_cad, 4);

    return 0;
}
