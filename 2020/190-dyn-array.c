
#include <stdio.h>
#include <stdlib.h>

int main() {
    /* 
    Guardar tam enteros en un array siendo tam el valor 
    de una variable que se pide al usuario ¿!Se puede!? */
    int i, tam; 
    int *vector; /* Esto está mal: int vector[tam] */ 

    scanf("%d", &tam);
    vector = (int*) malloc(tam*sizeof(int));

    /* 
    Generar números del 1 al 6 aleatorios */
    for ( i = 0; i < tam; ++i ) {
        vector[i] = rand() % 6 + 1;
    }

    /* 
    Modificar los números sumando a cada uno el valor del siguiente */
    for ( i = 0; i < tam - 1; ++i ) {
        vector[i] = vector[i] + vector[i+1];
    }

    /* 
    Mostrar el resultado */
    for ( i = 0; i < tam; ++i ) {
        printf("%d\n", vector[i]);
    }

    /* 
    ¿Hay que hacer algo más? Sí! */
    free(vector);

    /* Ya no debo poner nada con vector[i] */

    return 0;
}