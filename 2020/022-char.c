
#include <stdio.h>

int main() {
    char letra_1 = '1';
    char letra_2 = '\n';

    signed char num = 'A'; /* es lo mismo que 65; */
    signed char otro;

    otro = num + 1;

    printf("letra 1: $%c$, 2: '%c'\n", letra_1, letra_2);

    printf("num: %c, %d\n", num, num);
    printf("otro: %c, %d\n", otro, otro);

	return 0;
}
