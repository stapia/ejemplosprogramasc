
#include <stdio.h>

/* Problema 8 */
int* genVec(int *matricula) {
    int *result;
    int num = *matricula, suma = 0, i;
    while ( num > 0 ) {
        suma = suma + num % 10;
        num = num / 10;
    }
    result = (int*) malloc(suma*sizeof(int));
    for ( i = 0; i < suma; ++i ) {
        /* Si suma es 15 y rand() es 46 => el resto es 1 
           en general, el resto está entre 0 y 14 
           Si suma 15 y rand() es 39 => el resto de rand() % 16 es 7,
           en general, el resto está entre 0 y 15 */
        result[i] = rand() % (suma + 1);
    }
    *matricula = suma;
    return result;
}

int main() {
    int matricula = 15531, i;
    int *v;
    /* srand(time(NULL)); */
    v = genVec(&matricula);
    for ( i = 0; i < matricula; ++i ) {
        printf("%d ", v[i]);
    }
    free(v);
    return 0;
}
