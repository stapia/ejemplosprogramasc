
#include <stdio.h>

/*
Cumplir al menos una de
1 : x es multiplo de 7
2 : x entre 30 y 35 inclusives
3 : La última cifra de x es 5
*/

int main() {
    int x, result;
    printf("Introduce x\n");
    scanf("%d", &x);
    result =
        ( x % 7 == 0 ) /* 1 */
     || ( x >= 30 && x <= 35) /* 2 */
     || ( x % 10 == 5 ); /* 3 */
    printf("cumple = %d\n", result);

    result = x ^ 15;
    printf("negar 4 últimos bits = %d\n", result);

    result = x ^ ( 15 << 4 ) ;
    printf("negar bits del 5 al 8 últimos = %d\n", result);

    result = x / 100 % 10;
    printf("centenas = %d\n", result);

    return 0;
}
