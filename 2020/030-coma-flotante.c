
#include <stdio.h>
#include <math.h>

int main() {
    double x = 3.1234512351234512345;
    double y = 4e-12, z;

    printf("x = %.16f, y = %.10e\n", x, y);

    x = sin(3.14);
    y = sin(3.1416);

    printf("x = %.16f, y = %.10e\n", x, y);

    x = 1e14;
    y = 1e-2;
    z = x + y;

    printf("z = %.20e\n", z);

    return 0;
}
