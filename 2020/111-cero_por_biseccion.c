/* 
    Algoritmo de la bisección para encontrar el cero 
    de una función (se basa en el teorema de bolzano).

    Escribir una función que obtenga el valor de x, tal que
    que x está en [a, b] y sen(x) = 0. Siendo a y b parámetros
    de la función. Compruebe el resultado obteniendo el valor
    de PI con varios valores de a y b por ejemplo 3 y 4 ó 5 y 2.
*/
#include <stdio.h>
#include <math.h>

double cero_seno(double a, double b) {
    double medio; 
    while ( fabs(b - a) > 1e-8 ) {
        medio = (a + b) / 2.0;
        if ( sin(medio) * sin(a) < 0 ) { /* sin(medio)*sin(b) > 0 */
            /* El nuevo intervalo es [a, medio] */
            b = medio;
        }
        else {
            /* El nuevo intervalo es [medio, b] */
            a = medio;
        }
    }
    return medio; /* a ó b están en 1e-8 */
}

int main() {
    /* Calculamos PI de otra manera, por ejemplo con el acos de 1/2 que es pi/3: */
    double Pi1, Pi2 = 0, cero;
    Pi1 = 3*acos(0.5);
    Pi2 = cero_seno(3,4);
    printf("Pi con el acos: %.14f, Pi con biseccion: %.14f\n", Pi1, Pi2);
    Pi2 = cero_seno(5.01,2.1);
    printf("Pi con el acos: %.14f, Pi con biseccion: %.14f\n", Pi1, Pi2);
    cero = cero_seno(-1.5,2.1);
    printf("cero: %.14f\n", cero);
    return 0;
}
