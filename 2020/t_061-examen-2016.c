
#include <stdio.h>

#include <stdio.h>
#define DIM 6

void crear_archivo(const char filename[], int A[DIM][DIM]) {
    FILE *g = fopen(filename, "w");
    if ( g != NULL ) {
        int i, j;
        for ( i = 0; i < DIM; ++i ) {
            for ( j = 0; j <= i; ++j ) {
                if ( A[i][j] != 0 ) {
                    fprintf(g, " %d %d %d;", i, j, A[i][j]);
                }
            }
            fprintf(g, "\n");
        }
        fclose(g);
    }
    else {
        printf("Error no se ha podido abrir el archivo: '%s'", filename);
    }
}

int main( ) {
    int mat[DIM][DIM] = { { 0 } };

    mat[0][0] = -2;
    mat[3][2] = 5;
    mat[3][3] = 1;
    
    /* Se dan valores a mat… */
    crear_archivo("matriz.txt", mat);
    return 0;
}
