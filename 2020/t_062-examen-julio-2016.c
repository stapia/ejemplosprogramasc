
/* Problema 1
a b c  res
0 1 0  1
0 1 1  1
*/

/* Problema 2 */

#include <stdio.h>
#include <string.h>

/* Suponemos que solo me dan numeros de matricula */
void suma1(char *mat) {
    int i; /* strlen mejor no */
    for ( i = 0; mat[i] != '\0'; ++i ) {
        if ( mat[i] == '9' ) {
            mat[i] = '0';
        }
        else {
            mat[i] += 1; 
        }
    }
}

struct variablefisica { char nombre[10]; double valor; char unidad[10]; };

/* void medir(struct variablefisica*); */
void medir(struct variablefisica* st) {
    int ok;
    /* st->nombre <=> (*st).nombre */
    ok = scanf("%9s %lf %9s", st->nombre, &(st->valor), st->unidad);
    if ( ok != 3 ) {
        printf("Error de lectura\n");
    }
    /* Pedir primera letra del unidad */
    /* scanf("%c", &(st->unidad[0]) ); */
}

int main() {   
    /* Problema 3 */
    struct variablefisica ensayo;
    medir(&ensayo);
    printf("%s %f %s\n", ensayo.nombre, ensayo.valor, ensayo.unidad);

    scanf("%9s %lf %9s", ensayo.nombre, &(ensayo.valor), ensayo.unidad);

    /* Problema 2 */
    {
        char matricula[] = "90373";
        suma1(matricula);
        printf("'%s'\n", matricula);

        strcpy(matricula, "15829");
        suma1(matricula);
        printf("'%s'\n", matricula);
    }

    {
        /* Problema 1 */
        int a = 1, b = 1, c = 0;
        int res;
        /* res = (a==0) && (b==1) && (c==0) || (a==0) && (b==1) && (c==1) */
        res = (!a && b && !c) || (!a && b && c);
        printf("res = %d\n", res);
    }

    return 0;
}