  
#include <stdio.h>

/* Comentario:

   El compilador los ignora 
*/

int main() {
	
	/* Comment out 
	int a b; */
	
	int a = 4, b = -10;
	double mi_dato = 2.1, 
	       miDato = 1.23e-10,  
		   x1=0.0,
		   _1 = -10e3; 
	/* Este no vale como identficador: 1x */
	
	printf("Hola \n \"Mundo' 2\t13");
	
	return 0;
}
