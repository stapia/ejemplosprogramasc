
/* This example is a adaptation of the examples
 * in the tutorial found http://www.lighthouse3d.com */

#include <GL/glut.h>

void changeSize(int w, int h) {
    float ratio;
    
    if (h == 0) h = 1;

	ratio =  w * 1.0 / h;

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	glViewport(0, 0, w, h);

	gluPerspective(45.0f, ratio, 0.1f, 100.0f);

	glMatrixMode(GL_MODELVIEW);
}

float angle = 0.0f;

void renderScene(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();

	gluLookAt(	0.0f, 0.0f, 10.0f,
				0.0f, 0.0f,  0.0f,
				0.0f, 1.0f,  0.0f);

	glRotatef(angle, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);
		glVertex3f(-2.0f,-2.0f, 0.0f);
		glVertex3f( 2.0f, 0.0f, 0.0);
		glVertex3f( 0.0f, 2.0f, 0.0);

		glVertex3f(-2.0f,-2.0f, 1.0f);
		glVertex3f( 2.0f, 0.0f, 1.0);
		glVertex3f( 0.0f, 2.0f, 1.0);
	glEnd();

	glutSwapBuffers();
}

void idleFunction(void) {
   	angle+=0.1f;
    glutPostRedisplay();
}

int main(int argc, char **argv) {
    
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(50,50);
	glutInitWindowSize(620,620);
	glutCreateWindow("-- Ejemplo GLUT --");

	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutIdleFunc(idleFunction);

	glutMainLoop();

	return 0;
}
