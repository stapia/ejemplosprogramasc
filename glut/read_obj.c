
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_VERTEX 1000
#define MAX_FACE 2000

void changeSize(int w, int h) {
  float ratio;
  if (h == 0) h = 1;
  ratio =  w * 1.0 / h;

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glViewport(0, 0, w, h);
  gluPerspective(95.0f, ratio, 0.1f, 100.0f);
  glMatrixMode(GL_MODELVIEW);
}

float angle = 0.0f;

int vertices = 0;
int faces = 0;

float vertex_array[MAX_VERTEX][3];
int face_array[MAX_FACE][4];
int face_type[MAX_FACE]; /* 3 = triangle, 4 = quad */

void renderScene(void) {
  int i;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  gluLookAt(	0.0f, 0.0f, 10.0f,
              0.0f, 0.0f,  0.0f,
              0.0f, 1.0f,  0.0f);

  glTranslatef(0.0f, -6.0f, 0.0f);
  glRotatef(angle, 0.0f, 1.0f, 0.0f);

  glBegin(GL_TRIANGLES);
      for ( i = 0; i < faces; ++i )
      {
        if ( face_type[i] == 3 ) {
          int j;
          for ( j = 0; j < 3; ++j )
            glVertex3f(vertex_array[face_array[i][j]][0],
                       vertex_array[face_array[i][j]][1],
                       vertex_array[face_array[i][j]][2]);
        }
      }
  glEnd();

  glBegin(GL_QUADS);
      for ( i = 0; i < faces; ++i )
      {
        if ( face_type[i] == 4 ) {
          int j;
          for ( j = 0; j < 4; ++j )
            glVertex3f(vertex_array[face_array[i][j]][0],
                       vertex_array[face_array[i][j]][1],
                       vertex_array[face_array[i][j]][2]);

        }
      }
  glEnd();

  glutSwapBuffers();
}

void idleFunction(void) {
   	angle+=0.1f;
    glutPostRedisplay();
}

void load_file( FILE* g) {
  char line[256];
  float *v;
  int *index;
  char code;

  while ( fgets(line, 256, g) != NULL ) {
    if ( sscanf(line," %c ", &code) == 1 ) {
      switch ( code ) {
        case 'v':
          v = vertex_array[vertices];
          if ( sscanf(line, "v %f %f %f", v, v+1, v+2) == 3 ) {
            ++vertices;
          }
        break;
        case 'f':
          index = face_array[faces];
          /* Example line: f 1/1/1 15/25/2 16/27/3 3/3/4 */
          face_type[faces] = sscanf(line, " f %i/%*i/%*i %i/%*i/%*i %i/%*i/%*i %i/%*i/%*i",
                                    index, index+1, index+2, index+3);
          if ( face_type[faces] == 3 || face_type[faces] == 4 ) {
            ++faces;
          }
        break;
        default:
          ;
      }
    }
  }

}

void load_obj(const char* filename) {
    FILE *g = fopen(filename, "r");
    if ( g != NULL ) {
        load_file( g );
        printf("Loaded file: '%s' with %i vertices and %i faces\n", filename, vertices, faces);
        fclose(g);
    }
    else {
        fprintf(stderr, "file: '%s' not found\n", filename);
    }
}

void cleanup() {
    printf("Exiting main loop (cleaning up)\n");
}

int main(int argc, char **argv) {
  atexit( cleanup );

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
  glutInitWindowPosition(50,50);
  glutInitWindowSize(620,620);
  glutCreateWindow("-- Ejemplo GLUT --");

  glutDisplayFunc(renderScene);
  glutReshapeFunc(changeSize);
  glutIdleFunc(idleFunction);

  load_obj("MaleLow.obj");

  printf("Entering main loop ...\n");
  glutMainLoop();

  return 1;
}
