
## Hacer un pequeño programa en GLUT ##

Para Linux es más sencilla la instalación y compilación de todo lo 
necesario, por eso lo vamos a suponer que tenemos un Linux tipo 
Debian - Ubuntu para instalar y compilar.

### Instalar ###

El comando apt-get install sirve para instalar todo tipo de librerías y
aplicaciones en nuestro caso necesitamos hacer:

```bash
    sudo apt-get install freeglut3 freeglut3-dev
```

donde `sudo` significa *super user do* y freeglut3 y freeglut3-dev son 
los paquetes que necesitamos con las librerías de C.

### Compilar ###

Para compilar tenemos que añadir las librería que usamos el comando para
compilar queda:

```bash
    gcc programa.c -o programa.run -ansi -pedantic -Wall -lm -lGL -lglut -lGLU
```

Y ya está. 

En esta carpeta se encuentra un programa básico para trabajar con GLUT.
