

/* Esto es double (*A)[5] Vector a fila */
/* NO es double *A[5] vector de punteros */
double traza(double A[5][5]) {
	
	double suma = 0; int i;
	
	for ( i = 0; i < 5; ++i ) {
		/* Traza diagonal secundaria */
		suma += A[i][5-1-i];
	}
	
	return suma; 
}

/* NO se puede retornar array!! */ 
void suma_filas(double A[][5], unsigned tam, 
				double *V) {
					
	int i, j;
	for ( i = 0; i < tam; ++i ) {
		V[i] = 0;
		for ( j = 0; j < 5; ++j ) {
			V[i] += A[i][j];
		}
	}
}



int main() {
	return 0;
}
