
#include <stdio.h>

void nada(int x, int y) {
	int aux;
	aux = x;
	x = y;
	y = aux;
}

void swap(int *x, int *y) {
	int aux;
	aux = *x;
	*x = *y;
	*y = aux;
}

void ordenar(int *x, int *y, int *z) {
	if ( *x > *y ) {
		swap(x,y);
	}
	if ( *y > *z ) {
		swap(z,y);
	}
	if ( *x > *y ) {
		swap(x,y);
	}	
}

int main() {
	int a = 3, b = 15, c = 7;
	nada(a,b);
	printf("a = %d, b = %d\n", a, b);
	swap(&a,&b);
	printf("a = %d, b = %d\n", a, b);

	printf("a = %d, b = %d, c= %d\n", a, b, c);
	ordenar(&a,&b,&c);
	printf("a = %d, b = %d, c= %d\n", a, b, c);
	return 0;
}
