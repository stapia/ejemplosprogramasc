
#include <string.h> 
#include <stdio.h> 

int main() {
	
	char palabra[64];
	int igual;
	
	scanf("%63s", palabra);
	
	igual = strcmp("hola", palabra);
	
	if ( igual == 0 ) {
		printf("Iguales, palabra es 'hola'\n");
	}
	else if ( igual == -1 ) {
		printf("Más 'grande' que 'hola'\n");
	}
	else {
		printf("Más 'pequeña' que 'hola'\n");
	}
	return 0;
}
