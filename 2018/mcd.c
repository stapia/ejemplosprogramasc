
#include <stdio.h>

int main() {
	int x, y, resto = 1;
	
	scanf("%d %d", &x, &y);
	
	while ( resto != 0 ) {
		resto = x % y;
		x = y;
		y = resto;
	}
	
	printf("MCD: %d\n", x);
	
	return 0;
}
