
#include <stdio.h>
#include <stdlib.h>

#define VECES (50+50)
#define PI 3.1416
#define TEXTO "Genero %d numeros aleatorios\n"

int main() {
	
	int i;
	int aleatorio;
	double x;
	
	srand( 10 );
	printf(TEXTO, VECES);
	printf("RandMax = %d\n", RAND_MAX);
	
	for ( i = 0; i < VECES; ++i ) 
	{
		aleatorio = rand() % 100 + 1;
		x = rand() * 1.0 / RAND_MAX;
		printf("%3d %11.8f", aleatorio, x);
		if ( (i+1) % 6 == 0 ) {
			printf("\n");
		}
	}
	
	return 0;
}
