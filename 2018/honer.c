
#include <stdio.h>

#define GRADO_MAX 5

int main() 
{
	int i; double x, suma;
	double P[GRADO_MAX+1];
	int grado_1 = GRADO_MAX+1;
	
	scanf("%lf", &x);
	
	for ( i = 0; i < grado_1; ++i ) 
	{
		scanf("%lf",  &(P[i]) );
	}
	
	suma = 0;
	for ( i = GRADO_MAX; i >= 0; --i ) 
	{
		suma = suma * x + P[i];
	}
	
	printf("P(%f) = %f\n", x, suma);
	return 0;
}
