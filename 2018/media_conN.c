
#include <stdio.h>

int main() {
	int i, N; double x, resultado;
	scanf("%d", &N);
	resultado = 0;
	for ( i = 0; i < N; ++i ) 
	{
		scanf("%lf", &x);
		resultado = resultado + x;
	}
	resultado /= N;
	printf("La media es %.2f\n", resultado);
	return 0;
}
