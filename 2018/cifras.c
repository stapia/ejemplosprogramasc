
#include <stdio.h>

int main() {
	int num; 
	scanf("%d", &num);
	
	while ( num > 9 ) 
	{
		printf("%d\n", num % 10);
		num = num / 10;
	}
	printf("%d\n", num);
	
	return 0;
}
