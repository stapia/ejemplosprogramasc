
#include <stdio.h>

#define DIM 10

int main() 
{
	int i = 0;
	double V[DIM];
	
	for ( ; i < DIM; ++i ) 
	{
		scanf("%lf",  &(V[i]) );
		i + 1; 
	}

	for ( i = 0; i < DIM; ++i ) 
	{
		V[i] = V[i] * 2;
	}
	
	for ( i = 0; i < DIM-1; ++i ) 
	{
		printf("%5.2f, ", V[i]);
	}
	printf("%5.2f\n", V[DIM-1]);
	
	for ( i = 1; i < DIM; i = i + 2 ) 
	{
		V[i] = V[i] + 0.3;
	}
	
	for ( i = 0; i < DIM-1; ++i ) 
	{
		printf("%5.2f, ", V[i]);
	}
	printf("%5.2f\n", V[DIM-1]);
	
	for ( i = 0; i < DIM; ++i ) 
	{
		if ( V[i] > 10.0 ) {
			V[i] = V[i] - 10.0;
		}
	}

	for ( i = 0; i < DIM-1; ++i ) 
	{
		printf("%5.2f, ", V[i]);
	}
	printf("%5.2f\n", V[DIM-1]);
	
	for ( i = 1; i < DIM; ++i ) 
	{
		V[i] = V[i] + V[i-1];
	}

	for ( i = 0; i < DIM-1; ++i ) 
	{
		printf("%5.2f, ", V[i]);
	}
	printf("%5.2f\n", V[DIM-1]);

	return 0;
}
