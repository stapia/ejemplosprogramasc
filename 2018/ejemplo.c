#include <stdio.h>

int main() {
	
	int a = 35, resultado;
	double x = 3.45;
	
	scanf("%d", &a);
	resultado = a / 10;	
	printf("El resultado es: %d \n", resultado);
	resultado = a % 10;
	printf("El resultado es: %d \n", resultado);

	/* resultado = 2 * x; */
	printf("El resultado es: %f \n", 2*x);
	
	return 0;
}
