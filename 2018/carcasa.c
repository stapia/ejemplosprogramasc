
#include <stdio.h>

double carcasa(const char *ruta, char mat) 
{
	FILE *g; double result = 0;
	
	g = fopen( ruta, "r");
	
	if ( g != NULL ) {
		char mat_pieza; 
		int num;
		double masa;
		char nombre[30];
		int sc;
		
		sc = fscanf(g," %c %d %lf %29s", 
		          &mat_pieza, &num, &masa, nombre);
		          
		while ( sc == 4 ) {
			if ( mat_pieza == mat ) {
				result += masa * num;
			}
			
			sc = fscanf(g," %c %d %lf %29s", 
					  &mat_pieza, &num, &masa, nombre);
		}
		
		if ( sc != EOF ) {
			printf("Error\n");
		}
		
		fclose( g );
	}
	else {
		printf("No se puede abrir '%s'\n", ruta);
	}
	return result;
}

int main() {
	printf("La masa es: %f gramos\n", 
		carcasa("pieza.txt", 'P'));
	
	return 0;
}
