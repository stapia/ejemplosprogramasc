
#include <stdio.h>
#include <math.h>

int main() {
	unsigned char num1 = 23, num2 = 34, num3 = 3;
	double resultado;
	
	resultado = (num1 + num2 / 256.0) * pow(2, num3);
	
	printf("El número es: %f\n", resultado);
	
	return 0;
}
