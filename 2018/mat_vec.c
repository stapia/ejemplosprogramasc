
#include <stdio.h>

#define DIM 3

int main() {
	int i, j; double prod;
	double A[DIM][DIM], x[DIM], b[DIM];
	
	for ( i = 0; i < DIM; ++i ) 
	{
		prod = 0;
		for ( j = 0; j < DIM; ++j ) 
		{
			prod += A[i][j] * x[j];
		}
		b[i] = prod;
	}

	return 0;
}
