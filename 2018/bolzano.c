
#include <math.h>
#include <stdio.h>

int main() {
	
	double a=3, b=4, m;
	
	while ( fabs(b - a) > 1e-13 ) {
		m = (b + a) / 2;
		/* Signo de sin(m) es igual que signo sin(a) */ 
		if ( sin(m) * sin(a) > 0 ) { 
			a = m;
		}
		else {
			b = m;
		}
	}
	printf("%.15f\n", m);
	
	return 0;
}
