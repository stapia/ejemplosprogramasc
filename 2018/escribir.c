

#include <stdio.h>

void escribe(FILE* gg, double x, double y) 
{
	fprintf(gg, "X: %f, Y: %f\n", x, y);
}

int main() {
	FILE *g;
	double y = 2.0;
	
	g = fopen("Datos.txt", "w"); 
	
	if ( g != NULL ) {
		/* Escribo */
		fprintf(g, "Hola mundo\n");
		fprintf(g, "Hoy el programa %d\n", 2*33);
		
		escribe(g, 3.4, y);
		
		fclose( g );
	} 
	else 
	{
		printf("No he podido abrir el archivo\n");
	}
	return 0;
}
