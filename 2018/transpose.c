
#include <stdio.h>

#define DIM 3

int main() {
	
	int i, j, aux;
	int A[DIM][DIM] = { {1,2,3}, {4,5,6}, {7,8,9} };
	
	/* Traspone la matriz */
	
	for ( i = 0; i < DIM-1; ++i )  
	{
		for ( j = i+1; j < DIM; ++j )  
		{
			aux = A[i][j];
			A[i][j] = A[j][i];
			A[j][i] = aux;
		}
	}	
	
	/* Muestra la matriz */
	for ( i = 0; i < DIM; ++i )  
	{
		for ( j = 0; j < DIM; ++j )  
		{
			printf("%4i ", A[i][j]);
		}
		printf("\n");
	}
	
	return 0;
}
