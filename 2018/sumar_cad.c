

#include <stdio.h>

void sumar(const char *num1, const char *num2, char* result)
{
    int i = 0, llevada = 0;
    while ( num1[i] != 0 && num2[i] != 0 )
    {
        if ( num1[i] + num2[i] - '0' + llevada <= '9' )
        {
            result[i] = num1[i] + num2[i] + llevada - '0';
            llevada = 0;
        }
        else
        {
            result[i] = num1[i] + num2[i] + llevada - '0'- 10;
            llevada = 1;
        }
        ++i;
    }

    if ( num1[i] != 0 )
    {
      while ( num1[i] != 0 ) {
          if ( num1[i] + llevada <= '9' )
          {
              result[i] = num1[i] + llevada;
              llevada = 0;
          }
          else
          {
              result[i] = num1[i] + llevada - 10;
              llevada = 1;
          }
          ++i;
      }
    }
    else if ( num2[i] != 0 )
    {
      while ( num2[i] != 0 ) {
          if ( num2[i] + llevada <= '9' )
          {
              printf("%c ; %s\n", num2[i], result);
              printf("num2 %s \n", num2);
              result[i] = num2[i] + llevada;
              llevada = 0;
          }
          else
          {
              result[i] = num2[i] + llevada - 10;
              llevada = 1;
          }
          ++i;
      }
    }

    if ( llevada != 0 ) {
        result[i] = '1';
        ++i;
    }
    result[i] = 0;
}

int main() {
    char num1[] = "327245";
    char num2[] = "425536999999";
    /*             7428710000001  */
    char suma[256] = "wwwwwwwwwwwwwwwwwwww";

    sumar(num2, num1, suma);

    printf("Resultado: %s\n", suma);

    return 0;
}
