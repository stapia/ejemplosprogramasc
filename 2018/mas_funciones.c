
#include <math.h>
#include <stdio.h>

char letra2mays(char letra) 
{
	if ( 'a' <= letra && letra <= 'z' ) {
		letra = 'A' + (letra - 'a');
	}
	return letra;
}

double hipotenusa(double b, double c) 
{
	return sqrt(b*b + c*c); 
}


double max2(double x, double y) {
	if ( x > y ) {
		return x;
	}
	else {
		return y;
	}
}

double max3( double x, double y, double z ) {
	return max2(z, max2(x,y));
}


double tramos(double x) {
	double result;
	
	if ( x <= 0 ) {
		result = 0;
	}
	else if ( x <= 2 ) {
		result = x*x;
	}
	else if ( x <= 6 ) {
		result = -x + 6;
	}
	else {
		result = 0;
	}
	
	return result;
}

void tabla() {
	float x = -1.0;
	do {
		printf("f(%f)= %f\n", x, tramos(x));
		x = x + 0.1;
	}
	while ( x <= 7 );
}

int main() 
{
	char mi_letra = 'e', res;
	double a, b = 3.4, c = 4.5;	
	
	tabla();
	
	a = hipotenusa( b, c);
	printf("hipotenusa es: %f\n", a);
	
	res = letra2mays(mi_letra);
	printf("%c en mays: %c\n", mi_letra, res);
	
	printf("El maximo de los lados es: %f\n", max3(a,b,c));
	
	return 0;
}
