
#include <stdio.h>
#include <string.h>

int main() {
	int i, longitud;
	char cadena1[12] = "hola";
	char cadena2[12];
	char cadena3[12] = "mundo";
	char texto[256];
	char *p;
	
	scanf("%11s", cadena2);
	
	printf("Las cadenas son : '%s', '%s', '%s'\n", 
	         cadena1, cadena2, cadena3);
	
	longitud = strlen( cadena1 );
	for ( i = 0; i < longitud; ++i ) 
	{
		cadena1[i] += 'A' - 'a';
	}

	printf("cadena1 es : '%s'\n", cadena1);
	
	cadena1[longitud] = '#';
	cadena1[longitud+1] = '\0'; /* ó = 0 */
	
	printf("cadena1 es : '%s'\n", cadena1);
	
	for ( i = 0; cadena3[i] != 0; ++i ) {
		printf("%c\n", cadena3[i]);
	}
	
	strcpy(texto,"Inicio: ");
	printf("texto es : '%s'\n", texto);
	strcpy(texto, "");
	printf("texto es : '%s'\n", texto);
	strcpy(texto,cadena1);
	printf("texto es : '%s'\n", texto);
	
	strcat(texto, " ");
	strcat(texto, cadena3);
	strcat(texto, "$");
	strcat(texto, cadena3);
	printf("texto es : '%s'\n", texto);
	
	longitud = strlen(texto) + strlen(cadena3) + strlen("-");
	
	while ( longitud <= 255 ) {
		strcat(texto, "-");
		strcat(texto, cadena3);
		longitud = strlen(texto) + strlen(cadena3) + strlen("-");
	}

	printf("texto es : '%s'\n", texto);

	printf("texto es : '%s'\n", texto + 5);
	
	p = texto;
	
	for ( i = 0; i < 5; ++i ) 
	{
		scanf("%s", p);
		p = texto + strlen(texto);
	}

	printf("texto es : '%s'\n", texto);
	
	return 0;
}
