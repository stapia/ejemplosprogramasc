
#include <stdio.h> 

int todos_pares() 
{
	int i, num, result = 1;
	for ( i = 0; i < 5; ++i ) {
		scanf("%d", & num);
		result = result && (num % 2 == 0);
	}
	return result;
}

void mostrar(double re, double im) {
	printf("%f + %f i\n", re, im);
}

int x = 1;

int main() 
{	
	printf("Introduce 5 valores enteros\n");
	printf("Son todos pares es %d\n", todos_pares() );
	
	mostrar(3.7, 4.4);
	
	return x;
}
