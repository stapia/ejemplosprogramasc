#include <stdio.h>

int main() {
	double numero, maximo;
	int i;
	
	scanf("%lf", &numero);
	maximo = numero;
	
	for ( i = 0; i < 4; ++i ) 
	{
		scanf("%lf", &numero);
		if (numero > maximo) {
			maximo = numero;
		}
	}
	printf("El maximo es %f\n", maximo);
	return 0;
}
