
#include <stdio.h>

int main() {
	int num, unidades16;
	char letra = 'A'; /* '\t' */
	
	printf("La letra es %c\n", letra);
	
	scanf("%d", &num);
	scanf(" %c", &letra);
	printf("La letra es %c", letra);
	printf("El número es %c\n", num);
	
	unidades16 = num % 16;
	
	if ( unidades16 < 10 ) {
		printf("Unidades en base 16: %d\n", unidades16);
	}
	else {
		printf("Unidades en base 16: %c\n", 'A' + unidades16 - 10);
	}
	
	return 0;
}
