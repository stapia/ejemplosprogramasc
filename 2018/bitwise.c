
#include <stdio.h>

int main() {
	int num = 1, res;
	
	printf("sizeof num: %d \n", sizeof(num));
	printf("sizeof short: %d \n", sizeof(short));
	printf("sizeof long: %d \n", sizeof(long));
	
	res = num << 3;
	printf("num << 3: %d \n", res);
	
	res = num << ( sizeof(num) * 8 - 1);
	printf("num << 31: %d \n", res);

	res = num << ( sizeof(num) * 8 - 2);
	printf("num << 30: %d \n", res);
	
	num = -456;
	res = (num >> ( sizeof(num) * 8 - 1)) & 1;
	printf("Negativo: %d \n", res);
	
	num = -556;
	res = (num >> ( sizeof(num) * 8 - 1));
	printf("Negativo: %d \n", res);
	
	
	return 0;
}
