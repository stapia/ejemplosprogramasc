
#include <stdio.h>
#include <math.h>

#define PI 3.141592

struct gsl_sf_result { double val; double error; };

int gsl_sin(double x, struct gsl_sf_result* res) 
{
	res->val = sin(x);
	res->error = 1e-7;
	return 3;
}

int main() 
{
	int grados, status;
	struct gsl_sf_result st;
	
	for ( grados = 0; grados <= 60; grados = grados + 10 ) 
	{
		status = gsl_sin( grados / 180.0 * PI, &st );
		printf("a: %d, status: %d, sin: %f, error: %e\n", 
			       grados, status, st.val, st.error);
	}
	
	return 0;
}
