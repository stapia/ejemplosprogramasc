
#include <stdio.h>

float* mayor(float *a, float* b) {
	if ( *a >= *b ) {
		return a;
	}
	else {
		return b;
	}
}


void multiplicar(float *a, float* b) {
	float *puntero = mayor(a,b);
	*puntero = *puntero * 2.0;
}

void pedir(float *p, float *q) {
	scanf("%f %f", p, q);
	if ( *p < 0.0 ) {
		*p = 0.0;
	}
	*q = *q / 100.0;
}

int main() {
	float x = 3.4, y = 20.5, *p;
	p = mayor( &x, &y );
	printf("El mayor valor es %f y esta en %p\n", *p, (void*)p);
	multiplicar(&x, &y);
	printf("El mayor valor es %f y esta en %p\n", *p, (void*)p);
	
	pedir(&x, &y);
	printf("x es %f y es %f\n", x, y);
	return 0;
}
