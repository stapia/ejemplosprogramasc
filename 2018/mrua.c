#include <stdio.h>

int main() 
{
	double s, a, Vo, t;
	
	printf("Introduce: \n"
		   "    La aceleración en m/s², \n"
		   "    La velocidad inicial en m/s, \n"
		   "    El tiempo en s, \n"
		   "    Suponemos posición inicial 0 \n");
	
	scanf("%lf %lf %lf", &a, &Vo, &t);
	
	s = Vo * t + 0.5 * a * t * t; 
	/*
	s = Vo * t + 1/2.0 * a * t * t; 
	s = Vo * t + a * t * t / 2; 
	 */ 
	printf("La coordenada final es: %.2f m\n", s);
	
	return 0;
}
