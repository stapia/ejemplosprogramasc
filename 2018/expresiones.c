
#include <stdio.h>

int main() {
	int num, boolean, cifra;
	scanf("%d", &num);
	
	boolean = num > 5;
	printf("Numero > 5 es %d\n", boolean);

	boolean = (3 <= num) && (num <= 7);
	printf("Numero está entre 3 y 7 %d\n", boolean);

	boolean = (num % 3 == 0)  || (num % 7 == 0);
	printf("Numero divisible por 3 o 7 %d\n", boolean);
	
	boolean = !(num % 3) ||
	          !(num % 7);
	printf("Numero divisible por 3 o 7 %d\n", boolean);
	
	cifra = num % 10;
	printf("Las unidades son %d\n", cifra);

	cifra = num / 10;
	printf("Las demás son %d\n", cifra);

	cifra = num % 16;
	printf("Las unidades en Hx son %d\n", cifra);

	return 0;
}
