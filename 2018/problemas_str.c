
#include <string.h> 
#include <stdio.h> 

/* S1: Se supone que la palabra de la frase tienen un 
 * máximo de 31 letras */
void pedir_frase( char frase[], unsigned tam ) {
	char palabra[32];
	int len;
	
	strcpy(frase, "");
	
	scanf("%31s", palabra);
	len = strlen(frase)+strlen(palabra)+2;
	
	while ( strcmp(palabra, "fin") != 0 && len <= tam  ) 
	{
		strcat(frase, palabra);
		strcat(frase, " ");
		scanf("%31s", palabra);
		len = strlen(frase)+strlen(palabra)+2;
	}
}

void sustituir(char *cadena) 
{
	while ( *cadena != 0 && *(cadena+1) != '\0' ) {
		if ( *cadena == 'a' && cadena[1] == 'a' ) {
			*cadena = 'b';
			*(cadena+1) = 'b';
		}
		++cadena;
	}
}

void sustituir2(char *cadena) 
{
	int i; int longitud = strlen(cadena);
	for ( i = 0;  i < longitud-1; ++i  ) {
		if ( cadena[i] == 'a' && cadena[i+1] == 'a' ) {
			cadena[i] = 'b';
			cadena[i+1] = 'b';
		}
	}
}

int main() 
{
	char cadena[64];
	
	pedir_frase(cadena, 64);
	
	return 0;
}
