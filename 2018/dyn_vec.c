
#include <stdio.h>
#include <stdlib.h>

void mostrar( const double vector[], int n) {
	int i;
	for ( i = 0; i < n; ++i ) 
	{
		printf("%f ", vector[i]);
	}
	printf("\n");
}


double* genera( int tam ) 
{
	int i;
	double *p_dyn = (double *) malloc( tam * sizeof(double) );
	for ( i = 0; i < tam; ++i ) {
		p_dyn[i] = i + 0.1*i;
	}
	return p_dyn; 
}

int main() {
	double v[3] = { 1.1, 2.2, 3.3 };
	double *p_dyn; 
	int tam, i;

	mostrar( v, 3 );
	
	printf("Cuantos doubles quieres?\n");
	scanf("%d", &tam);
	
	p_dyn = (double *) malloc( tam * sizeof(double) );
	for ( i = 0; i < tam; ++i ) {
		p_dyn[i] = rand() * 10.0 / RAND_MAX;
	}
	mostrar( p_dyn, tam );
	free ( p_dyn );
	
	p_dyn = genera( 5 );
	mostrar( p_dyn, 5 );
	free( p_dyn );
	
	return 0;
}
