
#include <stdlib.h>

struct mediciones { int indice; double temperaturas[20]; };

double** mat_dyn( struct mediciones temps[], int tam ) 
{
	double **result; int i, i_vector, fila, j;
	result = (double**)malloc(50*sizeof(double*));
	for ( i = 0; i < 50; ++i ) {
		result[i] = NULL;
		/* (result+i) = NULL;  */
	}
	
	for ( i_vector = 0; i_vector < tam; ++i_vector ) {
		fila = temps[i_vector].indice;
		result[fila] = (double*)malloc( 20 * sizeof(double) );
		for ( j = 0; j < 20; ++j ) {
			result[fila][j] = temps[i_vector].temperaturas[j];
		}
	}
	return result;
}

int main() {
	return 0;
}
