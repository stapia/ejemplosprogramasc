
#include <stdio.h>
#include <math.h>

void referencia() {
	int *p; int a = 3;
	
	printf("a = %d\n", a);
	
	p = &a;
	
	printf("a = %d, p = %p\n", a, (void*) p);
	
	*p = 45;
	
	printf("a = %d, p = %p\n", a, (void*) p);
	
}

double mod_arg(double Re, double Im, double *arg) {
	*arg = atan2(Im, Re);
	return sqrt(Re*Re+Im*Im);
}

int main() {

	double re = 3.2, im = 2.1, modulo, argumento;
	
	modulo = mod_arg(re, im, &argumento);
	
	printf("mod = %f, arg = %f\n", modulo, argumento);

	referencia();

	return 0;
}
