
#include <stdio.h>

int main() {
	char letra;
	
	scanf(" %c", &letra);
	
	if ( '0' <= letra && letra <= '9' ) 
	{
		printf("La letra '%c' es un digito\n", letra);
	}
	else if ( 'a' <= letra && letra <= 'z' ) 
	{
		printf("La letra '%c' es una minúscula\n", letra);
		if ( letra == 'z' ) {
			printf("La siguiente letra es 'a'\n");
		}
		else {
			printf("La siguiente letra es '%c'\n", letra + 1);
		}
	}
	else if ( 'A' <= letra && letra <= 'Z') 
	{
		printf("La letra '%c' es una mayúscula\n", letra);
	}
	else 
	{
		printf("La letra '%c' es símbolo\n", letra);
	}
	
	
	return 0;
}
