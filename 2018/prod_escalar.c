
#include <stdio.h>

#define DIM 3

int main() {
	int i; double prod;
	double V[DIM], W[DIM];
	double *p, *q;
	
	for ( i = 0; i < DIM; ++i ) 
	{
		scanf("%lf",  &(V[i]) );
	}

	for ( i = 0; i < DIM; ++i ) 
	{
		scanf("%lf",  W+i );
	}
	
	prod = 0;
	for ( i = 0; i < DIM; ++i ) 
	{
		prod = V[i] * W[i] + prod;
	}
	
	printf("Producto escalar: %f\n", prod);
	
	prod = 0;
	p = V; /* p es double* */
	q = W+(DIM-1);
	printf("V: %d, p: %d\n", sizeof(V), sizeof(p));
	for ( i = 0; i < DIM; ++i, ++p ) 
	{
		prod = (*p) * (*q) + prod;
		--q;
	}
	printf("Producto escalar: %f\n", prod);
	
	return 0;
}
