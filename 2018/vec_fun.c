
#include <stdio.h>

void mostrar(const double *V, unsigned tam) 
{
	int i; 
	for ( i = 0; i < tam; ++i ) {
		printf("%7.2f ", V[i]);
	}
	printf("\n");
}

void pedir(double *V, unsigned tam) 
{
	while ( tam > 0 ) {
		scanf("%lf", V);
		--tam; 
		++V;
	}
	/*
	int i; 
	for ( i = 0; i < tam; ++i ) {
		scanf("%lf ", &(V[i]));
	}
	*/  
}

int pares_ordenados(const double *V, unsigned tam) 
{
	int i, contar = 0; 
	for ( i = 0; i < tam-1 ; ++i ) {
		if ( V[i] < V[i+1]) {
			++contar;
		}
	}
	return contar;
}

int main() {
	double W[10];
	pedir(W, 10);
	return 0;
}
