#include <stdio.h>
/* Para compilar con math.h hace falta añadir -lm a 
   a las opciones de compilación en gnu-linux  */
#include <math.h>

int main() 
{
	double a, b, c; 
	double x1 = 0;
	
	scanf("%lf", & a);
	scanf("%lf", & b);
	scanf("%lf", & c);
	
	x1 = ( -b + sqrt( b*b - 4*a*c) ) / (2*a);
	
	printf("X1 es %.14f\n", x1); 
	
	return 0;
}
