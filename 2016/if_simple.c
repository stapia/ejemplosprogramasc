
#include <stdio.h>

int main() 
{
	int a; 
	
	scanf("%i", &a);
	
	if ( a < 0 ) {
		printf("a es menor que 0\n");
	}
	else {
		printf("voy por aqui\n");
		if ( a > 0 ) {
			printf("a es mayor que 0\n");
		}
		else {
			printf("a es 0\n");
		}		
	}
	
	if ( a > 0 )
	{
		printf("a es mayor que 0\n");
	}
	else if ( a < 0 ) 
	{
		printf("a es menor que 0\n");
	}
	else 
	{
		printf("a es 0\n");
	}
	
	return 0;
}
