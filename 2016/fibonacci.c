
#include <stdio.h>

int main() 
{
	int i, An, An1 = 1, An2 = 1;
	
	for ( i = 0; i < 10; ++i ) {
		An = An1 + An2;
		printf("An=%i, An-1=%i, An-2=%i\n",
		        An,    An1,     An2); 
		An2 = An1;
		An1 = An;
		printf("Nuevo An1=%i, An2=%i\n",
		              An1,     An2); 
	}
	
	return 0;
}
