#include <stdio.h>

int main() 
{
	double traza = 0; int i;
	double M[][3] = { { 2.23, 4.60, 6.71 },
	                  { 1.00, 2.00, 1.00 },
	                  { 1.00, 2.00, 1.00 } };
	                
	for ( i = 0; i < 3; ++i ) {
		traza += M[i][(3-1)-i];
	}

	/* Esta es una solucion "peor"
	for ( i = 0, j = 2; i < 3; ++i, --j ) {
		traza += M[i][j];
	}
	*/ 
	
	printf("traza = %.3f\n", traza);
	 
	return 0;
}
