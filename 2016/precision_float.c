
#include <stdio.h>

int main()
{
	float x = 1.31231e-7, y;
	double X = 1.31231e-7, Y;
	
	y = 1 + x; Y = 1 + X;
	printf("x = %.14f, y = %.14f\n", x, y);
	printf("X = %.14f, Y = %.14f\n", X, Y);
	
	return 0;
}
