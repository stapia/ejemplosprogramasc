
#include <stdio.h>
#include <string.h>
#define TAM 11

/* Declaracion equivalente 
 * void escribir_5(const char cad[]) */
void escribir_6(const char*cad) 
{
	if ( strlen(cad) >= 6 ) {
		printf("%c\n", cad[5]);
	}
	else {
		printf("fuera de rango\n");
	}
}

int main() 
{
	int i;
	char vacia[TAM] = "";
	char cad[TAM] = "Hola Mundo";
	char *puntero;
	
	escribir_6(vacia);
	escribir_6(cad);
	
	printf("Vacia : '%s' "
	       "y saludo: '%s'\n", vacia, cad);
	       
	for ( i = 0; cad[i] != '\0'; ++i ) {
		if ( cad[i] == 'o' ) {
			cad[i] = 'a';
		}
	}
	
	puntero = NULL;
	for ( i = 0; cad[i] != '\0'; ++i ) {
		if ( cad[i] == 'n' ) {
			puntero = &(cad[i]);
		}
	}
	
	cad[4] = '\0';
	printf("saludo: '%s'\n", cad);
	printf("letra en 5: %c\n", cad[5]);
	
	return 0;
}
