
#include <stdio.h>

int bisiesto(int anho)
{
	return  ((anho % 4 == 0) && 	
			(anho % 100 != 0)) ||	
			(anho % 400 == 0);
}

int main() 
{
	printf("2000 bisiesto es: %i\n", bisiesto(2000));
	printf("1900 bisiesto es: %i\n", bisiesto(1900));
	printf("2016 bisiesto es: %i\n", bisiesto(2016));
	printf("2017 bisiesto es: %i\n", bisiesto(2017));
	return 0;
}
