
#include <stdio.h>

#define FILAS 3
#define COLUM 3

void pedir(double mat[FILAS][COLUM]) {
	int i, j;
	for ( i = 0; i < FILAS; ++i ) {
		for ( j = 0; j < COLUM; ++j ) {
			scanf(" %lf", &(mat[i][j]));
		}
	}	
}

void mostrar(double mat[][COLUM], int filas) {
	int i, j;
	for ( i = 0; i < filas; ++i ) {
		for ( j = 0; j < COLUM; ++j ) {
			printf("%5.2f ", mat[i][j]);
		}
		printf("\n");
	}
}

int main() 
{
	double A[FILAS][COLUM];
	pedir( A ) ;
	mostrar ( A, 3 ) ;
	return 0;
}
