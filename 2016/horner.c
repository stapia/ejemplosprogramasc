
#include <stdio.h>

#define GRADO 5

int main() 
{
	double pol[GRADO+1] = { 1.2, 5.4, 4.4, 0.5, 1.0, -1.1};
	double eval = 0, x = 2.3; int i;
	
	for ( i = GRADO; i >= 0; --i ) 
	{
		eval = pol[i] + x*eval;
	}
	
	printf("p(%f) = %f\n", x, eval);
	
	return 0;
}
