
#include <stdio.h>

void bubble(double *v, unsigned tam) 
{
	int N, j; double aux;
	for ( N = tam-1; N >= 1; --N ) {
		for ( j = 0; j < N; ++j ) {
			if ( v[j] > v[j+1] ) {
				aux = v[j];
				v[j] = v[j+1];
				v[j+1] = aux;
			}
		}
	}
}

int main() {
	int i;
	double w[] = { 7.3, 5.6, 10.0, 
	               13.7, -1.9 };
	bubble(w, 5);
	
	for ( i = 0; i < 5; ++ i ) {
		printf("%6.1f ", w[i]);
	}
	printf("\n");
	return 0;
}
