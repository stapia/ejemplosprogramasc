
#include <stdio.h>

int main() 
{
	unsigned max_uint; int x = -5;
	
	max_uint = -1;
	
	printf("Max = %u\n", max_uint);
	
	x = ~x + 1;
	
	printf("x = %i\n", x);
	
	x = 5;
	
	printf("x << 1 = %i\n", x << 1);
	printf("x >> 1 = %i\n", x >> 1);
	printf("1 << 4 = %i\n", 1 << 4);
	
	x = 0;
	printf(" ~0 es %i\n", ~x );
	
	printf(" 0 | 8 es %i\n", 0 | 8 );
	printf(" 1 | 8 es %i\n", 1 | 8 );
	printf(" 1 | 9 es %i\n", 1 | 9 );
	printf(" 1 ^ 9 es %i\n", 1 ^ 9 );
	printf(" 1 & 9 es %i\n", 1 & 9 );
	
	return 0;
}
