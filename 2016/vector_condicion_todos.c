
#define DIM 4

int main() 
{
	int V[DIM] = { -2, -6, -20, 1 };
	int i, contador = 0, todos;
	
	for ( i = 0; i < DIM; ++i ) 
	{
		contador += V[i] % 2 == 0 && V[i] < 0;
	}
	
	todos = (contador == DIM);
	
	todos = 1; i = 0;
	while ( todos == 1 && i < DIM)
	{
		todos = V[i] % 2 == 0 && V[i] < 0;
		++i;
	}
	
	todos = 1;
	for ( i = 0; i < DIM; ++i ) 
	{
		todos = todos && V[i] % 2 == 0 && V[i] < 0;
	}
	
	return 0;
}
