
#include <stdio.h>

int main() 
{
	float x = 1e6, y = 100.01, res;
	int a = 100000000, b = 10001, num;
	
	res = x + y;
	printf("Suma = %f\n", res);
	
	res = (1-.12)*res;
	printf("descuento del 12%% es %f\n", res);
	
	num = a + b;
	printf("Suma = %i,%02i\n", num / 100,
	                         num % 100);

	num = 88*num/100;
	printf("Suma = %i,%02i\n", num / 100,
	                         num % 100);

	return 0;
}
