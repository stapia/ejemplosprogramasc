
#include <math.h>
#include <stdio.h>

int main() 
{
	double a = 3, b = 4, medio;
	
	while (  fabs(b-a) > 1e-10 ) {
		medio = (b + a) / 2.0;
		if ( sin(b) * sin(medio) < 0 ) {
			a = medio;
		}
		else {
			b = medio;
		}
	}
	printf("Pi es %.14f\n", a);
	
	return 0;
}
