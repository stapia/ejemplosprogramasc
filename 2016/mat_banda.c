
#include <stdio.h>

#define DIM 4

int main() {
	double Mat[DIM][DIM] = {
		{ 1.2, 5.6, 0.0, 0.0 },
		{ 7.1, 9.2, 3.3, 0.0 },
		{ 0.0, 5.6, 9.6, 8.8 },
		{ 0.0, 0.0, 4.9, 6.2 }
		};
	double Mat2[DIM][DIM];
	int i, j;
	FILE *g = fopen("banda.txt", "w");
	if ( g != NULL ) {
		fprintf(g, "%i\n", DIM);
		fprintf(g, "%f %f\n", Mat[0][0], Mat[0][1]);
		for ( i = 1; i < DIM; ++i ) {
			for ( j = i-1; (j <= i+1) && (j < DIM); ++j ) {
				fprintf(g, "%f ", Mat[i][j]);
			}
			fprintf(g, "\n");
		}
		fclose(g);
	}
	
	g = fopen("banda.txt", "r");
	if ( g != NULL ) {
		int tam; 
		
		fscanf( g, "%i", &tam );
		/* 
		FALTA COMPROBAR QUE EL TAMAÑO NO EXCEDE AL PERMITIDO (DIM)
		Se supone que la matriz a banda tiene un tamaño mínimo de 3 o 4, 
		no tiene sentido para tamaños inferiores.
		*/
		
		fscanf( g, "%lf", &(Mat2[0][0]);
		fscanf( g, "%lf", &(Mat2[0][1]);
		for ( j = 2; j < tam; ++j ) {
			Mat2[0][j] = 0.0;
		}
		
		for ( i = 1; i < tam; ++i ) {
		    /* Falta rellenar con ceros */
			for ( j = i-1; (j=i+1)&&(j<tam); ++j ) {
				fscanf( g, "%lf", &(Mat2[i][j]);
			}
			/* Falta rellenar con ceros */
		}
		
		fclose( g );
	}

	return 0;
}
