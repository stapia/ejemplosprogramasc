
#include <stdio.h>

#define TAM 10

int main() {
	int i;
	double x[TAM] = { 1.2, 4.5, 10.8 };
	
	for ( i = 0; i < TAM; ++i ) {
		printf("%.2f ", x[i]);
	}
	printf("\n");
	
	for ( i = 0; i < TAM; ++i ) {
		x[i] = x[i] + 3.3;
	}
	
	for ( i = 0; i < TAM; ++i ) {
		printf("%.2f ", x[i]);
	}
	printf("\n");
	
	return 0;
}
