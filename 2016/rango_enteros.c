#include <stdio.h>

int main()
{
	unsigned char x = 23, y;
	signed char r = 10, s;
	char a = 23, b;
	
	y = x + 300;
	s = r + 118;
	b = a + 200;
	
	printf("Y es %i\n", y);
	printf("S es %i\n", s);
	printf("B es %i\n", b);
	
	return 0;
}
