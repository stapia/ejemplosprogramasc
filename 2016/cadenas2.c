
#include <string.h>
#include <stdio.h>

void modifique(char* cad, const char* dada)
{
	/* "Hola --- */
	strcpy( cad, "Hola ");
	strcat( cad, dada );
}

void copiar_saltando_4(char *otra, const char*dada)
{
	strcpy(otra, dada+4);
}

int main() 
{
	char saludo[256] = "se pierde";
	char mundo[] = "mundo";
	char cortada[256];
	/* Esta es incorrecta: 
	modifique("Hola ", "mundo"); */
	modifique(saludo, "Mundo");
	modifique(saludo, mundo);
	printf("%s\n", saludo);
	
	copiar_saltando_4(cortada, saludo);
	printf("'%s'\n", cortada);
	
	return 0;
}
