
#include <stdio.h>

int main() 
{
	int i; double x, total = 0;
	
	for ( i = 0; i < 5; ++i ) 
	{
		scanf("%lf", &x);
		total = total + x;
	}
	
	total = total / 5;
	printf("La media es %f\n", total);
	
	printf("Introduce valores. Para acabar un negativo\n");
	
	i = 0; total = 0; scanf("%lf", &x);
	while ( x >= 0 ) {
		total = total + x;		
		scanf("%lf", &x);
		i = i + 1;
	}
	
	if ( i == 0 ) {
		printf("No has introducido valores\n");
	}
	else {
		total /= i;
		printf("La media es %f\n", total);
	}
	
	return 0;
}
