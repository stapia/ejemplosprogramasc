
#include <stdio.h>
#include <string.h>

/* Declarar struct con valor y ponderacion */

struct kelvin {
	double valor, ponderacion;
	char nombre[20];
} ;

struct kelvin pedir() 
{
	struct kelvin x;
	scanf("%lf%lf%s", &(x.valor), 
	                  &(x.ponderacion),
	                  x.nombre );
	return x;
}

void mostrar(struct kelvin x) 
{
	printf("%f %f %s\n", x.valor, 
	                   x.ponderacion,
	                   x.nombre );
}

double media( struct kelvin v[], int tam) {
	int i; double suma = 0, s_pond = 0;
	
	for ( i = 0; i < tam; ++i ) {
		suma += v[i].valor * v[i].ponderacion;
		s_pond += v[i].ponderacion;
	}
	return suma / s_pond;
}

int main() 
{
	int i; struct kelvin k, q; 
	struct kelvin vec[3];
	double resultado;
	k.valor = 4.0;
	k.ponderacion = .5;
	strcpy(k.nombre,"Hola");
	q = k;
	printf("%s\n", q.nombre);
	for ( i = 0; i < 3; ++i ) 
	{
		vec[i] = pedir();
		mostrar(vec[i]);
	}
	
	resultado = media( vec, 3 );
	printf("La media es %f\n", resultado);
	return 0;
}

