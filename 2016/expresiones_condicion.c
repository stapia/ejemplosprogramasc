
#include <stdio.h>

int main() 
{
	int i = 12; int cond;
	
	/* Entre 7 y 10 inclusive */
	cond = 7 <= i && (i <= 10);

	printf("Entre 7 y 10 inclusive = %i\n", cond);
	
	/* Par */
	cond = i % 2 == 0;

	printf("Par = %i\n", cond);
	
	/* Acaba en 5 */
	cond = i % 10 == 5;

	printf("Acaba en 5 = %i\n", cond);
	
	return 0;
}
