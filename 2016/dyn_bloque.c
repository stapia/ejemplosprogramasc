
/*
Este programa reserva una variable dinámica donde
se van a guardar:
a) Un número hasta 127 que es la longitud de una cadena (en 1 byte)
b) Un número fijo, 31, también en 1 byte
c) Una cadena alfanumérica que se pide al usuario y luego se copia 
   variable dinámica (incluido el caracter nulo)
d) Un entero de valor 4356 (el valor es irrelevante). 

Para hacerse una idea, hay que dibujar la variable con 
los bloques de 1 byte en un diagrama.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() 
{
	char *var; char aux[128]; int *p;
	char len;
	scanf("%127s", aux);
	len = strlen(aux);
	var = (char*)malloc(3+len+sizeof(int));
	
	var[0] = len;
	var[1] = 31;
	strcpy(var+2, aux);
	p = (int*)(var+(3+len));
	*p = 4356;
	
	free(var);
	return 0;
}
