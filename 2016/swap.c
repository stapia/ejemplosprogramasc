
#include <stdio.h>

void cambiar(int i, int j) {
	int aux; aux = i; i = j; j = aux;
	printf("Valor de i: %i y j: %i\n", i, j);
}

void intercambiar(int *p, int *q) {
	int aux; 
	aux = *p; *p = *q; *q = aux;
}

int main() {
	int a = 10, b = 20;
	cambiar(a, b);
	printf("Valor de a: %i y b: %i\n", a, b);
	intercambiar(&a, &b);
	printf("Valor de a: %i y b: %i\n", a, b);
	return 0;
}
