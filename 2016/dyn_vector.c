
#include <stdio.h>
#include <stdlib.h>

int main() 
{
	int i, N; int *vector;
	scanf("%i", &N);
	vector = (int*)malloc(N*sizeof(int));
	for ( i = 0; i < N; ++i ) {
		vector[i] = i*3;
	}
	for ( i = 0; i < N; ++i ) {
		printf("%i ", vector[i]);
	}
	free(vector);
	return 0;
}
