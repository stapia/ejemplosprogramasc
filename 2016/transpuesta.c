#include <stdio.h>

#define DIM 3

int main() 
{
	double A[][DIM] = { { 2.23, 4.60, 6.71 },
	                    { 1.00, 2.00, 1.00 },
	                    { 1.00, 2.00, 1.00 } };
	double aux; int i, j;

	for ( i = 0; i < DIM; ++i ) {
		for ( j = i+1; j < DIM; ++j ) {
			aux = A[i][j];
			A[i][j] = A[j][i];
			A[j][i] = aux;
		}
	}
	
	return 0;
}
