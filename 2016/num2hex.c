
#include <stdio.h>
#include <string.h>

char digit16( int digito ) {
	if ( digito > 9 ) {
		return digito + 'A' - 10;
	}
	else {
		return digito + '0';
	}	
}

void d16_a_cadena(int digito, char aux[])
{
	aux[0] = digit16( digito );
	aux[1] = 0;
}

/* Concatenea en c2 : c2 y c1 y pone el 
 * resultado tambien en c1. */
void str_contat_21( char* c1, char *c2) {
	strcat( c2, c1 );
	strcpy( c1, c2 );
}

void n2hex( int num, char cad[] ) {
	char aux[32];
	strcpy(cad, "");
	if ( num == 0 ) {
		strcpy(cad, "0");
	}
	else while ( num > 0 ) {
		d16_a_cadena( num % 16, aux );
		printf("aux: '%s'\n", aux);
		str_contat_21(cad, aux);
		printf("cad: '%s'\n", cad);
		num = num / 16;
	}
}

int main() {
	int num = 5365; char cad[32];
	n2hex(num, cad);
	printf("%i en hx: %s\n", num, cad);
	return 0;
}
