
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

struct monomio {
	double coef;
	int grado;
};

double eval(double x, struct monomio V[], unsigned tam) {
	double result = 0; int i;
	for ( i = 0; i < tam; ++i ) {
		result += V[i].coef * pow( x, V[i].grado );
	}
	return result;
}

int main() {
	struct monomio* V;
	
	V = (struct monomio*)malloc(
				3*sizeof(struct monomio));
				
	V[0].coef = 2.0; V[0].grado = 0;
	(V+1)->coef = 3.3;(V+1)->grado = 3;
	V[2].coef = 4.0; V[2].grado = 20;
	
	printf("Res = %f\n", eval(0.68, V, 3));
	
	free(V);	
	return 0;
}
