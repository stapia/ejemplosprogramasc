
#include <stdio.h>

void incrementar(int i) {
	++i;
	printf("Valor i : %i\n", i);
}

void inc(int *p) {
	*p = *p + 1; /* También vale: (*p)++ ó ++(*p). Es mejor poner los paréntesis siempre */
}

int main() {
	int a = 10;
	incrementar(a);
	printf("Valor de a: %i\n", a);
	inc(&a);
	printf("Valor de a: %i\n", a);
	return 0;
}
