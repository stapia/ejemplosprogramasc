
#include <stdio.h>
/*
char* buscar_chr( const char *cad, char c ) 
 */ 
char* buscar_chr( char cad[], char c ) {
	int i = 0;
	while ( cad[i] != 0 && cad[i] != c ) {
		++i;
	}
	
	if ( cad[i] == c ) {
		return &(cad[i]);
	}
	else {
		return NULL;
	}
}

int main() {
	char cadena[] = "Hola soy un texto muy largo ";
	char *aux;
	
	aux = buscar_chr(cadena, 'o');
	
	while ( aux != NULL ) {
		printf("Letra : `%c`\n", *(aux+1));
		printf("Cadena : `%s`\n", aux);
		aux = buscar_chr( aux+1 , 'o');
	}
	
	return 0;
}
