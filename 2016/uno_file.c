
#include <stdio.h>

int numero_de_negativos( const char *nombre ) {
	FILE* g; int num = 0;
	g = fopen( nombre, "r" );
	if ( g != NULL ) {
		double x; 
		while ( fscanf(g, "%lf", &x) == 1) {
			if ( x < 0 ) ++num;
		}
		fclose(g);
	}
	return num;
}

int main() 
{
	FILE* g = fopen("temperaturas.dat", "r");
	double media = 0; int cuenta = 0;
	if ( g != NULL ) {
		char lugar[32]; double temp; int sc;
		sc = fscanf(g, "%31s %lf", lugar, &temp);
		while ( EOF != sc ) {
			if ( sc == 1 ) {
				printf("He leido el lugar, pero no la temperatura\n");
			} else if ( sc == 2 && lugar[0] == 'A' ) {
				media += temp;
				++cuenta;
			}
			sc = fscanf(g, "%31s %lf", lugar, &temp);
		}
		media = media / cuenta;
		printf("La media es %f\n", media);
		fclose(g);
	}
	
	{ /* Contar negativos */
		int num = numero_de_negativos("contar_negativos.txt");
		printf("Negativos hay: %i\n", num);
	}
	
	return 0;
}
