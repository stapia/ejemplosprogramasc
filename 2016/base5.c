
#include <stdio.h>

int main() 
{
	int num;
	
	scanf("%i", &num);
	
	while ( num > 4 ) {
		printf("%i", num % 5);
		num = num / 5;
	}
	printf("%i\n", num);
	
	return 0;
}
