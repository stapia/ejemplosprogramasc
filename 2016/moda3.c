
#include <stdio.h>

int main() 
{
	const int N = 4;
	int mal = 0, regular = 0, bien = 0, i; 
	char resp;
	
	for ( i = 0; i < N; ++i ) {
		scanf(" %c", &resp); 
		switch ( resp ) {
			case '+': ++bien; break;
			case '-': ++mal; break;
			case '0': ++regular; break;
		}
	}
	
	printf("--- Solucion 1 ------\n");
	if ( bien > mal && bien > regular) {
		printf("La moda es 'bien'\n");
	}
	else if ( regular > mal ) {
		printf("La moda es regular\n");
	}
	else {
		printf("La moda es mal\n");
	}
	
	printf("--- Solucion 2 ------\n");
	if ( bien >= mal && bien >= regular) {
		printf("La moda es 'bien'\n");
	}	
	if ( regular >= mal && regular >= bien) {
		printf("La moda es 'regular'\n");
	}
	if ( mal >= bien && mal >= regular) {
		printf("La moda es 'mal'\n");
	}	
	
	return 0;
}
