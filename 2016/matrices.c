
#include <stdio.h>

#define FILAS 2
#define COLUMNAS 3

int main() {
	int i, j; double suma = 0.0;
	double matriz[FILAS][COLUMNAS] = { 
		{ 1.0, 2.0, 3.3 },
		{ 7.9, 8.4, 9.9 } } ;
	
	for ( i = 0; i < FILAS; ++i ) {
		for ( j = 0; j < COLUMNAS; ++j ) {
			printf("%.2f ", matriz[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	
	for ( i = 0; i < COLUMNAS; ++i ) {
		suma = suma + matriz[1][i];
	}
	printf("suma de la 2 fila: %.2f\n", suma);

	return 0;
}
