
#include <stdio.h>

#define DIM 3

int main() 
{
	int i, j, k;
	double A[][DIM] = { { 2.23, 4.60, 6.71 },
	                  { 1.00, 2.00, 1.00 },
	                  { 1.00, 2.00, 1.00 } };

	double B[][DIM] = { { 4.23, 4.60, 1.71 },
	                  { 3.00, 2.20, 7.00 },
	                  { 1.11, 2.45, -5.00 } };
	                  
	double C[DIM][DIM];
	
	for ( i = 0; i < DIM; ++i ) {
		for ( j = 0; j < DIM; ++j ) {
			C[i][j] = 0;
			for ( k = 0; k < DIM; ++k ) {
				C[i][j] += A[i][k]*B[k][j];
			}
		}
	}
	
	return 0;
}
