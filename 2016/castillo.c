
#include <stdio.h>
/* Para la funcion seno, incluimos math.h */
#include <math.h>

int main() 
{
	/* Probar con x = 0 para ver que pasa al dividir
	 * por 0 */
	double x = 1, y = 2, z;
	
	z = ( (x+y)/3 - 1/x ) / (2 + sin(y));
	
	printf("z = %f\n", z);
	
	return 0;
}
