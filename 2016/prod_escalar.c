
#include <stdio.h>

int main() 
{
	double prod = 0; int i;
	double V[ ] = { 2.23, 4.60, 6.71 };
	double W[3] = { 1.00, 2.00, 1.00 };
	
	for ( i = 0; i < 3; ++i ) {
		prod += V[i]*W[i];
	}
	printf("V . W = %.3f\n", prod);
	 
	return 0;
}
