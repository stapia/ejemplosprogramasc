
#include <stdio.h>
#include <stdlib.h>

int ejemplo1() {
	double *mat[4]; int i, j;
	for ( i = 0; i < 4; ++i ) {
		mat[i] = (double*) malloc((4-i)*sizeof(double));
		for ( j = 0; j < (4-i); ++j ) {
			mat[i][j] = 1.1;
		}
	}
	for ( i = 0; i < 4; ++i ) {
		free( mat[i] );
	}	
	return 0;
}

int main() {
	double **p; int N = 4; int i, j;
	p = (double**)malloc(N*sizeof(double*)); 
	for ( i = 0; i < N; ++i ) {
		p[i] = (double*) malloc((N-i)*sizeof(double));
		for ( j = 0; j < (N-i); ++j ) {
			p[i][j] = 1.1;
		}
	}
	
	for ( i = 0; i < 4; ++i ) {
		free( p[i] );
	}
	free(p);
	
	return 0;
}

