
#include <stdio.h>

void intercambiar(double *p, double *q) 
{
	double aux;
	aux = *p;
	*p = *q;
	*q = aux;
}

double* maximo(double *p, double *q) {
	/* ATENCION: es un error muy grave devolver como resultado
	 * la dirección de una variable local. 
	 * Por ejemplo "return &maxi;" es un ERROR */
	double maxi;
	if ( *p > *q ) {
		maxi = *p;
		return p;
	}
	else {
		maxi = *q;
		intercambiar(p,q);
		return p;
	}
}

int main() 
{
	double a = 10, b = 7.5, c = 12.2; double *puntero = &a;
	
	intercambiar(&a, &b);
	intercambiar(puntero,&b);
	
	printf("a: %f, b: %f\n", a, b);
	printf("&a: %p, &b: %p\n", &a, &b);
	
	puntero = maximo(&a, &b);
	printf("la mayor es: %f (%p)\n", 
					*puntero, 
					puntero);
					
	/* Ordenan a, b, c */
	maximo(&a, &b);
	maximo(&a, &c);
	maximo(&b, &c);
	
	printf("a,b,c: %f %f %f\n", a, b, c); 
	
	return 0;
}
