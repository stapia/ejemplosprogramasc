
#include <stdlib.h>
#include <stdio.h>

struct Elemento
{
    double dato;
    struct Elemento *sig;
};

struct FIFO
{
    struct Elemento *prim;
    struct Elemento *ult;
};

struct FIFO* create_queue()
{
    struct FIFO* f = malloc(sizeof(struct FIFO));
    f->prim = NULL; /* A */
    return f;
}

int is_empty(const struct FIFO *f)
{
    return f->prim == NULL;
}

double pop(struct FIFO *f)
{
    struct Elemento *aux;
    double result;

    if ( is_empty( f ) )
    {
        return 0.0/0.0; /*easy way to produce NaN*/
    }
    else
    {
        aux = f->prim; /* 1 */
        result = aux->dato; /* 2 */
        f->prim = aux->sig; /* 3 */
        free ( aux ); /* 4 */
        return result; /* 5 */
    }
}

void destroy_queue(struct FIFO* f)
{
    while ( ! is_empty( f ) )
    {
        pop( f );
    }
    free ( f );
}

void push(struct FIFO *f, double x)
{
    struct Elemento *aux;

    aux = malloc(sizeof(struct Elemento)); /*1*/
    aux->dato = x; /*2*/
    aux->sig = NULL; /*3*/

    if ( f->prim != NULL )
    {
        f->ult->sig = aux; /*4*/
        f->ult = aux; /*5*/
    }
    else
    {
        f->prim = aux; /*6*/
        f->ult = aux; /*7*/
    }
}

int main()
{
    int i;
    struct FIFO *fila;

    fila = create_queue();

    push(fila, 4.2);
    push(fila, 3.7);
    push(fila, 8.2);

    printf("El primero es %f\n", pop(fila));

    push(fila, 7.0);

    for ( i = 0; i < 3; ++i )
    {
        printf("%f\n", pop(fila));
    }

    printf("vacia: %i\n", is_empty(fila));

    printf("queda alguno?: %f\n", pop(fila));

    destroy_queue( fila );

    return 0;
}
