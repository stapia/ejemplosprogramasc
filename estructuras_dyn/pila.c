
#include <stdlib.h>
#include <stdio.h>

struct FILO;

struct FILO* create_stack();
void destroy_stack(struct FILO*);

int is_empty(const struct FILO *f);
double pop(struct FILO *f);
void push(struct FILO *f, double x);

int main()
{
    int i;
    struct FILO *pila;

    pila = create_stack();

    push(pila, 4.2);
    push(pila, 3.7);
    push(pila, 8.2);

    printf("El primero es %f\n", pop(pila));

    push(pila, 7.0);

    for ( i = 0; i < 3; ++i )
    {
        printf("%f\n", pop(pila));
    }

    printf("vacia: %i\n", is_empty(pila));
    
    destroy_stack(pila);

    return 0;
}


struct Elemento
{
    double dato;
    struct Elemento *sig;
};

struct FILO
{
    struct Elemento *prim;
};

struct FILO* create_stack()
{
    struct FILO* p = malloc(sizeof(struct FILO));
    p->prim = NULL; /* A */
    return p;
}

int is_empty(const struct FILO *p)
{
    return p->prim == NULL;
}

double pop(struct FILO *p)
{
    struct Elemento *aux;
    double result;

    aux = p->prim; /* 1 */
    result = aux->dato; /* 2 */
    p->prim = aux->sig; /* 3 */
    free ( aux ); /* 4 */
    return result; /* 5 */
}

void push(struct FILO *p, double x)
{
    struct Elemento *aux;

    aux = malloc( sizeof(struct Elemento) ); /*1*/
    aux->dato = x; /*2*/
    aux->sig = p->prim; /*3*/
    p->prim = aux; /*4*/
}

void destroy_stack(struct FILO* f)
{
    while ( ! is_empty( f ) )
    {
        pop( f );
    }
    free ( f );
}
