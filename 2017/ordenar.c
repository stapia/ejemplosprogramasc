
#include <stdio.h>

void swap( int *p, int *q ) {
	int c; c = *p; *p = *q; *q = c;
}

void ordenar3( int *px, int *py, int *pz ) {
	if ( *px < *py ) {
		swap( px, py );
	}
	if ( *py < *pz ) {
		swap( py, pz );
	}
	if ( *px < *py ) {
		swap( px, py );
	}	
}

int main() {
	int a, b, c;
	scanf("%i %i %i", &a, &b, &c);
	
	if ( a < b ) {
		swap( &a, &b );
	}
	
	if ( b < c ) {
		swap( &b, &c );
	}
	
	if ( a < b ) {
		swap( &a, &b );
	}
	printf("%i %i %i\n", a, b, c);
	
	ordenar3(&c, &b, &a);
	printf("%i %i %i\n", a, b, c);
	
	return 0;
}
