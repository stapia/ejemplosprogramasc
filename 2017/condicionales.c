
#include <stdio.h>

int main() {
	
	int j;
	
	scanf("%i", &j);
	
	if ( j > 0 ) {
		printf("El numero es positivo\n");
		j -= 10;
		printf("y le voy a restar 10\n");
	}
	
	printf("j es %i\n", j);
	
	if ( j > 0 ) {
		printf("positivo\n");
	}
	else {
		printf("negativo o cero\n");
	}
	
	/* MAL!!! */ 
	
	if ( j > 0 ) 
		if ( j % 2 == 0 ) printf("j es par y positivo\n");
	else 
	    printf("j es negativo\n");
	
	if ( j > 0 ) {
		if ( j % 2 == 0 ) printf("j es par y positivo\n");
	}
	else {
	    printf("j es negativo\n");
	}
	
	if ( j == 0 ) {
		printf("cero\n");
	}
	else if ( j >= 0 ) {
		printf("positivo\n");
	}
	else if ( j % 3 == 0) {
		printf("negativo y divisible por 3\n");
	}
	else {
		printf("negativo y no divisible por 3\n");
	}

	return 0;
}
