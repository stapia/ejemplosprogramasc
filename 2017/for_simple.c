
#include <stdio.h>

int main() {
	int i, suma, x;
	
	for ( i = 0; i < 3; i = i + 1) {
		printf("Voy por %i\t", i);
		printf("Voy!\n");
	}
	printf("La i vale: %i\n", i);

	i = 1;
	for ( ; i <= 11; ++i) {
		printf("Voy por %i\t", i);
		printf("Voy!\n");
	}
	printf("La i vale: %i\n", i);
	
	suma = 0; 
	for ( i = 0; i < 4; ++i ) {
		scanf("%i", &x);
		printf("suma antes es: %i\n", suma);
		suma = suma + x;
		printf("suma despues es: %i\n", suma);
	}
	printf("Suma = %i\n", suma);
	
	return 0;
}
