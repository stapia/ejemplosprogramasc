
#include <stdio.h>
#include <math.h>

int main() {
	/* double x; */
	int a = 4, b; double x; double t = 3.3;
	
	b = (3*(a - 7)) / 10;
	printf("b : %i\n", b);
	
	b = 12 / 10;
	printf("12/10 : %i\n", b);

	b = 12 % 10;
	printf("12%%10 : %i\n", b);

	b = (a + 12) > 10;
	printf("a+12>10 : %i\n", b);

	b = (a + 12) <= 10;
	printf("a + 12<=10 : %i\n", b);

	b = (a + 12) == 10;
	printf("a + 12==10 : %i\n", b);
	
	b = (a + 12) != 10;
	printf("a + 12 != 10 : %i\n", b);

	b = (a + 12) == 16;
	printf("a + 12 == 16 : %i\n", b);
	
	x = 3.9;
	b = (0.0 <= x) && (x <= 3.0);
	printf("x entre 0 y 3 : %i\n", b);

	b = ! b; /* ! 0.6 */
	printf("no b es: %i\n", b);

	x = (1/2)*t*t;
	printf("x: %f\n", x);
	
	b = (x == 0.0); 
	printf("x == 0 es: %i\n", b);
	
	b = fabs(x - 0.0) < 1e-6;  
	printf("x == 0 es: %i\n", b); 
	
	printf("%f %f %f\n", sin(30.0), log(3), pow(2,3));
	
	printf("%f %f %f\n", sqrt(30.0), sqrt(-1), atan(4));
	
	printf("%f\n", pow(30.0, 1.0/3.0));
	
	return 0;
}
