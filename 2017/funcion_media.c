
#include <stdio.h>

double media( void ) {
	double x, suma, result; int N; 
	suma = 0.0; N = 0; 
	scanf("%lf", &x);
	while ( x > 0.0 ) {
		suma = suma + x;
		++N;
		scanf("%lf", &x);
	}
	
	if ( N == 0 ) {
		return 0;
	}
	result = suma / N;
	return result;
}

int main() {
	printf("Introduce valores hasta escribir 0\n");
	printf("La media es %.2f\n", media() );
	return 0;
}
