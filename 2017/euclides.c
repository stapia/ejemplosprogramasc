
#include <stdio.h>

int main() {

	int a, b, resto; 
	
	scanf("%i %i", &a, &b);
	
	if ( b < a ) {
		printf("No se puede\n");
		return 1;
	}
	
	while ( a % b != 0 ) {
		resto = a % b;
		a = b;
		b = resto;
	}

	printf("El MCD es %i\n", b);
	return 0;
}
