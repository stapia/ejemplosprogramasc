
#include <stdio.h>

int main() {
	FILE *g;
	
	/* Ruta Relativas (PATH)
	g = fopen("datos.txt", "w");
	g = fopen("./datos.txt", "w");
	g = fopen("../datos.txt", "w");
	g = fopen("carpeta/datos.txt", "w");
	 */ 
	
	/* Ruta Absoluta */
	g = fopen("/home/usurios/datos.txt", "w");
	
	if ( g == NULL ) {
		printf("No se pudo abrir el archivo");
	}
	else {
		fprintf( g, "%i", 234214);
		fclose( g );
	}
	
	return 0;
}
