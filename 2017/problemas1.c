
#include <stdio.h>

int main() {
	/* Un punto menor que otro cuando x < x' o
	  cuando x = x' e y < y' */
    {
		printf("Introduce 2 puntos con valores: x y x' y'\n");
		double x,y, xp, yp; int result; 
		scanf("%lf %lf %lf %lf", &x, &y, &xp, &yp);
		result = ( x < xp) || (( x == xp ) && (y < yp));
		printf("Menor es %i\n", result);
	}
	
	{
		printf("Introduce un numero para sumar cifras\n");
		int num, suma; 
		scanf("%i", &num);
		suma = 0;
		while ( num >= 10 ) 
		{
			suma = suma + (num % 10);
			num = num / 10;
		}
		suma = suma + num;
		printf("suma: %i\n", suma);
	}
	
	{
		printf("Introduce un numero para sumar cifras\n");
		int num, suma; 
		scanf("%i", &num);
		suma = 0;
		do
		{
			suma = suma + (num % 10);
			num = num / 10;
		}
		while ( num > 0 ); 
		
		printf("suma: %i\n", suma);
	}
	
	{
		printf("Introduce un numero para saber si esta en [3,5]\n");

		double x; int dentro;
		scanf("%lf", &x);
		
		dentro = (x >= 3.0) && (x <= 5.0);
		printf("Dentro de [3,5] es %i\n", dentro);
	}
	
	return 0;
}
