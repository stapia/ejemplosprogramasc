
#include <stdio.h>
#include <string.h>

int main() {
	char cad_1[20];
	char cad_2[] = "Hola mundo 24\n"; 
	
	printf("%s", cad_2);
	/*
	sprintf(cad_1, 
		"Esta es la 1, escrita con sprintf"
		",%s", cad_2);
		*/
		
	scanf("%19s", cad_1);
	printf("Esta es la cad_1: '%s'\n", cad_1);
	printf("longitud de 1 = %i\n", strlen(cad_1));
	
	cad_1[0] = 'H'; /* '\0' */
	cad_1[1] = cad_1[1] + 1;
	cad_1[2] = 65;
	cad_1[4] = 0; /* '\0' */
	printf("Esta es la cad_1: '%s'\n", cad_1);
	printf("longitud de 1 = %i\n", strlen(cad_1));

	strcpy(cad_1,cad_2); /* Esto es asignar! */
	printf("cad_1 despues de strcpy: %s\n", cad_1);
	
	printf("son la misma: %i\n", cad_1 == cad_2);

	printf("son iguales: %i\n", strcmp(cad_1,cad_2)==0);
	
	strcat(cad_1, "--");
	printf("cad_1 despues de strcat: '%s'\n", cad_1);
	
	printf("Escribe cad_2 +4: '%s'", cad_2 + 4);
	
	strcpy(cad_1, "hola");
	strcpy(cad_1+5, "mundo");
	printf("\ncad1: '%s'", cad_1);
	cad_1[4] = ' ';
	printf("\ncad1: '%s'", cad_1);
	
	return 0;
}