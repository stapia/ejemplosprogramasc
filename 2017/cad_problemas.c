
#include <string.h>
#include <stdio.h>

void replace( char *cad ) {
	int i; 
	/* for ( i = 0; cad[i]; ++i ) */
	for ( i = 0; cad[i] != '\0'; ++i ) {
		if ( 'A' <= cad[i] && cad[i] <= 'Z' ) {
			cad[i] += 'a' - 'A';
		}
	}
}

void add_comma( char * cad ) {
	while ( (*cad != '\0') && (*cad) != ' ' ) {
		++cad;
	}
	if ( *cad == ' ' ) 
	{
		char aux1 = *cad, aux2;
		*cad = ',';
		++cad;
		
		while ( *cad != '\0' ) {
			aux2 = *cad;
			*cad = aux1;
			aux1 = aux2;
			++cad;
		}
		*cad = aux1;
		cad[1] = '\0';
	}
}

/* TODO: comprobar que pasa al quitar el const de s2 
 * Respuesta: Quitar el const es incorrecto, pero el compilador
 * no lo marca con el correspondiente "aviso" salvo que se ponga
 * otra opción adicional: -Wwrite-strings
 * */
void mayor( char *res, const char* s1, const char *s2)
{
	int comparacion = strcmp( s1, s2 );
	switch ( comparacion ) {
		case -1: 
		case 0: 
			strcpy( res, s2 );
			break;
		case 1: 
			strcpy( res, s1 );
			break;
	}
}

int main() {
	char cadena[80] = "HoLa MUndo";
	char resultado[80] = "";
	
	printf("%s\n", cadena);
	replace(cadena);
	printf("%s\n", cadena);
	add_comma(cadena);
	printf("%s\n", cadena);
	
	mayor( resultado, cadena, "Hi world");
	printf("%s\n", resultado);
	
	return 0;
}
