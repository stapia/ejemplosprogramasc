
#include <stdio.h> 
#include <stdlib.h>

void leer_vector( double v[], int N ) {
	int i; 
	for ( i = 0 ; i < N; ++i ) {
		scanf("%lf", v+i);
	}
}

void aleatorio_vector( double *v, int N ) {
	int i; 
	for ( i = 0 ; i < N; ++i ) {
		v[i] = rand() * 2.0 / RAND_MAX;
	}
}

void escribir( const double *v, int N ) {
	int i; 
	for ( i = 0 ; i < N; ++i ) {
		printf("%f ", *v);
		v = v + 1;/* ++v; */
	}
	printf("\n");
}

double prod_escalar(double *u, double *w, int N ) {
	int i; double suma = 0;
	for ( i = 0; i < N; ++i ) {
		suma += u[i]*w[i];
	}
	return suma;
}

double prod_alreves(double *u, double *w, int N ) {
	int i; double suma = 0;
	w = w + (N-1);
	for ( i = 0; i < N; ++i ) {
		/*suma += u[i]*w[N-1-i];*/
		suma += (*u) * w[0];
		++u; --w;
	}
	return suma;
}

int main() {
	double vector[20];
	/* aleatorio_vector(vector, 20); */
	leer_vector(vector, 20);
	escribir(vector+2, 5);
	
	printf("Prod: %f \n", 
		prod_escalar(vector, vector, 3));
	
	printf("Prod al reves: %f \n", 
		prod_alreves(vector, vector, 3));

	return 0;
}
