
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void changeSize(int w, int h) {
    float ratio;
    if (h == 0) h = 1;
	ratio =  w * 1.0 / h;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, w, h);
	gluPerspective(45.0f, ratio, 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);
}

float angle = 0.0f;

int vertices = 0;
float **dyn_mat = NULL;

void renderScene(void) {
    int i;
    
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	gluLookAt(	0.0f, 0.0f, 10.0f,
				0.0f, 0.0f,  0.0f,
				0.0f, 1.0f,  0.0f);

	glRotatef(angle, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);  
        for ( i = 0; i < vertices; ++i ) {
            glVertex3f(dyn_mat[i][0],dyn_mat[i][1], dyn_mat[i][2]);
        }
	glEnd();

	glutSwapBuffers();
}

void idleFunction(void) {
   	angle+=0.1f;
    glutPostRedisplay();
}

float** load_mat( FILE* g, int *vertices ) {
    /* Read size */
    int i = 0, ok = 0; float **mat = NULL; float v[3];
    if ( fscanf(g, "%i", vertices) != 1 ) {
        return NULL;
    }
    
    /* Generate vector of pointer */
    mat = malloc( *vertices * sizeof( float* ) );
    
    ok = fscanf(g, "%f %f %f", v, v+1, v+2 );
    for ( i = 0; i < *vertices && ok == 3; ++i ) {
        /* Generate each vertex (a vector of 3 floats) */
        mat[i] = malloc( 3 * sizeof(float) );
        memcpy( mat[i], v, 3 * sizeof(float) );
        ok = fscanf(g, "%f %f %f", v, v+1, v+2 );
    } 
    return mat;
}

void free_mat( float** mat, int vertices ) {
    int i;
    for ( i = 0; i < vertices ; ++i ) {
        free( mat[i] );
    } 
    free( mat );
}

void create_obj(const char* filename) {
    FILE *g = fopen(filename, "r");
    if ( g != NULL ) {
        dyn_mat = load_mat( g, &vertices );
        printf("Loaded file: '%s' with %i vertices\n", filename, vertices);
        fclose(g);
    }
    else {
        fprintf(stderr, "file: '%s' not found\n", filename);
    }
}

void cleanup() {
    printf("Exiting main loop (cleaning up)\n");
    free_mat(dyn_mat, vertices);
}

int main(int argc, char **argv) {
    atexit( cleanup );
    
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(50,50);
	glutInitWindowSize(620,620);
	glutCreateWindow("-- Ejemplo GLUT --");

	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutIdleFunc(idleFunction);

    create_obj("obj.txt");
    
    printf("Entering main loop ...\n");
    glutMainLoop();
    
	return 1;
}
