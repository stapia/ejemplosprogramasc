
#include <stdio.h>

double X;

int fulanito( int a, int b ) {
	int result;
	a = 200000 + a;
	printf("En fulanito la a vale: %i y la b %i\n", a, b);
	result = a + b;
	return result;
}

void grande( int y ) {
	/* MAL : double y;  */
	if ( y > 100 ) 
	{
		double Y = 4.0; double y = Y;
		if ( Y < X ) {
			printf("X(%.2f) es mayor que Y(%.2f)\n", X, y);
		}
		printf("y es muy grande\n");
	}
	else {
		/* MAL : printf("La Y grande es %f\n", Y); */
		printf("y es peque�a\n");
	}
}

/* Declaracion */
int main() {
	/* Implementacion */
	int y = 2, a = 5;
	y = 3*fulanito( a, y );
	/* Deberia avisar: y = 3*fulanito( a, 3.5 ); */
	/* cast explicito: y = 3*fulanito( a, (int)3.5 ); */
	/* MUY MAL: y = 3*fulanito( a, b, 3.5 ); */
	printf("y vale %i y a vale %i\n", y, a);
	
	grande( 100 * y);
	
	if ( y > 20 ) {
		return fulanito( 4, 6); 
	}
	else {
		return 0;
	}
}
