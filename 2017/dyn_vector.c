
#include <stdlib.h>
#include <stdio.h>

int suma( int v[], int tam) {
	int s = 0; int i;
	for ( i = 0; i < tam; ++i ) {
		s += v[i];
	}
	return s;
}

int main() {
	int *vector; int i, num = 3;
	
	vector = (int*) malloc( num * sizeof(int) );
	
	vector[0] = 3;
	vector[1] = 2;
	vector[2] = 42;
	
	printf("Suma : %i\n", suma( vector, num ));
	
	free( vector );
	
	scanf("%i", &num);
	vector = (int*) malloc( num * sizeof(int) );
	
	for ( i = 0; i < num; ++i ) {
		vector[i] = rand() % 101 - 50;
	}

	for ( i = 0; i < num; ++i ) {
		printf("%02i ", vector[i]);
	}
	printf("\nSuma : %i\n", suma( vector, num ));
	
	free( vector );

	return 0;
}
