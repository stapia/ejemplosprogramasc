
#include <stdio.h>

int main() {
	double Vo, a, t, V, X;
	
	printf("Introduce el tiempo, la aceleracion(-) y la Vo\n");
	scanf("%lf %lf %lf", &t, &a, &Vo);
	
	V = a * t + Vo;
	
	if ( V > 0 ) {
		X = (1.0/2.0)*a*t*t + Vo*t;
	}
	else {
		X = - Vo * Vo / (2 * a);
	}
	
	printf("La V y X finales son %f y %f\n", V, X); 
	
	return 0;
}
