
#include <stdio.h>

int main() { 
	unsigned char i;
	signed char j; 
	short _a = 013;
	unsigned int numero_mesa = 30;
	char letra = '!';
	double x2, malHecho;
	float y;
	x2 = 3.1416e-2;
	malHecho = 3,1416;
	
	printf("El valor de a (int) "
	       "es \t %i\n", _a);
	printf("x es %f\t mal es %f\n", x2, malHecho);
	printf("letra es %c\n", letra);
	_a = 0xA6; 
	printf("a es %i\n", _a);
	
	numero_mesa = -1;
	printf("numero de mesas es %u\n", numero_mesa);
	
	i = 200 + 57;
	j = 120 + 9;

	printf("i es %u\t j es %i\n", i, j);
	printf("-j es %i\n", ~j + 1);
	
	y = 0.1;
	printf("y es %.10f\n", y);
	
	y = 1e8;
	y = y + 1;
	printf("y es %.10f\n", y);
	
	return 0;
}
