
#include <stdio.h>

#define DIM 4

void min_maxV(double v[], int tam, double m_m[2])
{
	double minimo = v[0], maximo = v[0];
	int i; 
	for ( i = 1; i < tam; ++i ) {
		if ( minimo > v[i] ) {
			minimo = v[i];
		}
		if ( maximo < v[i] ) {
			maximo = v[i];
		}
	}
	
	m_m[0] = maximo;
	m_m[1] = minimo;
}

double min_max2(double v[], int tam, double *max) 
{
	double minimo = v[0], maximo = v[0];
	int i; 
	for ( i = 1; i < tam; ++i ) {
		if ( minimo > v[i] ) {
			minimo = v[i];
		}
		if ( maximo < v[i] ) {
			maximo = v[i];
		}
	}
	*max = maximo;
	return minimo;
}

int main() {
	double vector[DIM] = { 2, 5.4, -4.7, 10 };
	int i; double max, min; double extremos[2];
	for ( i = 0; i < DIM; ++i ) {
		printf("%10.2f\n", vector[i]);
	}
	
	min = min_max2( vector, DIM, &max) ;
	min_maxV( vector, DIM, extremos);
	
	printf("M/m %10.2f %10.2f\n", max, min);
	printf("M/m %10.2f %10.2f\n", extremos[0], extremos[1]);
	
	return 0;
}
