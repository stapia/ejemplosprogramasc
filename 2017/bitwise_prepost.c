
#include <stdio.h>

int main() {
	unsigned char i = 17,j = 5;
	unsigned char k;
	
	k = 16 & i;
	printf("%i\n", k);

	k = i | j;
	printf("%i\n", k);

	k =  ~i;
	printf("%i\n", k);

	k = 16 ^ i;
	printf("%i\n", k);
	
	k = i << 1;
	printf("%i\n", k);
	
	k = i >> 1;
	printf("%i\n", k);

	k = i >> 5;
	printf("%i\n", k);
	
	printf("%i\n", i);
	printf("%i\n", ++i);
	
	printf("%i\n", i);
	
	printf("%i\n", i--);
	
	printf("%i\n", i);
	
	++i;
	i--;
	printf("%i\n", i);
	
	i += 2;
	printf("%i\n", i);

	i = 130;
	printf("%c\n", i);

	return 0;
}
