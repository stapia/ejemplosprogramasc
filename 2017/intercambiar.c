
#include <stdio.h>

void intercambiar( int a, int b ) {
	int c; 
	c = a; 
	a = b;
	b = c;
}

void swap( int *p, int *q ) {
	int c; 
	printf("swap: p es %p\n", (void*)p);
	c = *p;
	*p = *q;
	*q = c;
}

int main() {
	int x = 4, y = 20; 
	int *px; /* Estoy declarando un puntero */
	
	intercambiar( x, y );
	printf("x = %d, y = %d\n", x, y);

	px = &x; 
	printf("La dirección de x es %p\n", (void*)px);
	
	swap( px, &y );
	printf("x = %d, y = %d\n", x, y);
	return 0;
}
