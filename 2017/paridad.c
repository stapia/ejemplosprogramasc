#include <stdio.h>

int main() {
	unsigned num;
	int par;
	
	num = 27;
	par = 1;
	while ( num != 0) {
		par = par ^ (num & 1);
		num = num >> 1;
	}
	
	printf("paridad: %i\n", par);
	
	return 0;
}
