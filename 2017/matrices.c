#include <stdio.h>

void prod(int A[][3], int B[][3], int C[][3]) {
	int i, j, k;
	for ( i = 0; i < 3; ++i ) {
		for ( j = 0; j < 3; ++j ) {
			C[i][j] = 0;
			for ( k = 0; k < 3; ++k ) {
				C[i][j] += A[i][k]*B[k][j];
			}
		}
	}
}

void transpose(int A[][3]) {
	int i, j, aux;
	for ( i = 0; i < 3; ++i ) {
		for ( j = i+1; j < 3; ++j ) {
			aux = A[i][j];
			A[i][j] = A[j][i];
			A[j][i] = aux;
		}
	}
}

void escribir(int A[][3]) {
	int i, j; 
	for ( i = 0; i < 3; ++i ) {
		for ( j = 0; j < 3; ++j ) {
			printf("%5i ", A[i][j]);
		}
		printf("\n");
	}
}

int main() {
	int matA[3][3] = {
		{ 11,   2, 3 },
		{  4,   5, 6 },
		{  7,   8, 9 }
	};

	int matB[3][3] = {
		{ 1,  0,  0 },
		{ 4,  2,  6 },
		{ 7,  8, -1 }
	};
	
	int matC[3][3];
	
	prod( matA, matB, matC );
	printf("Producto C = Axb : \n");
	escribir( matC );
	
	printf("Matriz A\n");
	escribir( matA );
	transpose( matA ); 
	printf("Matriz A transpuesta\n");
	escribir( matA );

	return 0;
}
