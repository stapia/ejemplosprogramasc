
#include <string.h>
#include <stdio.h>

struct datos {
	char cadena[80];
	double vector[3];
	int index, num;
};

void escribir(struct datos x) {
	printf("Index %i\n", x.index);
	/* ... */
}

void cambiar(struct datos *x) {
	x->index = x->index + 1;
	/* ... */
}

struct datos generar() {
	struct datos aux; 
	aux.index = 3; /* ... */
	return aux;
}

int main() 
{
	struct datos mi_dato, otro_dato;
	struct datos vect[5];
	mi_dato.index = 10;
	mi_dato.num = mi_dato.index + 3;
	strcpy(mi_dato.cadena,"Hola mundo");
	mi_dato.vector[0] = 3.1;
	
	vect[0].index = 0;
	(vect + 2)->index = 2;
	
	escribir( mi_dato );
	
	cambiar ( & mi_dato );
	escribir( mi_dato );
	
	mi_dato = generar();
	
	otro_dato = mi_dato;
	
	escribir( otro_dato );
	
	return 0;
}
