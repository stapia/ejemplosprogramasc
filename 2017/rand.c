
#include <stdio.h>
#include <stdlib.h>

int main() {
	
	int i, num; double x;
	
	srand(5);
	
	printf("Maximo de rand: %i\n", RAND_MAX);
	
	for ( i = 0; i < 10; ++i ) {
		num = rand();
		printf("%i\n", num);
		x = num * 1.0 / RAND_MAX;
		printf("%f\n", x);
		printf("1d6: %i\n", num % 6 + 1);
	}
	
	return 0;
}

