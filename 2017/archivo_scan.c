
#include <stdio.h>

int main() {
	FILE *g;
	g = fopen("datos.txt", "r");
	
	if ( g == NULL ) {
		printf("No se pudo abrir el archivo");
	}
	else {
		int num; double x; char letra;
		char cadena[128] = "Esto es lo que hay";
		int ok;
		/* Ejemplo de reglas de lectura */
		fscanf( g, "%i", &num);
		fscanf( g, "%lf", &x);
		fscanf( g, "%c", &letra);
		printf("Los numeros en el archivo son: %i\n", num);
		printf("y %f\n", x);
		printf("y la letra es '%c'\n", letra);
		fscanf( g, " %c", &letra);
		printf("y la letra es '%c'\n", letra);
		fscanf(g, " $Precio %127s", cadena);
		printf("y la cadena es '%s'\n", cadena);
		
		/* Comprobacion del retorno */
		ok = fscanf( g, "%i", &num);
		printf("El resultado de fscanf es %i\n", ok);
		ok = fscanf(g, "%127s %i", cadena, &num);
		printf("El resultado de fscanf es %i\n", ok);
		printf("y la cadena es '%s'\n", cadena);
		
		/* Leo hasta el final */
		while ( fscanf(g, "%c", &letra) != EOF ) 
		{
			printf("La letra es '%c'\n", letra);
		}
		fclose( g );
	}
	
	return 0;
}
