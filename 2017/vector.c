
#include <stdio.h>
#include <stdlib.h>

/* #define DIM 1000000 */
#define DIM 10

int main() {
	double datos[DIM]; double suma;
	int i = 0; int pares_desordenados;
	
	for ( i = 0; i < DIM; ++i ) {
		datos[i] = rand() * 1.0 / RAND_MAX;
		/* scanf("%lf", &(datos[i])); */
	}
	
	suma = 0;
	for ( i = 0; i < DIM; ++i ) {
		suma = suma + datos[i];
	}
	
	for ( i = 0; i < DIM; ++i ) {
		printf("%f\n",datos[i]);
	}
	
	/* Pares desordenados */
	pares_desordenados = 0;
	for ( i = 0; i < DIM-1; ++i ) {
		if ( datos[i+1] > datos[i] ) {
			pares_desordenados++;
		}
	}

	printf("media : %f\n",suma / DIM);
	
	return 0;
}
