
#include <stdio.h>

int main() {
	
	double x, y;
	
	scanf("%lf %lf", &x, &y);
	
	if ( x >= 0 && y > 0) 
	{
		printf("Primer Cuadrante\n");
	}
	else if ( x < 0 && y >= 0)
	{
		printf("Segundo Cuadrante\n");
	}
	else if ( x <= 0 && y < 0 )
	{
		printf("Tercer Cuadrante\n");
	}
	else
	{
		printf("Cuarto Cuadrante\n");
	}

	return 0;
}
