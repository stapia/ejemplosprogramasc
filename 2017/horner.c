
#include <stdio.h>

int main() {
	
	double coef, x, pol; int i;
	printf("Introduce la x\n");
	scanf("%lf", &x);
	
	pol = 0;
	for ( i = 4; i >= 0; --i ) {
		printf("Introduce el coef de grado %i ", i);
		scanf("%lf", &coef);
		pol = pol * x + coef;
	}
	
	printf("p(x=%f)= %f\n", x, pol);
	
	return 0;
}
