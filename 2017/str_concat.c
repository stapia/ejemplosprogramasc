
#include <string.h>
#include <stdio.h>

int main() {
	char frase[256]; 
	char palabra[32];
	
	/* Leer una frase, palabra a palabra, suponemos que el 
	 * resultado tiene menos de 256 caracteres, acabamos 
	 * cuando se escriba "fin" */
	
	strcpy(frase, "");
	scanf("%31s", palabra);
	while ( (strcmp(palabra,"fin") != 0) &&
			(strlen(frase) + strlen(palabra) + 1 < 256)
	       ) {
		strcat( frase, palabra );
		strcat( frase, " " );
		scanf("%31s", palabra);
	}
	frase[strlen(frase)-1] = '\0';
	printf("'%s'\n", frase);
	
	return 0;
}
