
int comprobar20(const double a[], int tam) 
{
	int resul, i;
	resul = 0;
	
	for ( i = 0; i < tam; ++i ) 
	{
		if ( a[i] > 20 ) 
		{
			resul = 1;
		}
	}
	
	return resul;
}

int comprobar20bis(const double a[], int tam) 
{
	int resul, i;
	resul = 0;
	
	for ( i = 0; i < tam && resul == 0; ++i ) 
	{
		resul = a[i] > 20; 
	}
	
	return resul;
}


int main() 
{
	return 0;
}
