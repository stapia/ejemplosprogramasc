
#include <stdio.h>

int main() 
{
	FILE *g;
	
	g = fopen("datos.txt", "r");
	
	if ( g == NULL ) 
	{
		printf("No puedo abrir el archivo\n");
	}
	else 
	{
		/* Se abrio correctamente */
		char aux[256];
		char letra; int otro, num = -1, i;
		int error;
		
		error = fscanf(g, "%i", &num);
		printf("He leido (%i): %i\n", error, num);

		fscanf( g, "%s", aux);
		printf("He leido: '%s'\n", aux);
		fscanf( g, "%s", aux);
		printf("He leido: '%s'\n", aux);

		fscanf( g, " %c", &letra);
		printf("He leido: '%c'\n", letra);

		for ( i = 0; i < 3; ++i ) 
		{
			error = fscanf( g, "%i %i", &num, &otro);
			printf("He leido (%i): %i\n", error, num);
		}
		
		fclose(g);
	}
	
	return 0;
}
