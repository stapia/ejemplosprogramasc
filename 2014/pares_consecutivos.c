
#include <stdio.h>

int main() 
{
	int vector[] = { 3, 17, 20, 22, -1 };
	int pares, i;
	
	pares = 0;
	for ( i = 0; i < 4; i++ )
	{
		if ( vector[i] % 2 != 0 && vector[i+1] % 2 == 0 ) 
		{
			pares = pares + 1;
		}
	}
	
	printf("Cumple 1º impar, 2º par: %i\n", pares);
	
	return 0;
}
