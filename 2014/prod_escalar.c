
#include <stdio.h>

double escalar(const double *A, const double *B)
{
	double suma = 0;
	int i;
	
	for ( i = 0; i < 3; ++i ) 
	{
		suma += A[i] * (*B);
		++B; /* B = B + 1; */
	}

	/* 
	for ( i = 0; i < 3; ++i ) 
	{
		suma += A[i] * (*(B+i));
	}
	*/
	
	return suma;
}

void dot(double k, double *vector, unsigned tam)
{
	unsigned i;
	for ( i = 0; i < tam; ++i ) 
	{
		vector[i] = k * vector[i];
	}
}

int main() 
{
	double vector[6] = { 1, 2, 3, 4, 5, 6 };
	int i;
	printf("escalar = %f\n", 
	         escalar(vector, vector+3));
	dot(3.0, vector, 4);
	for ( i = 0; i < 6; ++i ) 
	{
		printf("%f ", vector[i]);
	}
	return 0;
}
