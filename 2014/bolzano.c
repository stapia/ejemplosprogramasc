
#include <stdio.h>
#include <math.h>

int main() 
{
	double a, b;
	double m;
	
	scanf("%lf", &a);
	scanf("%lf", &b);
	
	while ( fabs(b-a) > 1e-6  ) 
	{
		m = (a+b) / 2;
		if ( sin(a) * sin(m) > 0 )
		{
			a = m;
		}
		else
		{
			b = m;
		}
	}
	
	printf("El cero esta en %.10f\n", m);
	
	return 0;
}
