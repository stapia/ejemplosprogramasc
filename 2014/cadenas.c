
#include <stdio.h>
#include <string.h>

int main() 
{
	char cadena[100] = "Hola Mundo\t";
	const char fin = 'H';
	int i; 
	
	printf("cadena = '%s', length = %i\n", 
	           cadena, strlen(cadena));
	
	cadena[7] = '\0'; /* cadena[7] = 0; */ 

	printf("cadena = '%s', length = %i\n", 
	           cadena, strlen(cadena));
			   
	cadena[7] = 78; /* cadena[7] = 'N'; */ 
			   
	printf("cadena = '%s', length = %i\n", 
	           cadena, strlen(cadena));
			   
	for ( i = 0; i < fin-'A'+1; ++i ) 
	{
		cadena[i] = 'A' + i;
	}
	cadena[i] = 0; /* cadena[fin-'A'+1] = '\0' */
	
	printf("cadena con letras = '%s', length = %i\n", 
	           cadena, strlen(cadena));

	cadena[0] = 0;
	printf("cadena? = '%s', length = %i\n", 
	           cadena, strlen(cadena));

	for ( i = 0; i < '9'-'0'+1; ++i ) 
	{
		cadena[i] = '0' + i;
	}
	cadena[i] = 0; /* cadena[fin-'A'+1] = '\0' */

	printf("cadena con numeros = '%s', length = %i\n", 
	           cadena, strlen(cadena));
	
	return 0;
}
