
#include <stdio.h>

int* mayor_funcion(int *p, int *q)
{
	if ( *p > *q ) 
	{
		return p;
	}
	else 
	{
		return q;
	}
}

int main() 
{
	int a = 14, b = 6;
	int *mayor;
	
	mayor = mayor_funcion(&a, &b);
	
	*mayor = (*mayor) * 2;
	
	printf("a = %i, b = %i, mayor = %i\n", a, b, *mayor);
	
	return 0;
}
