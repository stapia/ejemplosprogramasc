
#include <stdio.h>

/* Ejercicios con funciones con punteros */

/* Escribe el valor de la variable apuntada o "NULL" */
void escribe( int *p )
{
	if ( p != NULL ) 
	{
		printf("%d\n", *p);
	}
	else
	{
		printf("NULL\n");
	}
}

/* Devuelve si dos punteros apuntan a la misma variable */
int iguales( int *            a, int *b)
{
	return a == b;
} 

int main() 
{
	int a = 7;
	int b = 5;
	
	escribe( &a );
	escribe( NULL );
	
	printf("Son la misma variable es %i\n", iguales(&a, &a));
	printf("Son la misma variable es %i\n", iguales(&a, &b));
	printf("Son la misma variable es %i\n", iguales(&a, NULL));
	
	return 0;
}
