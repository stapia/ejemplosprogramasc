
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void pedir(double *V, int tam)
{
}

double producto(const double *V1, 
                const double *V2,
				int tam)				
{
	double suma = 0; int i;
	for ( i = 0; i < tam; ++i ) 
	{
		suma += V1[i] * V2[i];
	}
	return suma; 
}

int main() 
{
	int tam; double *V1; double *V2;
	double esc; char *cadena; 
	
	scanf("%i", &tam);
	
	V1 = malloc(tam*sizeof(double)
	            +5*sizeof(char));
				
	cadena = (char*)(V1 + tam);
	strcpy(cadena, "Hola");
	
	V2 = (double*)malloc(tam*sizeof(double));
	
	pedir(V1, tam);
	pedir(V2, tam);
	
	esc = producto(V1, V2, tam);
	
	free(V1);
	free(V2);
	
	return 0;
}
