
#include <string.h>
#include <stdio.h>

/* void intercambiar(char p[], char q[]) */
void intercambiar(char *p, char *q)
{
	char aux[256]; /* Ojo!! solo cadenas mas pequeñas */
	strcpy(aux, p);
	strcpy(p, q);
	strcpy(q, aux);	
}

int main() 
{
	char cad1[100] = "Hola";
	char cad2[100] = "Mundo";
	
	intercambiar(cad1, cad2);
	
	printf("%s %s\n", cad2, cad1);
	
	return 0;
}
