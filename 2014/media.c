
#include <stdio.h>

int main() 
{
	double x; 
	double total, media;
	int i;
	
	total = 0;
	
	for ( i = 0; i < 5; i = i + 1 )
	{
		scanf("%lf", &x);
		total = total + x;
	}
	
	media = total / 5;
	
	printf("La media es %f\n", media);
	
	return 0;
}
