
#include <stdio.h>

/* Suponemos que b es mayor que a */
int MCD(int a, int b)
{
	int resto;
	while ( b % a != 0 ) 
	{
		resto = b % a;
		b = a;
		a = resto;
	}
	return a;
}

void simplificar(int *num, int *den)
{
}

int main() 
{
	int p, q;
	
	scanf("%i %i", &p, &q);
	printf("El MCD es %i\n", MCD(p,q));
	
	return 0;
}
