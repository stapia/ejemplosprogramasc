
#include <stdio.h>

double frena ( double t, double v, char f) 
{
	double espacio = 0, a;
	
	if ( f == 'e' ) 
	{
		a = 0.9;
	}
	else 
	{
		a = 0.5;
	}
	
	a = ( f == 'e' ) * 0.9 + ( f == 's' ) * 0.5;
	
	if ( v - a * t >= 0 ) 
	{
		espacio = v * t - 0.5 * a * t*t; 
	}
	else 
	{
		espacio = (v * v) / (2 * a);
	}
	
	return espacio;
}

int main() 
{
	double inc_t, vel0;
	char frenado;
	double espacio;
	
	printf("Introduzca tiempo(s), velocidad(m/s) y frenado\n");
	
	scanf("%lf %lf %c", &inc_t, &vel0, &frenado);
	
	espacio = frena(inc_t, vel0, frenado);
	
	printf("Espacio = %f", espacio);

	return 0;
}
