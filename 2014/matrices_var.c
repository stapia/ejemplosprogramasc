
#define DIM 3

void inter(unsigned col, double mat[][DIM], unsigned tam)
{
	unsigned i; double aux;
	
	for ( i = 0; i < tam; ++i ) 
	{
		aux = mat[i][0];
		mat[i][0] = mat[i][col];
		mat[i][col] = aux;
	}
}
