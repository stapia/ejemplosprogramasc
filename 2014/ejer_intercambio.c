
#include <stdio.h>

void no_swap( double x, double y)
{
	double aux;
	
	aux = x;
	x = y;
	y = aux;
}

/* Intercambia variables (swap) */
void swap( double *x, double *y)
{
	double aux;
	
	aux = *x;
	*x = *y;
	*y = aux;
}

void ordenar(double *x, double *y) 
{
	if ( *x > *y ) 
	{
		swap( x, y );
	}
}

int main() 
{
	double a = 10, b = 20;
	
	printf("a = %f, b = %f\n", a, b);
	
	swap( &a, &b );
	
	printf("a = %f, b = %f\n", a, b);
	
	no_swap( a, b );

	printf("a = %f, b = %f\n", a, b);
	
	ordenar( &a , &b );

	printf("a = %f, b = %f\n", a, b);

	return 0;
}
