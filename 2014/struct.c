
#include <stdio.h>

struct Complejo
{
	double re, im;
};

struct Complejo crea(double x, double y)
{
	struct Complejo c;
	c.re = x; c.im = y;
	c.re = c.re + 2;
	return c;
}

void mostrar(struct Complejo c) 
{
	printf("%f + %f*i", c.re, c.im);
}

int main() 
{
	struct Complejo z = { 1.3, 2.3 };
	z = crea(23.3, 3.4);
	mostrar(z);
	printf("\n");
	return 0;
}
