
#include <stdio.h>
#include <math.h>

int main() 
{
	int num, suma;
	int es_impar;
	
	scanf("%i", &num);
	
	suma = 0;
	
	while ( num > 0 ) 
	{
		suma = suma + num % 10;
		num = num / 10;
	}
	
	es_impar = suma % 2 == 1;
	
		
	return 0;
}
