
#include <stdio.h>

struct Pol { double coef[100]; int grado; };

struct Mono { double coef; int grado; };

void multiplica(struct Pol *p, struct Mono m)
{
	int i;
	
	for ( i = p->grado; i >= 0; --i )
	{
		p->coef[i + m.grado] = p->coef[i] * m.coef;
	}
	
	for ( i = 0; i < m.grado; ++i ) 
	{
		p->coef[i] = 0;
	}
	
	p->grado += m.grado;
}

int main() 
{
	struct Pol pol;
	struct Mono mono;
	int i;
	
	pol.coef[0] = 3;
	pol.coef[1] = 0;
	pol.coef[2] = 0;
	pol.coef[3] = 2.7;
	pol.coef[4] = 0;
	pol.coef[5] = 8.7;
	
	pol.grado = 5;
	
	mono.coef = 2;
	mono.grado = 2;
	
	multiplica( &pol, mono );
	
	for ( i = 0; i <= pol.grado; ++i ) 
	{
		printf("%f*x^%i\n", pol.coef[i], i );
	}
	
	return 0;
}
