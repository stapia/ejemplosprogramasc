
#include <stdio.h>
#include <string.h>

int cad2num(const char *cad)
{
	unsigned int i, tam, result, cifra;
	tam = strlen(cad);
	result = 0;
	
	for ( i = 0; i < tam; ++i) 
	{
		if ( '0' <= cad[i] && cad[i] <= '9' ) 
		{
			cifra = cad[i] - '0';
		}
		else if ('a' <= cad[i] && cad[i] <= 'f' ) 
		{
			cifra = cad[i] - 'a' + 10;
		}
		result = result * 16 + cifra;
	}
	return result;
}

int main() 
{
	char cad[] = "4f";
	printf("%i\n", cad2num(cad));
	return 0;
}
