
#include <stdio.h>
#include <math.h>

int main() 
{
	double a, b, c;
	double x1, x2;
	
	scanf("%lf", &a);
	scanf("%lf", &b);
	scanf("%lf", &c);
	
	if ( a == 0 && b == 0 && c == 0 ) 
	{
		/* TIPO DE SOLUCIÓN 6 */
		printf("La solucion es cualquier x\n");
	}
	else if ( a == 0 && b == 0 )
	{
		/* TIPO DE SOLUCIÓN 4 */
		printf("No existe solucion\n");		
	}
	else if ( a == 0 )
	{
		/* TIPO DE SOLUCIÓN 5 */
		x1 = - c / b;
		printf("Ecuacion primer grado, x = %f\n", x1);		
	}
	else if ( (b*b - 4*a*c) == 0 )
	{
		/* TIPO DE SOLUCIÓN 2 */
		x1 = - b / (2 * a); 
		printf("Solucion es doble x = %f\n", x1);
	}
	else if ( (b*b - 4*a*c) < 0 )
	{
		/* TIPO DE SOLUCIÓN 3 */
		x1 = - b / ( 2*a);
		x2 = sqrt(-b*b + 4*a*c) / (2*a);
		printf("La solucion es : %f + %f i\n", x1, fabs(x2));
		printf("Y su conjugada\n");
	}
	else
	{
		/* TIPO DE SOLUCIÓN 1 */
		x1 = ( -b + sqrt(b*b - 4*a*c) ) / ( 2* a );
		x2 = ( -b - sqrt(b*b - 4*a*c) ) / ( 2* a );
		printf("Las soluciones son: \n");
		printf(" x1 = %f\n", x1);
		printf(" x2 = %f\n", x2);
	}	
	
	return 0;
}
