
#include <stdio.h>

int main() 
{
	FILE *g;
	char nombre[1024];
	
	scanf("%s", nombre);
	
	g = fopen(nombre, "r");
	
	if ( g == NULL ) 
	{
		printf("No puedo abrir el archivo\n");
	}
	else 
	{
		/* Se abrio correctamente */
		char linea[1024];
		char *aux;
		int leidos, numero1, numero2;
		double dato;
		
		aux = fgets(linea, 1024, g);
		
		while ( aux != NULL )
		{
			/* Proceso la linea */ 
			if ( linea[0] == ';' ) 
			{
				printf("%s", linea);
			}
			else 
			{
				leidos = sscanf(linea, "%i:%i :%lf", &numero1, &numero2, &dato);
				
				if ( leidos == 3 ) 
				{
					printf("He leido: %lf\n", dato);
				}
			}		
			aux = fgets(linea, 1024, g);
		}
		
		fclose(g);
	}
	
	return 0;
}
