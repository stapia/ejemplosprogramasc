
#include <stdio.h>
#include <math.h>
int main() 
{
	FILE *f;
	
	f = fopen("datos.txt", "w");
	
	if ( f == NULL ) 
	{
		printf("No puedo abrir el archivo\n");
	}
	else 
	{
		/* Se abrio correctamente */
		
		fprintf(f, "Hola Mundo\n");
		fprintf(f, "%5i%5i\n", 23, 24);
		fprintf(f, "%5i%5i\n", 123, 224);
		fprintf(f, "%4i%4i\n", 3, 4);
		fprintf(f, "%10.2f %10.10f\n", sqrt(2), sqrt(3));
		fprintf(f, "%10.3f %8.8f\n", sqrt(2), sqrt(3));
		
		fclose(f);
	}
	
	return 0;
}
