
#include <stdlib.h>
#include <stdio.h>

struct Elemento 
{
	double dato;
	struct Elemento *sig;
};

struct FIFO 
{
	struct Elemento *prim;
	struct Elemento *ult;
};

int is_empty(struct FIFO f) 
{
	return f.prim == NULL;
}

double pop(struct FIFO *f)
{
	struct Elemento *aux;
	double result;
	
	aux = f->prim; /* 1 */
	result = aux->dato; /* 2 */
	f->prim = aux->sig; /* 3 */
	free ( aux ); /* 4 */
	return result; /* 5 */
}

void push(struct FIFO *f, double x) 
{
	struct Elemento *aux;
	
	aux=malloc(sizeof(struct Elemento));/*1*/
	aux->dato = x; /*2*/
	aux->sig = NULL; /*3*/
	
	if ( f->prim != NULL ) 
	{
		f->ult->sig = aux; 
		f->ult = aux; 
	}
	else 
	{
		f->prim = aux;
		f->ult = aux;
	}
}

int main() 
{
	int i;
	struct FIFO fila;
	fila.prim = NULL;
	
	push(&fila, 4.2);
	push(&fila, 3.7);
	push(&fila, 8.2);

	printf("El primero es %f\n", pop(&fila));
	
	push(&fila, 7.0);

	for ( i = 0; i < 3; ++i ) 
	{
		printf("%f\n", pop(&fila));
	}
	
	printf("vacia: %i\n", is_empty(fila));
	
	return 0;
}
