
#include <stdio.h>

int maximo()
{
	int max, i, A;
	
	scanf("%d", &max);
	
	for ( i = 2; i <= 5; ++i ) 
	{
		scanf("%d", &A);
		
		if ( A > max ) 
		{
			printf("Cambio el valor de max\n");
			max = A;
		}
	}
	return max;
}

int main() 
{
	int vector[] = { 1, 5, 76, 12, -3 };
	int max, i, posicion;
	
	max = vector[0];
	posicion = 0;
	
	for ( i = 1; i < 5; i++ ) 
	{
		if ( vector[i] > max ) 
		{
			max = vector[i];
			posicion = i;
		}
	}
	
	printf("El maximo del vector es %i ", max);
	printf("y su posicion %i\n", posicion);

	/*printf("El maximo es %i\n", maximo());
	 */ 
	return 0;
}
