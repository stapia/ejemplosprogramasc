
#include <stdio.h>


void incrementar(double *a)
{
	printf("el valor de a es %p\n", (void*)a);
	*a = *a + 1;
}

int main() 
{
	double x = 20;
	
	incrementar(&x);
	printf("x esta en %p y vale %f\n", (void*)&x, x);
	
	return 0;
}
