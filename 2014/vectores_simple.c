
#include <stdio.h>

int main() 
{
	int vector[5] = { 1, 2, 3 };
	int i;
	
	printf("Vector = ( ");
	for ( i = 0; i < 5; ++i ) 
	{
		printf("%i ", vector[i]);
	}
	printf(") \n");

	for ( i = 0; i < 5; ++i ) 
	{
		vector[i] = (i+1)*3;
	}

	printf("Vector = ( ");
	for ( i = 0; i < 5; ++i ) 
	{
		printf("%i ", vector[i]);
	}
	printf(") \n");
	
	return 0;
}
