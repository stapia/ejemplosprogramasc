
#include <stdio.h>

int main() 
{
	double x = 4;
	double a = 1, b = 5;
	
	if ( x < a ) 
	{
		printf("X a la izquierda del intervalo");
	}
	else if ( x <= b ) 
	{
		printf("X en intervalo");
	}
	else 
	{
		printf("X a la derecha");
	}
	
	/*
	if ( x < 3 ) 
	{
		if ( x > 2 ) 
		{
			printf("Linea 12\n");
			printf("Linea 13\n");
		}
	    else 
		{
		   printf("Linea 17\n");
	    }   
	}
	else 
	{
	}
	*/
	
	scanf("%*s");
	
	return 0;
}
