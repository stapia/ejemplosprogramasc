

#include <stdio.h>

int main() 
{
	FILE *g;
	
	g = fopen("../data/datos1.txt", "r");
	
	if ( g == NULL ) 
	{
		printf("No puedo abrir el archivo\n");
	}
	else 
	{
		char palabra[256];
		int leido;
		
		leido = fscanf( g, "%s", palabra );
		while ( leido == 1 ) 
		{
			printf("%s ", palabra);
			leido = fscanf( g, "%s", palabra );
		}
		/* Se abrio correctamente */
		fclose(g);
	}
	
	return 0;
}
