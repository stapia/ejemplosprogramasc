
#include <string.h>
#include <stdio.h>

void add(char *p, unsigned num)
{
	char cifra[2] = "0";
	// añadir un espacio
	strcat(p, " ");
	
	cifra[0] = num % 10 + '0';
	strcat(p, cifra);

	// añadir otro espacio
	cifra[0] = 32;
	strcat(p, cifra);
	
	// añadir un punto y coma
	strcat(p, ";");
	
	/* Sustituir el primer caracter
	 * por $ */
	p[0] = '-';
}

int main() 
{
	char cad1[100] = "Hola";
	
	add(cad1, 87);
	
	printf("%s\n", cad1);
	
	strcpy(cad1, "Hola Mundo");
	printf("'%s'\n", cad1+5);

	printf("%c\n", cad1[5]);
	printf("%c\n", *(cad1+5));
	
	return 0;
}
