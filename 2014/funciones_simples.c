
#include <stdio.h>

void mostrar(double x, double y)
{
	printf("( %f , %f )\n", x, y );
	/* return; es opcional */
}

double media(double x, double y) 
{
	double resultado;
	resultado = ( x + y ) / 2;
	return resultado;
}

int main() 
{
	double m;
	
	mostrar( 3.3 , 6 );
	
	m = 2 * media( 3.3 , 6 );
	
	printf("El doble de la media es %f \n", m);
	
	return 0;
}
