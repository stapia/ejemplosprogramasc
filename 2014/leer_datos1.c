
#include <stdio.h>

int main() 
{
	FILE *g;
	
	g = fopen("../data/datos1.txt", "r");
	
	if ( g == NULL ) 
	{
		printf("No puedo abrir el archivo\n");
	}
	else 
	{
		/* Se abrio correctamente */
		char linea[1024];
		char *aux;
		int leidos, numero;
		double dato1, dato2, media;
		
		aux = fgets(linea, 1024, g);
		printf("He leido: %s", linea);
		aux = fgets(linea, 1024, g);
		printf("He leido: %s", linea);
		aux = fgets(linea, 1024, g);
		printf("He leido: %s", linea);
		
		aux = fgets(linea, 1024, g);
		printf("He leido: %s", linea);
		numero = 0; media = 0;
		
		while ( aux != NULL )
		{
			/* Proceso la linea */ 
			leidos = sscanf(linea, "%lf %lf", &dato1, &dato2);
			
			if ( leidos == 2 ) 
			{
				++numero;
				media += dato1;
			}
			
			aux = fgets(linea, 1024, g);
		}
		
		media = media / numero;
		printf("Media = %f\n", media);
		fclose(g);
	}
	
	return 0;
}
