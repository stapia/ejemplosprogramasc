
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() 
{
	int a; int b;
	int* p; int *q;
	
	srand(time(NULL));

	a = rand();
	b = rand() % 100 + 1;

	printf("a = %i, b = %i\n", a, b);
	
	p = &a;
	*p = 10;
	
	printf("a = %i, b = %i\n", a, b);

	q = &a;
	*q = 20;

	printf("a = %i, b = %i\n", a, b);
	
	p = &b;
	*p = rand();
	
	printf("a = %i, b = %i\n", a, b);
		
	return 0;
}
