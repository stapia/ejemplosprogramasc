
#include <stdio.h>

double tramos(double x) 
{
	double y;
	
	if ( x < 1.0 ) 
	{
		y = 0;
	}
	else if ( x < 2.0 ) 
	{
		y = x;
	}
	else if ( x < 3.0 ) 
	{
		y = 2;
	}
	else if ( x < 4.0 ) 
	{
		y = x - 1;
	}
	else
	{
		y = 3.0;
	}
	
	return y;
}

int main() 
{
	return 0;
}
